\chapter{CORRELATOR FITS}
\label{app:correlators}

\section{Baryons}

\subsection{Fit function}
In section \ref{sect:lattice_qcd}, we saw how to calculate the correlator of two observables using the Euclidean path integral.
\begin{equation}
\langle O_2(t) O_1(0) \rangle =
\frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[U] \, e^{-S_F[q, \overline q, U]-S_G[U]} O_2[q_{(t)}, \overline{q}_{(t)}, U_{(t)}] O_1[q_{(0)}, \overline{q}_{(0)}, U_{(0)}]
\end{equation}
This (Euclidean) path integral is sampled using Monte-Carlo to determine the two-point function on the lattice. However, there is another way one might defined the correlation function. As operators (rather than functionals), the correlation function is defined as
\begin{equation}
\langle O_2(t) O_1(0) \rangle
 = \lim\limits_{T \rightarrow \infty} \frac{1}{Z_T} \Tr \left\{ e^{-(T-t)\hat{H}} \hat{O}_2 e^{-t\hat{H}} \hat{O}_1 \right\}
\end{equation}
where $Z_T = \Tr \left\{ e^{-T \hat H}\right\}$. From this definition, we see that by introducing a complete set of energy eigenstates, we can rewrite this correlation function in terms of its energy spectrum.
\begin{align}
\langle O_2(t) O_1(0) \rangle
&= \sum_{m, n} \braket{m | e^{-(T-t) \hat H} \hat O_2| n} 
\braket{n | e^{-t \hat H} \hat O_1| m} \\
&\approx \sum_{n} \braket{0 | \hat{O}_2 | n} \braket{n | \hat{O}_1 | 0} e^{-t E_n} \qquad \text{as $T \rightarrow \infty$}
%\label{eqn:baryon-correlation-fcn}
\end{align}

The above formula is quite general, but it doesn't tell us how to calculate the mass of some particular baryon. To do that, we replace $\hat O_1, \hat O_2$ with operators that create a baryon from the vacuum at $t=0$, which is then destroyed at $t=1$.
\begin{equation}
\langle O_2(t) O_1(0) \rangle  \rightarrow \braket{\Omega | B(t) B^\dagger(0) | \Omega}
\end{equation}
Here $\Omega$ is the (QCD) vacuum and $B^\dagger$ is an operator that creates an excitation with quantum numbers equal to that of the specific baryon we're interested in studying. The details here are quite messy (see Chapter 6 of~\cite{Gattringer:2010zz}, for example), but the important bit is that these baryon interpolators links lattice sites (sources and sinks). We can either have the baryon be created/annihilated at a single source/sink (which we call Dirac or point) or some general small region on the lattice (which we call smeared).

For technical reasons, it's computationally cheaper to generate either the sink or the source smeared. In our case, the source is always smeared, which we use to generate two copies of the ensemble, one with a smeared sink and one with a point sink. The energy spectrum for either sink is the same, but the wave function overlaps can differ. Therefore, we simultaneously fit the following two equations to determine the energy spectrum:

\begin{align}
C_{PS}(t) &= \sum_n Z^{(P)}_n Z^{(S)}_n e^{-t E_n} \, ,\\
C_{SS}(t) &= \sum_n \lvert Z^{(S)}_n \rvert^2 e^{-t E_n} \, .
\end{align}


\subsection{Priors}

Our fits are implemented through \texttt{lsqfit}~\cite{lsqfit}, a Bayesian least squares fitter; since we are performing a Bayesian fit, we require that our \emph{degree of belief} in the values of each of our fits parameters be characterized \emph{a priori}. For the most part, we leave our priors much wider than expected, and therefore the prior mostly serves to guide the minimizer towards non-pathological local minima.

We use the following iterative process to set our priors for the hyperons.


\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figs/appendices/delta_a09m350_meff.pdf}
	\caption{Example of a correlator fit. The effective mass for a baryon asymptotes to the ground state, but the noise also grows with time.}
	\label{fig:app_baryon_eff_mass}
\end{figure}

\begin{enumerate}
	\item Plot the effective mass $M_\text{eff}(t) = \log[C(t)/C(t+1)]$  to determine a candidate time range. For the starting time $t_\text{start}$, choose a time near the first plateau. The ending time $t_\text{end}$ typically has little influence on the final result and can be chosen more freely; nevertheless, it's wise to avoid a $t_\text{end}$ where the noise is too great (in principle, the fitter should weigh these noisier points less, so including a few of them in the fit is fine).

	\item Perform an initial fit with a very wide prior, e.g.:
	\begin{align*}
	\texttt{p[`E']} &= \texttt{[1.0(1.0), 2.0(1.0), 3.0(1.0), $\cdots$]} \\
	\texttt{p[`wf\_dir']} &= \texttt{[0.0(1.0), 0.0(1.0), 0.0(1.0), $\cdots$] $\times$ e-04} \\
	\texttt{p[`wf\_smr']} &= \texttt{[0.0(1.0), 0.0(1.0), 0.0(1.0), $\cdots$] $\times$ e-04}
	\end{align*}
	The priors for the excited state energies, in particular, are not very good. However, the posterior will be roughly correct. Record the fit results for the ground state: $m_H \pm \sigma_{m_H}$, $A_\text{smr} \pm \sigma_{A_\text{smr}}$, $A_\text{dir} \pm \sigma_{A_\text{dir}}$.

	\item Using the results of the previous fit, we can now set reasonable priors for the fit. The ground state should be roughly the same as that in our loose fit; to that end, we set the central value to be $m_H$ from the previous fit. To prevent biasing the fit, set the width to be \texttt{100$\sigma_{m_H}$}, which in practice will cover all but the earlier and latest (nosiest) effective mass values.

	For the excited energy states, prior the next energy level to be two pions masses above the previous state with a width of one pion mass (a baryon plus two pions has the same parity and spin as the original baryon).

	For the wave function overlaps, we use the results of the previous fit. Since the Dirac wave function overlap can be negative, set the prior for the ground state to be $0 \pm 2A_\text{smr}$. The smeared wave function overlap must be positive, so we set the prior to $A_\text{dir} \pm A_\text{dir}$. The correlators are generated such that the excited states have smaller wave function overlaps than the ground state; therefore, it is sufficient to use the same prior for the excited states, too. 

\end{enumerate}

This procedure will generate a prior like
\begin{align*}
\texttt{p[`E']} &= \texttt{[$m_H$(100$\sigma_{m_H}$), $m_H$+2$m_\pi$($m_\pi$), $m_H$+4$m_\pi$($m_\pi$), $\cdots$]} \\
\texttt{p[`wf\_dir']} &= \texttt{[$A_{dir}$($A_{dir}$)\,\,\,\,\,\,\,\,, $A_{dir}$($A_{dir}$)\,\,\,\,\,\,\,\,\,\,, $A_{dir}$($A_{dir}$)\,\,\,\,\,\,\,\,\,\,, $\cdots$]} \\
\texttt{p[`wf\_smr']} &= \texttt{[0(2$A_{smr}$)\,\,\,\,\,\,\,\,\,\,\,, 0(2$A_{smr}$)\,\,\,\,\,\,\,\,\,\,\,\,, 0(2$A_{smr}$)\,\,\,\,\,\,\,\,\,\,\,\,\,, $\cdots$]}
\end{align*}
(Note that $m_H + 100 \sigma_{m_H} < m_H + 2 m_\pi$ even after the first iteration, so the ordering of the energy spectrum is as expected.)


\subsection{Fit criteria}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./figs/appendices/delta_a09m350_stability.pdf}
	\caption{Stability plot versus $t_\text{start}$ and $N_\text{states}$. Here $w_N$ denotes the relative weights of the different $N_\text{states}$ fit at a fixed $t$, which can be used to identify when the fit is stable versus $N_\text{states}$.}
	\label{fig:app_baryon_stability}
\end{figure}

When choosing a best candidate among fits, we rank a fit based on the following criteria:

\begin{enumerate}
	\item \textbf{The fit should have an acceptable $\chi^2_\nu$ and $Q$-value.} Fits with $\chi^2_\nu \gg 1$ underfit the data; fits with $\chi^2_\nu \ll 1$ overfit the data. Fits with poor $Q$-values either disagree strongly with the prior (in which case the choice of prior might need to be redone) or poorly match the data. Anything with $Q<0.1$ is suspect.

	\item \textbf{The fit should be stable versus time.} In practice, this means the fit should be stable versus $t_\text{start}$ since the noise grows with time, resulting in the fit being mostly determined by data at earlier times. We prefer fits that are in a stability plateau, not fits that are tending upwards or downwards as shown by the stability plot.

	\item \textbf{The fit should be stable versus number of states.} We prioritize fits that are nearly in exact agreement for $N_\text{states}=2$ and $N_\text{states}=3$ while having some agreement with the late-time $N_\text{states}=1$ fit.

\end{enumerate}



\section{Mesons}

The fit strategy employed for the mesons is generally taken from~\cite{Bouchard:2016heu}, albeit with more conservative choices for our fits. We fit the two-point correlator as a single $\cosh$ (see below), symmetrically fitting the interval $[t, T-t]$. When picking a fit, we prefer later plateaus to earlier ones, as those are the ones less likely to be contaminated by excited states; further, we choose our fit such that the majority of late-time fits fall within the error bar of the chosen fit (see stability plots below). That is, we choose as our representative fits to be the ones with errors likely noticeably larger than they could be, preferring to err on the side of overestimating our error versus underestimating it.

Because our fits are so conservative, and because the excited states are sensitive to the choice of prior (unlike the ground state), we omit stability plots of the excited states in our analysis.

\subsection{Fit function}
Let us briefly revisit the spectral decomposition of the correlation function.
\begin{equation}
	\langle O_2(t) O_1(0) \rangle
	= \sum_{m, n} \braket{m | e^{-(T-t) \hat H} \hat O_2| n} 
	\braket{n | e^{-t \hat H} \hat O_1| m}
\end{equation}
Previously we simplified this sum by taking the limit $T\rightarrow \infty$. However, there is no such thing as infinity on the lattice. Like the spacial extent, we force the temporal extent to have periodic boundary conditions. For a baryon, the noise of the correlation function grows exponentially with time, so by the time the signal correlators wraps back around to the start is imperceptible. This is not the case for mesons, which have nearly constant noise, so we can no longer ignore the $\braket{m | e^{-(T-t) \hat H} \hat O_2| n}$ term. This motivates the following expression for the correlation function,
\begin{equation}
C(t) = \sum_n Z^{(PS)}_{n} Z^{(SS)}_{n} \left( e^{-E_n t} + e^{-E_n(T-t)} \right) \, .
\label{eqn:meson-correlation-fcn}
\end{equation}

Suppose we are only interested in the ground state, not the wave function overlap factors or excited states. At later times, the higher excited states will die out, leaving only the ground state to be fit. For our fit function, therefore, we fit a single state (which we've reexpressed as a hyperbolic function)
\begin{equation}
C(t) \approx A_0 \cosh\left(e^{\xi_0} (t-T/2)\right)
\end{equation}
where we have fit $\xi_0 = \log E_0$ to ensure the ground state remains positive and we have absorbed the overlap in $A_0 = 2 Z^{(PS)}_{0} Z^{(SS)}_{0} e^{-E_0 T}$. Thus we get the result for $E_0$ by exponentiating $\xi_0$.

In this fit function, $E_0$ is log-normally distributed; however, since the fits are employed with {\tt lsqfit} (and the errors are propagated as {\tt gvar} variables), by exponentiating $\xi_0$ we propagate the error through, thereby getting a Gaussian distribution for $E_0$. Refer to Fig.~\ref{fig:E0_vs_logE0} for a comparison of fitting $E_0$ directly versus fitting $\log E_0$ and exponentiating the {\tt gvar} result.

\begin{figure}
	\centering

	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{./figs/appendices/bs_fit_E0.pdf}
		\caption{Fitting $C(t) \approx A_0 \cosh\left(E_0 (t-T/2)\right)$ for $E_0$}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{./figs/appendices/bs_fit_logE0.pdf}
		\caption{Fitting $C(t) \approx A_0 \cosh\left(e^{\xi_0} (t-T/2)\right)$ for $\xi_0$}
	\end{subfigure}
	
	\caption{Comparison of fit strategies for fitting a meson. After rerunning the fit on 5000 bootstraps resamples, the results are identical.}
	\label{fig:E0_vs_logE0}
\end{figure}



\subsection{Prior}

In order to use {\tt lsqfit}, we must first set the prior for our fit parameters, $\xi_0$ and $A_0$, which we obtain from the effective mass and effective wave function overlap, respectively. The former is constructed thus:
\begin{equation}
M_\text{eff}(t) = \text{arccosh} \left( \frac{C(t+1) + C(t-1)}{ 2 C(t)} \right) \, ,
\end{equation}
while the latter is constructed like so:
\begin{equation}
A_\text{eff}(t) = \frac{C(t)}{\cosh\left(M_\text{eff}(t- T/2)\right)} \, .
\end{equation}

When picking our priors for $E_0$ and $A_0$, we choose values that include the values of $M_\text{eff}(t)$ and $A_\text{eff}(t)$ at all but the few earliest and latest times, where contamination from higher order states is obvious. In practice, we achieve this through the following procedure, using \texttt{phi\_jr\_5/a15m350} as an example:
\begin{enumerate}
	\item Pick a candidate time range for the fit by looking at the effective mass plot. Because meson statistics are so well behaved, this choice can be essentially any time range.
	\item Pick very loose priors for $E_0$ and $A_0$, e.g. {\tt p[E0] = $1.0(1.0)$, p[wf\_dir 0] = $0.00000(10)$}
	\item Record the fit result (e.g., a loose fit might yield something like $E_0 = 0.41202(77)$, $A_0 = 1.039(11)\times 10^{-11}$).
	\item For $E_0$, keep the mean and multiply the uncertainty by $100$. Use this new value for the prior. For $A_0$, center the mean value about $0$ with an uncertainty equal to twice the mean value from the fit. Our new prior would be {\tt p[E0] = $0.41(10)$, p[wf\_dir 0] = $0.0(2.1)\times 10^{-11})$}.
\end{enumerate}

Notice that we use $E_0$ when picking our prior, despite fitting $\log E_0$. As an intermediate step, we use {\tt gvar} to convert {\tt p[E0]} to {\tt p[log(E0)]} when performing the fit.

These uncertainty of these priors end up being much larger than the uncertainties of any particular $M_\text{eff}(t)$ and $A_\text{eff}(t)$ as constructed from the data, so our Bayesian fit is essentially unaffected by our choice of prior; here the prior serves to assist the fitter in converging faster to the correct answer. In the $M_\text{eff}(t)$ and $A_\text{eff}(t)$ plots below (Fig.~\ref{fig:effective_meson}), the priors are wide enough that they cover the entirety of the $y$-limits in the plots.

\begin{figure}
	\centering

	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{./figs/appendices/kaon_a15m400_eff_mass.pdf}
		\caption{Effective mass}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{./figs/appendices/kaon_a15m400_eff_wf.pdf}
		\caption{Effective wave function overlap}
	\end{subfigure}
	
	\caption{Plots of effective quantities. Unlike for baryons, the noise is nearly constant with time.}
	\label{fig:effective_meson}
\end{figure}

\subsection{Fit criteria}
When choosing a representative fit, we evaluate the fit based on the following criteria, with importance ranked as follows:

\begin{enumerate}
	\item \textbf{Avoid fits with $\chi^2_\nu < 0.2$.} We exclude these fits under the assumption that they are too conservative.
	\item \textbf{Choose fits that agree with other fits.} Ideally the chosen fit should be wide enough such that it contains the uncertainty intervals of all but the latest and earliest symmetric fits. If there are multiple such fits, choose the one that minimally covers most fits.
	\item \textbf{Prefer later plateaus.} If there is no fit that generally agrees with most of the other fits, then prefer conservative fits that agree with later plateaus.
\end{enumerate}

