\chapter{IMPACT OF CHOICE OF $\Lambda_\chi$, $\mu$}
\label{app:fk_fpi_extras}

\section{Adapting a fixed renormalization scheme to the lattice} 
The appendix of \cite{Amoros:1999dp} gives $\chi$PT expressions for the pseudoscalar decay constants, which we repeat here. The expressions are written in the form
\begin{equation}
F_P = F_0 \left(1 + \delta F_P + \cdots \right) \, 
\end{equation}
where $F_0$ is the value of $F_\pi$ in the chiral limit. The NLO corrections to the decays constants are
\begin{align}
\delta F_\pi &= - l^0_\pi - \frac12 l^0_K + 4 \left[ 
	\left( \epsilon^2_\pi + 2 \epsilon^2_K \right) \overline L_4 + 4 \epsilon^2_\pi  \overline L_5
\right] \\
\delta F_K &= - \frac38 l^0_\pi - \frac34  l^0_K - \frac38  l^0_\eta + 4\left[ 
\left( \epsilon^2_\pi + 2 \epsilon^2_K \right) \overline L_4 + 4 \epsilon^2_K  \overline L_5
\right]
\end{align}
where
\begin{equation}
\epsilon_P = \frac{m_P}{4 \pi F_\pi} \, , \qquad 
l^{P^\prime}_P = \epsilon^2_P \log \left[\left( \frac{m_P}{\mu_{P^\prime}}\right)^2\right] \, , \qquad
\overline L_i = (4\pi)^2 L_i \, .
\end{equation}
In this case, $l^0_P$ corresponds to the choice $\mu=\mu_0$. We stress the following points:
\begin{enumerate}
	\item The choice of $\Lambda_\chi = 4 \pi F_\pi$ is only a proxy for the chiral scale; other choices are valid.
	\item Here $\mu_0 = 4 \pi F_0$ is a \emph{fixed} renormalization scale.
\end{enumerate}

For the actual  lattice calculation we would prefer to avoid using a fixed renormalization scale, as a fixed renormalization scale would require scale setting. Instead, we use the lattice values of $F_\pi$ and $F_K$ as a proxy for $F_0$ and introduce an NNLO correction to $F_K/F_\pi$ (whose value varies by ensemble) that accounts for the fact that our renormalization scale is not fixed. 

To understand how we can track this correction, we begin with the NLO expression for $F_K/F_\pi$.
\begin{align}
\frac{F_K}{F_\pi} &= \frac{1 + \delta F_K + \cdots}{1 + \delta F_\pi	 + \cdots} \nonumber \\
&\approx 1 + \delta F_K - \delta F_\pi \nonumber \\
&= 1 + \frac58 l^0_\pi - \frac14 l^0_K - \frac38 l^0_\eta +  4 \left( \epsilon^2_K - \epsilon^2_\pi 
\right) \overline L_5
\end{align}
Notice that the log  parts can be rewritten as
\begin{align}
l^0_P &= \epsilon_P^2 \log \left[ \left(\frac{m_P}{\mu_{P^\prime}}\frac{\mu_{P^\prime}}{\mu_0}\right)^2 \right] \nonumber \\
&= l^{P^\prime}_P + \epsilon_P^2 \log \left[ \left( \frac{\mu_{P^\prime}}{\mu_0} \right)^2 \right]
\end{align}
which allows us to rewrite the NLO expression for $F_K/F_\pi$ as
\begin{align}
\frac{F_K}{F_\pi}& =  \left(1 + \frac58 l^{P^\prime}_\pi - \frac14 l^{P^\prime}_K - \frac38 l^{P^\prime}_\eta +  4 \left( \epsilon^2_K - \epsilon^2_\pi 
\right) \overline L_5 \right)
- \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \log \left[ \left( \frac{\mu_{P^\prime}}{\mu_0} \right)^2 \right] \nonumber \\
&= \frac{F_K}{F_\pi} \Big\rvert_{\mu_0 \rightarrow \mu_{P^\prime}}
- \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \log \left[ \left( \frac{\mu_{P^\prime}}{\mu_0} \right)^2 \right] \, . 
\end{align}
The last term is the renormalization scale correction (the $\epsilon^2$ terms have been simplified using the GMOR relation (Eq.~\eqref{eq:gmor}). Available on our lattice are $F_K$ and $F_\pi$, which we use for the sliding renormalization scale. Since $\mu_{P^\prime}/\mu_0 = F_{P^\prime}/F_0 $, we have the following for the NNLO sliding renormalization scale corrections to the $\chi$PT  expression.
\begin{align}
- \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \log \left[ \left( \frac{\mu_\pi}{\mu_0} \right)^2 \right] &\approx
- \frac 32 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right)  \delta F_\pi \nonumber 
&= \delta^\text{NNLO}_{\mu=\mu_\pi} \, \, \, \, \, \, \, \,
\\
  - \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right)  \log \left[ \left( \frac{\mu_K}{\mu_0} \right)^2 \right] &\approx
 - \frac 32 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \delta F_K \nonumber 
 &= \delta^\text{NNLO}_{\mu=\mu_K} \, \, \, \, \, \, \, \,
 \\
    - \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \log \left[ \left( \frac{\mu_K}{\mu_0} \right) \left( \frac{\mu_\pi}{\mu_0} \right)\right] &\approx
  - \frac 34 \left( \epsilon_K^2 -  \epsilon_\pi^2 \right) \left( \delta F_K  + \delta F_\pi  \right) 
  &= \delta^\text{NNLO}_{\mu^2=\mu_\pi \mu_K}
\end{align} 

\section{Correcting for different choices of $\Lambda_\chi$}
In the previous section we considered three different choices for the sliding renormalization scale by using $F_K$, $F_\pi$ or the geometric average $\sqrt{F_K F_\pi}$. We consider the three cases instead of just a single case (say, $\mu = 4 \pi F_\pi$) as this allows us to systematically evaluate the choice of $\mu$ on our fits. Similarly if we choose, for instance, $\mu=4\pi F_K$ for our renormalization scale, it seems sensible to also set $\Lambda_\chi = 4 \pi F_K$ for the chiral symmetry breaking scale. Continuing with the example, this amounts to the following modification of $\epsilon_P$.
\begin{equation}
\frac{m_P}{4 \pi F_\pi} \rightarrow \frac{m_P}{4 \pi F_K}
\end{equation}
The new choice for $\Lambda_\chi$ introduces differences at NNLO. 
\begin{align}
\frac{F_K}{F_\pi} &\approx 1 + \delta F_K - \delta F_\pi \nonumber \\
&= 1 +\left[ \left(\delta F_K - \delta F_\pi \right) \big\rvert_{F^2_\pi \rightarrow F_K^2} \right]
\left(   \frac{F_K}{F_\pi} \right)^2  \nonumber \\
&\approx 1 +\left[ \left(\delta F_K - \delta F_\pi \right) \big\rvert_{F^2_\pi \rightarrow F_K^2} \right]
 \left[  1 + \left(\delta F_K - \delta F_\pi \right)  \right]^2 \nonumber \\
 &\approx \frac{F_K}{F_\pi} \Big\rvert_{F^2_\pi \rightarrow F_K^2} 
 + 2 \left(\delta F_K - \delta F_\pi \right)^2
\end{align}
Therefore we correct for the differences by introducing a term $\delta^\text{NNLO}_{\Lambda_\chi}$. For $F_\pi = 0$, the correction is trivially 0 (as $\Lambda_\chi=4\pi F_\pi$ was assumed to be the cutoff). Adapting the above derivation to use the geometric average instead is straightforward. In summary, 
\begin{align}
\delta^\text{NNLO}_{\Lambda_\chi \rightarrow 4 \pi F_\pi} \, \, \, \, \, \, \, \, \, \, \,&= 0 \nonumber \\
\delta^\text{NNLO}_{\Lambda_\chi \rightarrow 4 \pi F_K}    \, \, \, \, \, \,  \, \, \, \, &= 2 \left(\delta F_K - \delta F_\pi \right)^2 \nonumber \\
\delta^\text{NNLO}_{\Lambda_\chi \rightarrow 4 \pi \sqrt{F_\pi F_K}} &=  \left(\delta F_K - \delta F_\pi \right) ^2 \, .
\end{align}