\chapter{LATTICE PARTICULARS \& ENSEMBLE DATA}
\label{app:lattice_particulars}

Recall from the introduction that there are numerous way to discretize QCD (a few different choices are shown in Table~\ref{tab:lattice_actions_comparison}). However, that doesn't mean all choices are equally \emph{good}---each choice has its own set of advantages and drawbacks. For example, the action derived in Chapter~\ref{chap:qcd} (known as the naive action) has a couple pesky problems: first, the fermions do not respect chiral symmetry even in the massless limit; second, for each fermion described by the action, there are 15 extra fermions in what is dubbed the fermion doubling problem. Wilson demonstrated how to remove the doublers~\cite{Wilson:1974sk}, but even so the fermions did not respect chiral symmetry.

\begin{table}[]
    \begin{tabular}{l|lll|l}
        \hline\hline
        \begin{tabular}[c]{@{}l@{}}Fermion\\ action\end{tabular} & Doublers & Local & \begin{tabular}[c]{@{}l@{}}Chiral\\ symmetry\end{tabular} & Cost \\ \hline
        Naive & {\color{RubineRed} Yes (16)}  & Yes & {\color{RubineRed} No} & Cheap \\
        Wilson-Clover & No & Yes  & {\color{RubineRed} No} & Cheap \\
        {\color{ProcessBlue} Staggered} & {\color{RubineRed} Yes (4)}   & {\color{RubineRed} No}  & {\color{Orchid} Some} & Cheap \\
        {\color{JungleGreen} Domain Wall} & No & Yes & Yes & Expensive \\
        Overlap & No & {\color{RubineRed} No} & Yes & Expensive  \\
        \hline\hline
    \end{tabular}
    \caption{\label{tab:lattice_actions_comparison}
    A few different fermion actions for lattice QCD. We use a mixed-action, with {\color{JungleGreen} domain wall} fermions in the valence sector and {\color{ProcessBlue} staggered} quarks in the sea.}
    
\end{table}

In fact, this is not from a lack of cleverness (as if anyone would have the audacity to accuse Wilson of such). The Nielsen-Ninomiya theorem~\cite{Nielsen:1981hk} states that, given an even dimensional lattice gauge theory, it is impossible to build a lattice action that simultaneously (1) has no fermion doublers, (2) acts locally (i.e., only nearest neighbors can interact), and (3) respects chiral symmetry. 

That said, there is a loophole here, as the Nielsen-Ninomiya theorem only applies to \emph{even} dimensional theories. It is possible to build an odd dimensional theory that satisfies all three \emph{desiderata}, with the fermions we're interested in living on the 4-dimensional surface of this 5-dimensional volume; this is roughly the philosophy behind domain wall fermions.

Beyond mathematical restraints, however, there are funding constraints the aspiring lattice practitioner must consider: the computational price for each fermion action is different. Moreover, since these actions are simulated on half-billion dollar high-performance computers, these computations costs have palpable financial costs, too. For reference, it is roughly an order of magnitude more expensive to simulate a domain wall fermion than a staggered fermion.

Our collaboration uses a mixed action, that is, we employ a different action for the sea and valence quarks. For the sea quarks, we use staggered fermions (with MILC providing many of the staggered quark configurations \cite{MILC:2010pul,MILC:2012znn}). For the valence sector, we use domain wall fermions~\cite{Berkowitz:2017opd}.  

These projects use data from approximately 20 ensembles spanning 7 pion masses and multiple lattice spacings (see Fig.~\ref{fig:app_ensemble_data}). 

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{./figs/appendices/ensemble_data.pdf}
  
    \caption{\label{fig:app_ensemble_data} 
    Ensembles employed in these projects. The pion masses range from $\sim 130$ MeV to $\sim 400$ MeV, while the lattice spacing range from $\sim 0.06$ fm to $\sim 0.15$ fm. The strange quark masses are generally tuned near their physical point values, with a few lighter-than-physical strange quarks such that we have the option to track the slight mistuning.
    }
\end{figure}