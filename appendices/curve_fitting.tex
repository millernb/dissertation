\chapter{SUMMARY OF CURVE FITTING TECHNIQUES}
\label{app:curve_fitting}

%\section{The method of least squares}
%\cite{Lepage:2001ym}

\section{General considerations when extrapolating an observable to the physical point}

In this thesis we have performed extrapolations of chiral observables (i.e., observables that have \xpt expressions). The procedure for extrapolating a chiral observable is nearly identical for a given observable modulo the particular fit function. We summarize the procedure here. 

Given an observable $O$ with mass dimension 0, the generic extrapolation function can be written
\begin{equation}
    O^\text{fit} = O^\text{chiral} + O^\text{disc} \, . 
\end{equation}
Here $O^\text{chiral} = O^\text{chiral}(\{m_{G_i}\})$ denotes the collection of terms that depend strictly on the pseudo-Goldstone bosons, while $O^\text{disc} = O^\text{disc}(a, L, \ldots)$ denotes the remaining terms which arise from having employed the lattice. 

The $O^\text{chiral}$ terms come in two varieties in this thesis: either they are derived from \xpt or they are a Taylor expansion, the latter class being primarily used to test the ``goodness'' of the \xpt fits. For example, we might have 
\begin{align}
    O^\text{chiral} =&
    O_0 &\left(\mathcal{O}(\epsilon^0)\right)  \\
    &+ \sum_{G \in \{\pi, K\}}  \epsilon^2_G \alpha^{(2)}_G 
    &\left(\mathcal{O}(\epsilon^2)\right) \nonumber \\
    &+ \sum_{G \in \{\pi, K\}}  \epsilon^4_G \alpha^{(4)}_G 
    &\left(\mathcal{O}(\epsilon^4)\right) \nonumber \\
    &\qquad + \epsilon^4_\pi \beta^{(4)}_\pi \log \epsilon^2_\pi \nonumber \\
    &+ \sum_{G \in \{\pi, K\}}  \epsilon^6_G \alpha^{(6)}_G
    &\left(\mathcal{O}(\epsilon^6)\right) \nonumber \\
    & \qquad + \epsilon^6_\pi \beta^{(6)}_\pi \log \epsilon^2_\pi + \epsilon^6_\pi \gamma^{(6)}_\pi \left(\log \epsilon^2_\pi\right)^2  \nonumber
\end{align}
where $\epsilon_G = m_G /\Lambda_\chi$ and the rest are low-energy constants (LECs). When we exclude the $\log \epsilon^2$ terms from our fit, the model reduces to a Taylor expansion.

We have truncated this model at $\mathcal O(\epsilon^6)$, but there are in principle infinitely many more terms we could add; however, we expect that at some order the signal will be too weak to fit. The reasoning applies to the lower order terms, too. Even though the literature might provide chiral expression up to order $\mathcal O(\epsilon^6)$, that doesn't mean we'll necessarily be able to fit it. 

In general, we take a model-agnostic approach when fitting, considering different truncations of our chiral models and averaging over the different models assuming equal weight (see the next section). Occasionally we might exclude a model if we have good \emph{a priori} belief that it is not physical. In the case above, we might consider the models
\begin{align}
\begin{array}{rl}
    + 1:& \text{Taylor }\mathcal{O}(\epsilon^2) \\
    + 2:& \text{Taylor } \mathcal{O}(\epsilon^4) + \{ 0, \log \epsilon^2 \}\\
    + 3:& \text{Taylor } \mathcal{O}(\epsilon^6) + \{ 0, \log \epsilon^2, (\log \epsilon^2)^2  \}\\
    \hline
    6:& {\text{chiral choices}}
\end{array}
\end{align}
In this case, we're comparing Taylor models with/without $\chi$PT-motivated logarithms. Note that even though the $(\log \epsilon^2)^2$ term is also a \xpt term at $\mathcal O(\epsilon^6)$, the signal might nevertheless be too weak to fit, depending on the size of the LEC.

Lattice artifacts primarily stem from the size of the lattice spacings. These artifacts are accounted for by using a Taylor Ansatz, for example
\begin{equation}
    O^\text{disc} 
    \supset d_a \epsilon_a^2 
    +  \left( 
        d_{aa} \epsilon_a^2 + d_{al} \epsilon_\pi^2 + d_{as} \epsilon_K^2
    \right) \epsilon_a^2 
    + \cdots 
\end{equation}
where $\epsilon_a = a/2w_0$ is a dimensionless proxy for the lattice spacing (see Chapter~\ref{chap:scale_setting} for the definition of $w_0$). The discretization errors start at $\mathcal O(\epsilon_a^2)$ as a consequence of the particular lattice action we employ~\cite{Sharpe:2007yd}. 

The next obvious discretization effect comes from the lattice's finite volume. Although the real world might be infinite, the lattice certainly isn't---the typically lattice is only a few femtometers across, so it is possible for virtual particles to bump into the ``edge" of the universe. Or at least it would be, were it not for the fact that our boundary conditions are periodic. Loop integrals in the effective field theory must consequently be adjusted for finite volume, as now particles can ``wrap around'' before interacting. Typically this means replacing a $\log$ with a $\log$ plus Bessel function.  

Additionally $O^\text{disc}$ might include terms that are specific to our choice of lattice action, such as radiative corrections. 

Once the models have been decided on and the fits have been performed, the final step is to extrapolate to the physical point, which is defined as the continuum, infinite volume limit where observables take their physical/PDG values. As our lattice assumes the SU(2) limit, we take the isospin average of observables as needed, e.g. $m_\pi = (m_{\pi^-} + m_{\pi^0} + m_{\pi^+})/3$.

All fits are performed using the software packages \texttt{gvar} and \texttt{lsqfit}~\cite{Lepage:2001ym, gvar, lsqfit}, which implement nonlinear least squares regression with (Bayesian) priors.

\section{Model averaging}
Typically when we extrapolate observables in our work, we consider many different models. Rather than choose the ``best'' model (which could lead to bias or underestimate our uncertainty), we instead perform a model average over the different models employed in our work.

Our averaging procedure is performed under a Bayesian framework, following the procedure described in \cite{Chang:2018uxx,Miller:2020xhy} and with greater detail in \cite{Jay:2020jkz}. Suppose we are interested in estimating the posterior distribution of $Y = M_{\Xi}$, i.e. $P(Y|D)$. To that end, we must marginalize over the different models $M_k$.
\begin{equation}
    P(Y|D) = \sum_k P(Y | M_k, D) P(M_k | D)
\end{equation}
Here $P(Y | M_k, D)$ is the distribution of $Y$ for a given model $M_k$ and dataset $D$, while $P(M_k | D)$ is the posterior distribution of $M_k$ given $D$. The latter can be written, in accordance to Bayes theorem, as
\begin{equation}
    P(M_k | D) = \frac{P(D | M_k) P(M_k)}{\sum_l P(D | M_l) P(M_l)} \, .
\end{equation}
We can be more explicit with what the latter is in the context of our fits. First, mind that we are \emph{a priori} agnostic in our choice of $M_k$. We thus take the distribution $P(M_k)$ to be uniform over the different models. We calculate $P(D | M_l)$ by marginalizing over the parameters (LECs) in our fits:
\begin{equation}
    P(D | M_k) = \int \prod_j \text{d} \theta_j^{(k)} \,  P(D | \theta_j^{(k)}, M_k) P(\theta_j^{(k)} | M_k) \, .
\end{equation}
After marginalization, $P(D | M_k)$ is just a number. Specifically, it is the Bayes factor of $M_k$: $P(D | M_k) = \exp(\texttt{logGBF})_{M_k}$. Thus
\begin{equation}
    P(M_k | D) = \frac{\exp(\texttt{logGBF})_{M_k}}{K \sum_l \exp(\texttt{logGBF})_{M_l}}
\end{equation}
with $K$ the number of models included in our average.

Suppose $A$ and $B$ are statistics computed on models $\{M_k\}$ (e.g., $A=M_\Xi$, $B=M_{\Xi^*}$, and $M_k=$ ``a simultaneous fit of $M_\Xi$ and $M_{\Xi^*}$ to N$^2$LO in the $\chi$PT expansion"). The model average of an observable (e.g., $A$) is straightforward.
\begin{equation}
    \text{E}[Y] = \sum_k \text{E}[Y | M_k] \, P(M_k | D)
\end{equation}
The model-averaged covariance between $A$ and $B$ is given by 
\begin{align}
    \text{Cov}\left[A, B\right] &= \braket{AB} - \braket{A}\braket{B} \\ %
    &= \sum_k \braket{AB}_k p(M_k | D) 
    - \left(  \sum_k \braket{A}_k p(M_k | D) \right) \left(  \sum_k \braket{B}_k p(M_k | D) \right) \nonumber \\
    &= \sum_k \text{Cov}[A,B]_k \, p(M_k | D) 
    + \sum_k \braket{A}_k \braket{B}_k p(M_k | D) \nonumber \\
    &\qquad - \left(  \sum_k \braket{A}_k p(M_k | D) \right) \left(  \sum_k \braket{B}_k p(M_k | D) \right) 
    \nonumber
\end{align}
where $\braket{X}_k$ is the expectation value of $X$ on $M_k$. Notice that when $A=B$ this expression reduces to the variance expression in the references.

Finally, we add a caveat about weights. The normalized Bayes factors $p(M_k | D)$ (``the weights'') can only be compared for models sharing the same response data $D$. In practice, this means that we can only compute the weights for models that were fit simultaneously. In principle the covariance between two observables not fit together (e.g., $M_\Omega$ and $M_\Xi$) needn't be 0 since the could share the same set of explanatory data (e.g., $m_\pi$), but this covariance is comparably small and can be approximated as vanishing. We expect the bulk of the correlation to arise from shared LECs.


\section{The empirical Bayes method}

The empirical Bayes method allows us to estimate the prior distribution from the data; in that sense it is not a truly "Bayesian" approach, as the choice of prior is not data-blind. Nevertheless, it can serve as a useful point of comparison when evaluating the reasonableness of our priors.

Typically when we think of a model for a chiral expression, we imagine this to mean the choice of fit function (e.g., $M = $ ``a Taylor expansion to $\mathcal O(m_\pi^4)$''); however, we can extend the definition of a model to also include the prior. Let us therefore denote $M = \{ \Pi, f\}$ a candidate model for performing the extrapolation of some observable, where $f$ is the extrapolation function and $\Pi$ is the set of priors for the LECs. By Bayes' theorem, the most probable $\Pi$ for a given $f$ and dataset $D$ is

\begin{equation}
	{\color{RubineRed} p(\Pi | D, f)}
	= \frac{{\color{ProcessBlue} p(D |\Pi, f)} {\color{JungleGreen} p(\Pi | f)}}{p(D | f)} \, .
\end{equation}

Here we recognize ${\color{ProcessBlue} p(D |\Pi, f)}$ to be the familiar likelihood function and $p(D | f)$ to be some unimportant normalization constant. The curious term is the hyperprior distribution ${\color{JungleGreen} p(\Pi | f)}$, which parameterizes the distribution of the priors. We restrict our priors to the form $\pi(c_i) = N(0, \sigma^2_j)$ for LEC $c_i$, where the index $j$ denotes some blocking of the LECs. For example, one might use the chiral/discretization split
\begin{equation}
    \Pi = \begin{cases}
        \pi(c_\chi) &= N(0, \sigma^2_1) \\
        \pi(c_\text{disc}) &= N(0, \sigma^2_2)
    \end{cases} \, .
\end{equation}
The hyperprior ${\color{JungleGreen} p(\Pi | f)}$, in this context, parameterizes the $\sigma_j$. We then vary $\sigma_j$ uniformly on the interval $[ \sigma_j^\text{min}, \sigma_j^\text{max} ]$ (in this work, $\sigma_j^\text{min} = 0.01$ and $\sigma_j^\text{max}=100$). As we expect the LECs to be of order 1, we do not expect the optimal values of $\sigma_j$ to lie near the extrema. However, if they do, we should reflect on whether the terms are disfavored by the data ($\sigma_j \sim \sigma_j^\text{min}$) or the LEC is much greater than expected ($\sigma_j \sim \sigma_j^\text{max}$).

Because the hyperprior distribution is uniform, we see that the peak of the posterior ${\color{RubineRed} p(\Pi | D, f)}$  occurs at the peak of the likelihood function ${\color{ProcessBlue} p(D |\Pi, f)}$. Thus the empirical Bayes procedure is straightforward: we find the set of priors that maximizes the likelihood function. But there is one general caveat here. We reiterate that we have blocked the LECs together. One might instead be tempted to optimize each LEC individually; however, this would be an abuse of empirical Bayes---by varying too many parameters, the uniformity assumption can no longer be made in good faith. We emphasize that the empirical Bayes method is not a substitute for careful consideration when setting priors!


\section{Plotting lattice data by shifting their values}
In general, the fit function for some observable $O$ depends on multiple parameters ($m_\pi$, $m_K$, $a$, etc.), so the curve we're fitting is not a line but some multidimensional surface, which would generally be intractable to plot. It is significantly easier to interpret a plot of $O$ versus a single parameter, but this means carefully taking a slice of that surface. Fortunately, this is fairly easy: we simply fix the other parameters to their physical point values while allowing the single parameter to vary.

Of course, we're not interested in plotting solely the fit; we're also interested in plotting the data, which gives us a visual indication of goodness of fit. However, this is tricky since most data is not generated at their physical values. We therefore require some heuristic for ``shifting" the lattice data from their ensemble values to the physical point values. 

We accomplish this task in the following manner. Suppose we're interested in including on our plot the ensemble value $O_e$ at a given ensemble value of a variable $\boldsymbol w_e$ (e.g., $\boldsymbol w_e = m_\pi/\Lambda_\chi$ on \texttt{a12m220}). Denote the values of the remaining explanatory variables on that ensemble by $\boldsymbol x_e$. Then on each ensemble, we replace $O_e$ with $O^\text{shifted}_e$, defined by
\begin{equation}
    O^\text{shifted}_e =
    O_e
    + \hat O(\boldsymbol w, \boldsymbol x_\text{phys})
    - \hat O(\boldsymbol w, \boldsymbol x)
\end{equation}
where $\hat O$ is the fit function parameterized by the fit's posterior. 