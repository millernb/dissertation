\chapter{Scale Setting}
\label{chap:scale_setting}

Quantities generated on the lattice are dimensionless; however, many observables that we're interested in studying are not. To convert our dimensionless observables generated on the lattice into dimensionful quantities, we must introduce a scale. This process is known as \emph{scale setting}. 

The work described in this section resulted in the following publication.

\begin{itemize}
	\item[] N. Miller et al., Phys. Rev. D 103, 054511 (2021), arXiv:2011.12166 [hep-lat].
\end{itemize}

\section{Measuring dimensionful observables on the lattice}

As motivation for scale setting, let us review how to extract the mass of a baryon from the lattice. In Chapter \ref{chap:qcd}, we saw how to calculate the correlator of two observables using the Euclidean path integral.
\begin{equation}
	\label{eqn:corr_fcn_path_integral}
\langle O_2(t) O_1(0) \rangle =
\frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[U] \, e^{-S_F[q, \overline q, U]-S_G[U]} O_2[q_{(t)}, \overline{q}_{(t)}, U_{(t)}] O_1[q_{(0)}, \overline{q}_{(0)}, U_{(0)}]
\end{equation}
This (Euclidean) path integral is sampled using Monte-Carlo to determine the two-point function on the lattice. However, there is another way one might defined the correlation function. As operators (rather than functionals), the correlation function is defined as
\begin{equation}
\langle O_2(t) O_1(0) \rangle
 = \lim\limits_{T \rightarrow \infty} \frac{1}{Z_T} \text{tr} \left\{ e^{-(T-t)\hat{H}} \hat{O}_2 e^{-t\hat{H}} \hat{O}_1 \right\}
\end{equation}
where $Z_T = \text{tr} \left[ e^{-T \hat H}\right]$. From this definition, we see that by introducing a complete set of energy eigenstates, we can rewrite this correlation function in terms of its energy spectrum.

\begin{align}
	\langle O_2(t) O_1(0) \rangle
	&= \sum_{m, n} \braket{m | e^{-(T-t) \hat H} \hat O_2| n} 
	\braket{n | e^{-t \hat H} \hat O_1| m} \\
	&\approx \sum_{n} \braket{0 | \hat{O}_2 | n} \braket{n | \hat{O}_1 | 0} e^{-t E_n} \qquad \text{as $T \rightarrow \infty$}
	\label{eqn:baryon-correlation-fcn}
\end{align}

The above formula is quite general, but it doesn't tell us how to calculate the mass of some particular baryon. To do that, we replace $\hat O_1, \hat O_2$ with operators that create a baryon from the vacuum at $t=0$, which is then destroyed at $t=t$.
\begin{equation}
\braket{O_2(t) O_1(0)}
\rightarrow \braket{ \Omega | B(t) B^\dagger(0) | \Omega }
\end{equation}
Here $\Omega$ is the QCD vacuum and $B^\dagger$ is an operator that creates an excitation with the quantum numbers of the specific baryon we're interested in studying. 

To summarize, we can estimate the mass of a baryon with
\begin{equation}
\braket{\Omega | B(t = a n_t) B^\dagger(0) | \Omega}
\approx \sum_n A_n e^{-(n_t) (a E_n)} \, .
\end{equation}
The expression on the left-hand side is simulated on the lattice via \eqref{eqn:corr_fcn_path_integral} for different values of $t$; the parameters $A_n, aE_n$  on the right-hand side are determined by an $n$-state fit. Notice that we have substituted $t=a n_t$ since the separations in time, like the separations in space, are discretized. In fact, we do not even need to know what the times are: if $L_t$ is the ``length'' of the lattice in the $t$-direction, then we only require $n_t \in \{ 0, 1, \ldots, L_t/a \}$. The parameters we extract from this fit are therefore purely dimensionless.

Most lattices are simulated with quark masses tuned away from their physical (experimental) values; we therefore expect the baryon mass to differ from the experimental value also. If we know the lattice spacing, we can convert the dimensionless parameter from the two-point function fit into a dimensionful parameter. Reintroducing factors of $c$ and $\hbar$ temporarily, we must calculate
\begin{equation}
E_0^\text{phys} = \frac{ \hbar c}{a} (aE_0)^\text{latt} \, .
\end{equation}
But here's the snag: we generally do not know the lattice spacing \emph{a priori}.

\section{Lattice spacing dependence of the coupling}

Recall that QCD has $N_f+1$ degrees of freedom: one per each fermion flavor (up, down, strange, charm, bottom, and top) plus the gauge coupling $g$. On the lattice, one usually simulates less than the full $N_f=6$ theory, as the heavier quarks play a negligible roll in the low-energy regime. In this work, we simulate $N_f=2+1+1$ quarks (here the 2 denotes that the up and down quarks are degenerate, i.e. we take the isospin limit). Thus we have 3+1 parameters we must tune.

To set the quark masses, we can tune the masses until the ratio of the masses of hadrons with different quark content agree with their physical values, e.g. by tuning the ratios $m_\pi/M_p$, $m_K/M_p$, and $m_{D_s}/m_p$ until they agree with the PDG (see the next section).

For the gauge coupling, we realize that even in quark-less QCD (i.e., gluons only) we must still determine the running coupling constant which changes according to the renormalization scale through a process known as dimensional transmutation. Since the only dimensionful quantity available is the lattice spacing, determining the lattice spacing must be equivalent to determining $g$.  


\section{Physical versus theory scales}

The most obvious candidates for scale setting are observables known precisely from experiment, e.g. the nucleon mass. Scale setting observables which depend on comparison with experiment are known as \emph{physical} scales. Ideally a scale setting observable should be precise and easy to generate on the lattice, yet also have small systematics and depend only weakly on the quark mass~\cite{Sommer:2014mea} (a weak quark mass dependence minimizes errors from extrapolation or quark mass mistuning). However, physical scales tend to lack one of those desirable properties, so they are not ideal for scale setting. 

To rectify this deficiency, lattice QCD practitioners have invented \emph{theory} scales. Unlike physical scales, theory scales are impractical---if not outright impossible---to measure with experiment alone. Nevertheless, they can easily be measured on the lattice. We give an example of a physical scale and a theory scale below.

\subsection{A physical scale: scale setting with the proton}

As an example of scale setting with a physical scale, let us consider scale setting with the proton loosely following the procedure described in \cite{Durr:2008zz}. We assume that we have already extracted the pion, kaon, and proton masses from their associated two-point functions on multiple ensembles, spanning multiple quark masses but only at a single lattice spacing. For illustration, suppose we use the following Taylor ans\"atze in the quark masses (in general, more sophisticated expressions can be derived using chiral perturbation theory).
\begin{align}
a m_\pi &= c_\pi^{(0)} + \sum_{n, f} c_\pi^{(n, f)} am_{q_f} \nonumber \\
a m_K &= c_K^{(0)} + \sum_{n, f} c_K^{(n, f)} am_{q_f} \\
a M_p &= c_p^{(0)} + \sum_{n, f} c_p^{(n, f)} am_{q_f} \nonumber
\end{align}
Finally, let us assume that we're working in the isospin limit ($u=d$) and that the contribution from the charm quark is negligible such that $f\in \{u, s\}$ in the sum above.

To determine the lattice spacing, we must first fit the above expressions to determine the low energy constants $c^{(n, f)}_{\{\pi, K, p\}}$. We would like to know which values of $m_{q_f}$ cause the mass expressions to match the experimental values; we achieve this by solving for $m_{q_f^*}$ such that
\begin{equation}
\left.\frac{am_\pi}{aM_p} \right\vert_{m_{q_f^*}} = \frac{m_\pi^\text{exp}}{M_p^\text{exp}}
\qquad \text{and} \qquad
\left.\frac{am_K}{aM_p} \right\vert_{m_{q_f^*}} = \frac{m_K^\text{exp}}{M_p^\text{exp}}\, .
\end{equation}

Finally the lattice spacing $a$ is found by extrapolating $aM_p$ to ``physical'' quark masses and comparing against the experimental value. 
\begin{equation}
a = \left.\frac{a M_p^\text{fit}}{M_p^\text{exp}} \right|_{m_{q_f^*}}
\end{equation}
We then repeat this procedure for each lattice spacing. 

\subsection{A theory scale: scale setting with the Sommer parameter $r_0$}
As hinted at before regarding physical scales, there is a flaw with using the proton for scale setting: the proton correlator has a nasty signal-to-noise problem. To understand why, consider the variance of the proton correlator
\begin{equation}
	\label{eq:correlator_error}
	\text{Var}\left[C(t)\right] 
	= \underbrace{\text{E} \left[ C(t)C^\dagger(t)\right]}_{\sim \braket{(u u d)(\overline u \overline u \overline d)}} 
	- \text{E} \left[C(t)\right]^2 \, .
\end{equation}
The first term tells us that the variance is sensitive to the particles formed from 3 quarks and 3 anti-quarks. It is possible for a proton-antiproton pair to form, but this is rather heavy ($\sim 2 M_p$). A much lighter (and hence more energetically favorable) configuration is 3 pions, so we expect $\braket{|C(t)|^2} \sim e^{-3 m_\pi t}$. This term will be much larger than the second term in Eq.~\eqref{eq:correlator_error}, so the signal-to-noise ratio is approximately given by
\begin{equation}
	\frac{\text{E} \left[C(t) \right]}{\sqrt{\text{Var}\left[ C(t)\right]}}
	\sim
	e^{-\left(M_p - \frac 32 m_\pi \right)t}
\end{equation}

We expect the signal-to-noise ratio to shrink with lattice time. In fact, this is an issue with all baryons, albeit to varying degrees; mesons, however, do not suffer so, as the lightest combinations of quarks in the first term of Eq.~\eqref{eq:correlator_error} will still be mesonic.

As an alternative to scale setting using baryon masses, Sommer proposed a scale setting technique using the static QCD potential~\cite{Sommer:1993ce}. Compared to scale setting with a baryon, there is no signal-to-noise problem. We note that the static quark potential is given by \cite{Gattringer:2010zz}
\begin{equation}
V(r) = A + \frac Br + \sigma r
\end{equation}
where the $B/r$ term corresponds to a Coulomb-like potential and the $\sigma r$ term corresponds to the string tension/confinement (for reference, $\sigma\approx 900$~MeV/fm; the constant term $A$ is included for completeness, but it is irrelevant for our purposes). This effects a force between two static quarks,
\begin{equation}
F(r) = \frac{d}{dr} V(r) = -\frac{B}{r^2} + \sigma \, .
\end{equation}
(Convention omits the negative sign in the derivative.) We would like to use this potential to set the scale; however, we would also like to avoid an extrapolation in $r$. To that end, we instead consider
\begin{equation}
	r_0^2 F(r_0) = c \, .
\end{equation}
with a fixed value of $r=r_c$. When $m_q \gg \Lambda_\text{QCD}$, we can estimate the effect of this potential by using the (nonrelativistic) Schr\"odinger equation~\cite{Mansour:2018wql}, which can then be compared to experimental measurements of $\overline c c$ and $\overline b b$ states. Sommer suggested setting $c=1.65$, which would correspond to $r_0 \approx 0.49$ fm. Solving for $r_0$, we find that
\begin{equation}
r_0 = \sqrt{\frac{c + B}{\sigma}} \, .
\end{equation}
Dividing both sides by the lattice spacing $a$ and rearranging the equation, we have the following relation
\begin{equation}
\label{eqn:a_from_sommer}
a = r_0 \sqrt{\frac{\sigma a^2}{c + B}} \, .
\end{equation}
Notice that $B$ and $\sigma a^2$ are both dimensionless quantities, suggesting that they might be accessible on the lattice. Let us now focus on determining these parameters. 

We begin by noting (see Chapter 3 of \cite{Gattringer:2010zz} for details) that the static quark potential is related to the expectation value of the Wilson loop $W_\mathcal L$, a generalization of the plaquette (Eq. \eqref{eqn:plaquette}) to more general paths.
\begin{equation}
\left\langle W_\mathcal L \right\rangle \approx C e^{-t V(r)} = C e^{-(n_t) (aV(an))}
\end{equation}
Here we have substituted $r, t$ for the lattice values: $r=an$ and $t=an_t$. Suffice it to say that the expression on the left-hand side of this equation can be readily calculated on the lattice. By varying $n_t$ (but fixing $n$), we can fit this equation to determine the parameters $C$ and $aV(r=an)$. We then repeat this process varying $n$ such that we extract values of the potential $aV(r=an)$ for $n \in \{0, 1, \ldots, L/a\}$.

Finally we fit $aV(an)$ as a function of $n$ to determine $B$ and $\sigma a^2$. 
\begin{equation}
	a V(an) = Aa + \frac Bn + \sigma a^2 n
\end{equation}
These parameters can then be substituted into Eq.\ \eqref{eqn:a_from_sommer}, thus determining $a$.

\section{Scale setting with the gradient flow}
In our work, we use a theory scale derived from gradient flow instead of the static potential. In addition to being cheap to compute and highly precise, the gradient flow-derived scales, in contrast to the potential-derived scales, have an expression in chiral perturbation theory. But first we should describe what gradient flow actually is.

We define a gradient flow field $B_\mu$ which diffuses according to~\cite{Luscher:2010iy}
\begin{align}
	\dot B_\mu &= D_\nu G_{\nu \mu}  
	&\left.B_\mu \right|_{t=0} &= A_\mu \\
	G_{\mu \nu} &= \partial_\mu B_\nu - \partial_\nu B_\mu + \left[ B_\mu, B_\nu \right]
	& D_\mu &= \partial_\mu + \left[B_\mu, \, \cdot\,  \right]
\end{align}
where $A_\mu$ is the QCD gauge field, $B_\mu$ describes the flow of $A_\mu$, and $\dot B_\mu$ denotes the derivative of $B_\mu$  with respect to the flow time $t$. Critically, the flow time has mass dimension -2, making it possible for us to use it to set the scale. Observe that these flow equations must drive the gauge fields towards stationary points of the action. 

Now we define $E = \frac 14 G^a_{\mu \nu} G_a^{\mu \nu}$, which reduces to the gluonic energy density in the limit $t\rightarrow 0$. So long as the flow time satisfies $a \ll \sqrt{8 t_0} \ll aL$, we can approximate the expectation value as
\begin{equation}
	\braket{E} = \frac{3}{4 \pi t^2} \alpha_\text{S}(Q^2) \left( 1 + k_1 \alpha_\text{S}(Q^2) + \cdots \right)
\end{equation}
where $\alpha_\text{S}(Q^2)$ is the running coupling. The left-hand side is something we can calculate on the lattice for relatively cheap since it doesn't depend on the fermion action. 

If we multiply this quantity by the flow time squared $t^2$, we see that quantity is proportional to the coupling at leading-order. This is the motivation for defining the gradient flow scales $t_0$ and $w_0$. Empirically this quantity is observed to be nearly constant around $t^2 \braket{E}=0.3$, so we define $t_0$ to be the flow time at which this condition is satisfied. 

\begin{align}
	t^2 \braket{E(t)} \Big|_{t=t_{0, \text{orig}}} &= 0.3 \\
	W_\text{orig} = t \frac{d}{dt} \left(t^2 \braket{E(t)}\right) \Big|_{t=w_{0, \text{orig}}^2} &= 0.3
\end{align}
Here we have also included a related quantity, $w_0$,  which ameliorates artifacts introduced by the transition of $\braket{E}$ from a $1/t^2$ dependence to a $1/t$ dependence at larger flow times (including times near $t_0$)~\cite{Borsanyi:2012zs}. These equations are how the gradient scales were originally defined (hence the subscript ``orig'').

Additionally, Fodor et al.~\cite{Fodor:2014cpa} proposed the following ``improved" definitions (still at tree level) which remove some of the lattice spacing dependence of these scales.
\begin{align}
	\frac{t^2 \braket{E(t)}}{1 + \sum_n C_{2n} \frac{a^{2n}}{t^n}} \Bigg|_{t=t_{0, \text{imp}}} &= 0.3 \\
	t \frac{d}{dt} \left(\frac{t^2 \braket{E(t)}}{1 + \sum_n C_{2n} \frac{a^{2n}}{t^n}} \right) \Bigg|_{t=w_{0, \text{imp}}^2} &= 0.3
\end{align}
Here the $C_{2n}$ coefficients are just some rational numbers that were determined in~\cite{Fodor:2014cpa}.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./figs/scale_setting/a15m135XL_s_w0_orig.pdf}
	\caption{\label{fig:scale_W_field_plot}
	Measuring $w_0/a$ on one of the ensembles. Notice that the derivative is practically constant near the point at which $W_\text{orig} = 0.3$ 
	Figure from~\cite{Miller:2020evg}.
	}
\end{figure}

Finally, because $\braket{E}$ is just the kinetic gauge term, it is automatically invariant under chiral transformations. This implies the existence of a chiral expansion for $\braket{E}$, which in turn yields a chiral expansion for $t_0$ and $w_0$.


\section{Project goals}
Ultimately we would like to calculate the gradient flow scales $w_0$ and $t_0$, which will allow us to set the scale of our lattice. We accomplish this task by generating the gradient flow scales on each lattice ensemble (each of which is tuned for a different light quark/pion mass and have different lattice spacing sizes), then extrapolate from those lattice ensembles down to the physical point.
\footnote{See Appendix~\ref{app:lattice_particulars} for the ensemble data as well as a discussion of our lattice action.}

However, we cannot extrapolate directly the gradient flow scales---these are dimensionful quantities, after all. Instead we need to construct dimensionless quantities from the gradient flow scales and some dimensionful observable. For example, consider $w_0$, which has the following N$^2$LO \xpt expression~\cite{Bar:2013ora}.
\footnote{Throughout this thesis, we will assume the renormalization scale in loop integrals (logarithms) is chosen such that $\mu = \Lambda_\chi = 4 \pi F_\pi$ unless otherwise noted. This choice has limited impact on our extrapolations other than shuffling the values of the LECs. See Appendix~\ref{app:fk_fpi_extras}.}
\begin{equation}
	w_0 = w_{0, \text{ch}} \left( 
		1 + c_1 \epsilon_\pi^2 + c_2 \epsilon_\pi^4 + c_3 \epsilon_\pi^4 \log \epsilon_\pi^2
	\right)
\end{equation}
(Here we have defined the expansion parameter $\epsilon_\pi = m_\pi/\Lambda_\chi$, where $\Lambda_\chi = 4 \pi F_\pi$ is the chiral cutoff. The parameters $c_i$ and $w_{0, \text{ch}}$ are low-energy constants, with $w_{0, \text{ch}}$ in particular being the value of $w_0$ in the chiral limit, i.e. $m_q = 0$.) On the lattice, we do not generate $w_0$; instead we generate the dimensionless quantity $w_0/a$. If we divide both sides by the lattice spacing, we might think we have something more amenable to a fit.
\begin{equation}
	\label{eq:w0_interpolation}
	\frac{w_0}{a} = \frac{w_{0, \text{ch}}}{a} \left( 
		1 + c_1 \epsilon_\pi^2 + c_2 \epsilon_\pi^4 + c_3 \epsilon_\pi^4 \log \epsilon_\pi^2
	\right)
\end{equation}

There are a couple problems with this approach, however. First there is the obvious question: what does it even mean to extrapolate this quantity to the physical point, which includes the limit $a=0$? Since $w_0$ is finite, the quantity on the right-hand side will blow up. The proposal to extrapolate $w_0/a$ to the continuum limit is a non-starter. 

Second, we have changed the low-energy constant (LEC) $w_{0, \text{ch}}$ to $w_{0, \text{ch}}/a$. Notably, this LEC now also depends on the lattice spacing. Thus Eq.~\eqref{eq:w0_interpolation} is only useful if we're fitting ensembles that have (approximately) the same lattice spacing. 

However, none of these comments are meant to imply that Eq.~\eqref{eq:w0_interpolation} is useless. On the contrary, we can use Eq.~\eqref{eq:w0_interpolation} to determine the lattice spacings if we've already determined $w_0$ by some other method through the relation
\begin{equation}
	\label{eq:w0_lattice_spacing}
	a = \left.\frac{w_0}{\left(w_0/a\right)^\text{fit}}\right|_\text{physical point} \, .
\end{equation}

But first we must determine $w_0$. Notice that if we multiply $w_0$ by an observable with mass dimension 1---the $\Omega$ baryon mass, for example---then we will have something dimensionless. But like $w_0$, we do not directly generate $M_\Omega$ on a lattice but rather $aM_\Omega$; however, we see that the factors of $a$ will cancel. Therefore if we instead extrapolate the quantity $(w_0/a)(aM_\Omega) = w_0 M_\Omega$ to the physical point, we can determine $w_0$ by calculating
\begin{equation}
w_0 = \left.\frac{\left(w_0 M_\Omega\right)^\text{fit}}{M^\text{exp}_\Omega} \right|_\text{physical point} \, .
\end{equation}
That is, we use $M_\Omega$ as the physical scale when calculating the theory scale $w_0$. 

The example of using $M_\Omega$ is not arbitrary---that is, in fact, the physical scale we use in this project. One might wonder why we would use a baryonic quantity, which admittedly suffers from an aforementioned signal-to-noise issue, as opposed to some mesonic quantity like $F_\pi$. In fact, some collaborations do use $F_\pi$ as the physical scale when calculating the gradient flow scales. However, the $\Omega$ baryon, being composed of three valence $s$ quarks, has weak light quark dependence unlike $F_\pi$. Moreover, when calculating the variance of the correlation function per Eq.~\eqref{eq:correlator_error}, we form pairs of kaons instead of pions. Thus the signal-to-noise falls as $\exp[- (M_\Omega - 3 m_K /2) t]$, which is slower than other baryons.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figs/scale_setting/w0_a_vs_lFSq.pdf}
	\caption{\label{fig:scale_interpolation_results}
	Results of interpolating (or in the case of $w_0/a_{06}$, extrapolating) $w_0/a$ to the physical pion mass using Eq.~\ref{eq:w0_interpolation}. The square data have been ``shifted'' such that the dependence is only on $\epsilon_\pi^2 = l_F^2$; the black data are the original data (see Appendix~\ref{app:curve_fitting}). Multiplying $w_0 /(w_0/a_{15})$ yields the lattice spacing on a given ensemble (see Table~\ref{tab:gradient_float_interpolations}).
	Figure from~\cite{Miller:2020evg}.
	}
\end{figure}

\begin{table}
	\begin{tabular}{ccccc}
	\hline\hline
	Scheme              & $a_{15}$/fm& $a_{12}$/fm& $a_{09}$/fm& $a_{06}$/fm\\
	\hline
	$t_{0,\rm orig}/a^2$& 0.1284(10)& 0.10788(83)& 0.08196(64)& 0.05564(44)\\
	$t_{0,\rm imp}/a^2$ & 0.1428(10)& 0.11735(87)& 0.08632(65)& 0.05693(44)\\
	$w_{0,\rm orig}/a$  & 0.1492(10)& 0.12126(87)& 0.08789(71)& 0.05717(51)\\
	$w_{0,\rm imp}/a$   & 0.1505(10)& 0.12066(88)& 0.08730(70)& 0.05691(51)\\
	\hline\hline
	\end{tabular}
	\caption{\label{tab:gradient_float_interpolations} Determinations of the lattice spacing for four different scale setting schemes using Eq.~\eqref{eq:w0_lattice_spacing}. Notice that the lattice spacing is scheme dependent. However, the result of an extrapolation to the physical (continuum) point is \emph{not} scheme dependent; different schemes simply shuffle the LECs around.}
\end{table}


\section{Extrapolating the gradient flow scales to the physical point}

We perform fits of $w_0 m_\Omega$ and $\sqrt{t_0} m_\Omega $ in order to determine the gradient flow scales $w_0$ and $t_0$, which we can then use for scale setting. The chiral expansion for $\sqrt{t_0}$ is identical to that of $w_0$ given in Eq.~\ref{eq:w0_interpolation}, other than the LECs being different. For convenience, let us separate out the observable into two pieces, 
\begin{equation}
	(w_0 M_\Omega)^\text{fit} = (w_0 M_\Omega)^\text{chiral} + (w_0 M_\Omega)^\text{disc} \, ,
\end{equation}
and analyze them separately. 

General remarks on the procedure for extrapolating observables are available in Appendix~\ref{app:curve_fitting}.

\subsection{Chiral models}
The chiral expansion for $M_\Omega$ was worked out to N$^3$LO in~\cite{Tiburzi:2008bk}.
\begin{align}
	M_\Omega =& M_0 
	+ \alpha_2 \epsilon_\pi^2 \Lambda_\chi 
	+ \left(
		\alpha_4 + \beta_4 \log \epsilon_\pi^2
	\right)\epsilon_\pi^4 \Lambda_\chi \\
	&+ \left(
		\alpha_6 + \beta_4 \log \epsilon_\pi^2 + \gamma_6 \left(\log \epsilon_\pi^2\right)^2
	\right)\epsilon_\pi^6 \Lambda_\chi \nonumber
\end{align}
We multiply this expression with that for the gradient flow scales to yield the \xpt expression for the combined quantity.
\begin{align}
	\left(w_0 M_\Omega\right)^\text{chiral} =& w_{0, \text{ch}} M_0 
	& \text{(LO)}\\
	&+ c_l \epsilon_\pi^2  + c_s \epsilon_s^2
	& \text{(NLO)}\nonumber \\
	&+  \left[ 
		c_{ll} + c_{ll, g}\log(\epsilon_\pi^2) 
	\right] \epsilon_\pi^4 + c_{ls} \epsilon_\pi^2 \epsilon_s^2 + c_{ss} \epsilon_s^4   & \text{(N$^2$LO)} \nonumber  \\
	&+ \left[ 
		c_{lll} + c_{lll, g}\log(\epsilon_\pi)  + c_{lll, g^2}\log^2(\epsilon_\pi) 
	\right] \epsilon_\pi^6
	& \text{(N$^3$LO)} \nonumber \\
	& + c_{lls} \epsilon_\pi^4 \epsilon_s^2 + c_{lss} \epsilon_\pi^2 \epsilon_s^4 + c_{sss} \epsilon_s^6 \nonumber
\end{align}
The strange quark dependence is parameterized by $\epsilon_s = (2 m_K^2 - m_\pi^2)/\Lambda_\chi$. We reiterate that the expression for $\sqrt{t_0} M_\Omega$ is identical other than the LECs.

We consider two choices for $\Lambda_\chi$ when model averaging: either $\Lambda_\chi$, as is typically chosen in $\chi$PT,  or $\Lambda_\chi = M_\Omega$, taking inspiration from~\cite{HadronSpectrum:2008xlg}.


\subsection{Discretization effects}

The lattice spacing corrections are a simple Taylor Ansatz, as explained in Appendix~\ref{app:curve_fitting}. There is some ambiguity in the definition of the expansion parameter $\epsilon_a$ (e.g., $\epsilon_a = a/2w_0$ or $\epsilon_a = a/2\sqrt{t_0}$ ); however, this choice impacts the final extrapolation at less than a fraction of a standard deviation, so we choose $\epsilon_a = w_0/2a$.

\begin{table}
	\begin{tabular}{c|cccccccccc}
		\hline\hline
		$|\mathbf{n}|$& 1 & $\sqrt{2}$& $\sqrt{3}$& $\sqrt{4}$& $\sqrt{5}$& $\sqrt{6}$& $\sqrt{7}$& $\sqrt{8}$& $\sqrt{9}$& $\sqrt{10}$\\
		\hline
		$c_n$& 6&12& 8& 6& 24& 24& 0& 12& 30& 24 \\
		\hline\hline
	\end{tabular}
\caption{\label{tab:w0_cN_weights}
	Finite volume weight factors for the first few finite volume modes.
}
\end{table}
The finite volume corrections require us to modify the logarithms (associated with loop integrals in $\chi$PT) in the following manner~\cite{Gasser:1986vb, Colangelo:2004xr}. The single logarithms become
\begin{equation}
	\log \epsilon_\pi^2 \rightarrow \log \epsilon_\pi^2 + 4 k_1(m_\pi L)
\end{equation}
where
\begin{equation}
k_1(x) = \sum_{|\mathbf{n}| \ne 0} c_n \frac{K_1 \left( x|\mathbf{n}| \right)}{x |\mathbf{n}|}
\end{equation}
and $K_1$ is a modified Bessel function of the second kind and the $c_n$ are given in Table~\ref{tab:w0_cN_weights}. Meanwhile the two-loop integrals, manifest in the $(\log \xi)^2$ terms, are modified slightly differently. These become
\begin{align}
\Big[ \log \xi_l \Big]^2 \ \rightarrow & \Big[\log \xi_l+ 4 k_1(m_\pi L) \Big]^2 - \Big[ \log \xi_l \Big]^2\\
&\approx 8 k_1(m_\pi L) \log \xi_l \nonumber
\end{align}
with the approximation being valid for our range of $m_\pi L$.

Additionally, there is a radiative correction $\alpha_s a^2$ for our sea quark action that scales as $\log a$~\cite{Follana:2006rc}.

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/scale_setting/w0_mO_vs_ea_combined_xpt_n3lo_FV_F.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/scale_setting/w0_mO_vs_l_xpt_n3lo_FV_F.pdf}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/scale_setting/sqrtt0_mO_vs_ea_combined_xpt_n3lo_FV_F.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/scale_setting/sqrtt0_mO_vs_l_xpt_n3lo_FV_F.pdf}
	\end{subfigure}
	\caption{\label{fig:scale_extrapolation_results}
	Extrapolations of $w_0 M_\Omega$ and $\sqrt{t_0} M_\Omega$ using the tree-level and improved definitions of the gradient flow scales. Here $l_F=\epsilon_\pi$. Notably, we see that $\sqrt{t_0} M_\Omega$ approaches the continuum limit in drastically different manners depending on which version of $t_0$ is used. However, the continuum values agree to within a fraction of a sigma, demonstrating that observables should agree in the continuum limit independent of scheme even if their values on a particular ensemble do not.
	Figure from~\cite{Miller:2020evg}.
	}
\end{figure}



\section{Results \& conclusions}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{./figs/scale_setting/model_breakdown.pdf}
	\caption{\label{fig:scale_model_breakdown}
	Model breakdown and comparison using the scale-improved, gradient flow derived quantities (i.e., $\sqrt{t_{0, \text{imp}}} m_\Omega$, $w_{0, \text{imp}} m_\Omega$).  The vertical band is our model average.
	\pmb{$\chi$pt-full}:~$\chi$PT model average, including $\log(\epsilon_\pi^2)$ corrections.
	\pmb{$\chi$pt-ct}:~$\chi$PT model average with counterterms only, excluding $\log(\epsilon_\pi^2)$ corrections.
	\pmb{N$^3$LO/N$^2$LO}:~model average restricted to specified order.
	\pmb{$\Lambda_\chi$}:~model average with specified chiral cutoff.
	\pmb{incl./excl. $\alpha_S$}:~model average with/without $\alpha_S$ corrections.
	\pmb{fixed/variable $\epsilon_a^2$}:~model average using two different definitions of $\epsilon^2_a$.
	\pmb{excl. a06m310L}:~model average excluding $a=0.06$ fm ensemble (\texttt{a06m310L}).
	\pmb{excl. a12m220ms}:~model average excluding small strange quark mass ensemble (\texttt{a12m220ms}).
	\pmb{Below solid line}:~results from other collaborations:
	BMWc [2020]~\cite{Borsanyi:2020mff},
	MILC [2015]~\cite{Bazavov:2015yea},
	HPQCD [2013]~\cite{Dowdall:2013rya},
	CLS [2017]~\cite{Bruno:2016plf},
	QCDSF-UKQCD [2015]~\cite{Bornyakov:2015eaa},
	RBC [2014]~\cite{Blum:2014tka},
	HotQCD [2014]~\cite{Bazavov:2014pvz},
	BMWc [2012]~\cite{Deuzeman:2012jw}
	and ALPHA [2013]~\cite{Bruno:2013gha}.
	Figure from~\cite{Miller:2020evg}.
	}
\end{figure}

We summarize the choice of models we average over in the following table; the impact of these choices is demonstrated in Fig.~\ref{fig:scale_model_breakdown}. (The model averaging procedure is described in Appendix~\ref{app:curve_fitting}.)
\begin{align*}
	\begin{array}{rl}
		\times 2:& \text{Taylor or \xpt} \\
		\times 2:& \text{Expand to N$^2$LO or N$^3$LO} \\
		\times 2:& \text{Include/exclude finite volume corrections}\\
		\times 2:& \text{Include/exclude radiative corrections $\alpha_s a^2$}\\
		\times 2:& \text{Choice of $\Lambda_\chi \in \{4\pi F_\pi, M_\Omega\}$}\\
		\hline
		32:& {\text{Total choices}}
	\end{array}
\end{align*}

Overall, we find that our model average is largely insensitive to these choices. We note that it is unnecessary to include either the finest lattice spacing ensemble (\texttt{a06m310L}) or the smaller-than-physical strange quark mass ensemble (\texttt{a12m220ms}) in order to achieve the precision reported in this work; however, excluding either ensemble does shift the central value of the model average, albeit less than~$1\sigma$.

We find that our model average is insensitive to the choice of $\chi$PT- or Taylor-type fit as well as the order at which the expansion in truncated. Nevertheless, the choice for $\Lambda_\chi = 4 \pi F_\pi$ (the typical \xpt choice) is favored by a Bayes factor of roughly 150. 

Below we report the values for the extrapolations to the physical point using $\epsilon_a = a/2w_0$. The full paper~\cite{Miller:2020evg} lists the results for an alternate choice for the definition of $\epsilon_a$, but the difference is barely perceptible so we omit it here.
\begin{align*}
	\sqrt{t_0} M_\Omega &= \tmub
	\nonumber\\ &= \tm\, ,
	\nonumber\\
	\frac{\sqrt{t_0}}{\rm fm} &= \tfmub
	\nonumber\\ &= \tfm\, ,
	\\\nonumber\\
	w_0 M_\Omega &= \wmub
	\nonumber\\
		&= \wm
	\nonumber\\
	\frac{w_0}{\rm fm} &= \wfmub
	\nonumber\\
		&= \wfm\, .
\end{align*}
The uncertainties have been split by type: statistics ($s$), chiral model ($\chi$), lattice spacing ($a$), finite volume ($V$), physical point input (phys), and model uncertainty ($M$). For $w_0$, the largest contribution to our error budget comes from statistics, suggesting a clear path forward for improving our result. In contrast, for $t_0$ the model selection uncertainty is comparable to the statistical uncertainty, with the former source of error primarily arising from different ways of modeling the discretization effects.

In future work we will likely need to incorporate the effects of isospin breaking (QCD + QED) if we wish to significantly reduce our uncertainty budget (for example, as was performed in~\cite{Borsanyi:2020mff}). As scale setting is necessary for converting any dimensionful observable calculated on the lattice into physical units, it is imperative that the uncertainty introduced by scale setting be minimized to the greatest extent possible. For now, however, we expect the contribution from scale setting to be subdominant when calculating such observables. 