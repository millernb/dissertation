\chapter{The hyperon spectrum}

Hyperons are a class of baryons containing strange quarks but no heavier quarks. In this chapter we discuss how hyperon decays can be used to determine $|V_{us}|$  in a manner orthogonal to $F_K/F_\pi$, and we present some preliminary calculations of the mass spectrum for the cascades, $\Xi$ and $\Xi^*$, as a stepping stone for a future determination of the hyperon transition matrix elements.

The work described in this chapter was presented at the 38th International Symposium on Lattice Field Theory and accompanied with the following proceeding. 

\begin{itemize}
	\item[] N. Miller et al., in 38th International Symposium on Lattice Field Theory (2022) arXiv:2201.01343 [hep-lat].
\end{itemize}

\section{Background: $\vert V_{us} \vert$ from hyperon decays}

In the previous chapter we showed how to determine the CKM matrix element $|V_{us}|$ using leptonic kaon decays ($K_{\ell 2}$) and $F_K/F_\pi$. But this is not the only way to determine $|V_{us}|$; there is a competing technique using semi-leptonic kaon decays ($K_{\ell 2}$) and a lattice calculation of the 0-momentum form factor $f^+(0)$. 

Unfortunately, there is some tension in the two kaon-based methods (see Fig.~\ref{fig:vus_sources}), with the two kaon-derived values for $\vert V_{us} \vert$ differ by roughly $2 \sigma$. We previously mentioned that $|V_{us}|$  was estimated historically using hyperon decays and assuming SU(3) flavor symmetry. However, with the lattice we don't need to make these assumptions---we can instead directly calculate the relevant hadronic matrix elements. 

A lattice determination of the hyperon transition matrix elements, nevertheless, presents its own set of challenges: the signal is baryonic, not mesonic, and thus inherently noisier than the kaon-based methods; and unlike the kaon determinations where only a single form factor (or ratio of form factors) need be determined, there are multiple form factors in hyperon decays that must be accounted for.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{./figs/hyperons/vus_sources.pdf}
	\caption{\label{fig:vus_sources}Determinations of $\vert V_{us} \vert$ from different sources.
	The two kaon-derived estimates are taken from FLAG \cite{Aoki:2021kgd}; the phenomenological hyperon-derived value is taken from the Particle Data Group \cite{Zyla:2020zbs} (specifically \cite{Cabibbo:2003ea}); the semi-inclusive $\tau$-derived average is taken from the Heavy Flavor Averaging Group \cite{HFLAV:2019otj}.
	Note that $K_{\ell 2} \enspace \& \enspace F_K/F_\pi$ only determine the ratio $\vert V_{us} \vert / \vert V_{ud} \vert$, so here we have also assumed the Particle Data Group average for $\vert V_{ud} \vert$. The green band spans the minimum/maximum values of $\vert V_{us} \vert$ from kaon decays.
	Figure from~\cite{Miller:2022vcm}.}
  \end{figure}

To get an idea of how this works, let us write down the transition matrix element $T$ for the semi-leptonic baryon decay $B_1 \rightarrow B_2 + l^- + \overline \nu_l$ \cite{Towner:2010zz}.
\begin{equation}
	T = \frac{G_\text{F}}{\sqrt{2}} V_{us}
	\Bigg[
	\overbrace{\braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}^\text{\color{ProcessBlue} axial-vector}
	- \overbrace{\braket{B_2 | \overline u \gamma_\mu s | B_1}}^\text{\color{JungleGreen} vector}
	\Bigg]
	\overline l \gamma^\mu (1 - \gamma^5) \nu_l
\end{equation}
The transition matrix element can then be related to the decay widths to extract $\vert V_{us} \vert$, which depends on two hadronic matrix elements. By projecting out the Lorentz structure, we obtain the form factors.
\begin{align}
	{\color{ProcessBlue} \braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}
	&= g_A(q^2) \gamma_\mu \gamma^5
	+ \underbrace{\cancel{\frac{f_\text{T}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu \gamma^5}}_\text{\color{RubineRed} $G$-parity} + \frac{f_\text{P}(q^2)}{2 M} q_\mu \gamma^5
	\\
	{\color{JungleGreen} \braket{B_2 | \overline u \gamma_\mu s | B_1}}
	&= g_V(q^2) \gamma_\mu + \frac{f_\text{M}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu
	+ \overbrace{\cancel{\frac{f_\text{S}(q^2)}{2 M} q_\mu}}^\text{\color{RubineRed} CVC}
\end{align}

In total there are six form factors, though one can reduce the total to four by invoking the conserved vector current (CVC) hypothesis and appealing to $G$-parity (a generalization of $C$-parity to multiplets~\cite{Weinberg:1958ut}). Given recent measurements of the hyperon decay widths from the LHCb experiment \cite{AlvesJunior:2018ldo}, we believe we can extract a competitive hyperon-derived value of $\vert V_{us}\vert$ so long as we can determine the transition form factors to $\sim$1$\%$.

Before we can calculate all the hyperon transition form factors, however, we will undertake a few more modest goals: first we will calculate the hyperon mass spectrum and then the hyperon axial charges. As our final result for these observables will depend upon an extrapolation based on $SU(2)$ \xpt for hyperons~\cite{Tiburzi:2008bk,Jiang:2009sf,Jiang:2009fa}, it is prudent to study the convergence pattern of this effective field theory (EFT) and to benchmark our results with the experimental measurements of the hyperon masses.
Prior to having precise lattice QCD results for hyperon quantities, $SU(3)$ baryon chiral perturbation theory ($\chi$PT) was utilized to relate the otherwise numerous low-energy-constants (LECs) describing various processes involving hyperons.  However, $SU(3)$ heavy baryon $\chi$PT generally does not exhibit a converging expansion~\cite{Walker-Loud:2008rui,PACS-CS:2009cvn,Torok:2009dg}.
Lattice QCD can be used to determine the more extensive set of LECs that arise in $SU(2)$ (heavy) baryon $\chi$PT for hyperons, thus providing the theory with predictive power.
The benefit of checking the heavy baryon $\chi$PT predictions using the masses are twofold: first, experimental measurements are readily available; second, masses are relatively easy to calculate on the lattice.

The next step will be to calculate the hyperon axial charges.
The leading order LECs that contribute to the hyperon axial charges also describe the pion exchange between hyperons as well as the radiative pion-loop corrections to the hyperon spectrum.
Therefore a precise determination of the axial charges will improve the determination of other observables derived from these Lagrangians.
These same hyperon axial charge LECs will also be important for understanding the hyperon-nucleon interactions germane in light hyper-nuclei and possibly for understanding the role of hyperons in neutron stars.

\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{./figs/hyperons/mass_data.pdf}

	\caption{\label{fig:xi_data}
	$M_\Xi$ as a function of $m_\pi^2$ for each of our ensembles. Here the lattice spacings range from ${\sim}0.06$ fm (purple) to  ${\sim}0.15$ fm (red). We convert from lattice units to physical units by scale setting with $M_\Omega$ and the gradient flow scale $w_0$ as explained in Chapter~\ref{chap:scale_setting}. The violet bands denote the physical point values of each observable; for $M_\Xi$ in particular, discrepancies between the physical pion mass ensembles and the physical point vanish once the strange quark mistuning and lattice spacing effects are accounted for.
	Figure from~\cite{Miller:2022vcm}.}
\end{figure}


\section{Project goals \& lattice details}

The eventual goal of this program is to calculate the hyperon transition matrix elements as motivated by the previous section. To perform these calculations, we employ an EFT for hyperons as derived in \cite{Tiburzi:2008bk,Jiang:2009sf,Jiang:2009fa} that relies on heavy baryon $\chi$PT. The first goal of this project is to test the convergence of the EFT employed in this work. To that end, we will first calculate the hyperon mass spectrum, which we determine by taking the chiral mass formula derived from this EFT and extrapolating to the physical point. Later we will calculate the hyperon axial charges and the other transition form factors, which will allow us to determine the transition matrix elements.

The hyperon spectrum has been calculated numerous times, for example in \cite{Durr:2008zz, Lin:2008rb}. There has been comparatively less work on the hyperon axial charges. The first lattice determination of the hyperon axial charges occurred in 2007 but only involved a single lattice spacing \cite{Lin:2007ap}; a calculation involving a physical pion mass ensemble and an extrapolation to the continuum limit didn't occur until 2018 \cite{Savanur:2018jrb}. However, that work only employed a Taylor extrapolation, not a $\chi$PT-motivated extrapolation to the continuum limit. Moreover, our work will benefit from the inclusion of three lattice spacings at the physical pion mass (four in total).


\section{Extrapolation details}

Let us consider the strangeness $S=2$ hyperons, i.e. the (strange) cascades. The chiral expressions for the mass formulae are as follows
\begin{align*}
	M_\Xi^{(\chi)} = &\phantom{+}  M_\Xi^{(0)} + {\color{ProcessBlue} \sigma_\Xi} \Lambda_\chi \epsilon_\pi^2
	& M_{\Xi^*}^{(\chi)} = &\phantom{+}  M_{\Xi^*}^{(0)} + {\color{ProcessBlue} \overline{\sigma}_\Xi} \Lambda_\chi \epsilon_\pi^2
	\\
	& - \frac{3\pi}{2} {\color{JungleGreen} g_{\pi\Xi\Xi}^2} \Lambda_{\chi} \epsilon_\pi^3
	&
	& - \frac{5\pi}{6} {\color{JungleGreen} g_{\pi\Xi^*\Xi^*}^2} \Lambda_{\chi} \epsilon_\pi^3
	\\
	& \qquad- {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu)
	&
	& \qquad - \frac{1}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi}  \mathcal{F}(\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu)
	\\
	& + \frac{3}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \sigma_\Xi} - {\color{ProcessBlue} \overline{\sigma}_\Xi} ) \Lambda_\chi \epsilon_\pi^2
	\mathcal{J} (\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu)
	&
	& + \frac{3}{4} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \overline{\sigma}_\Xi} -{\color{ProcessBlue} \sigma_\Xi} ) \Lambda_\chi \epsilon_\pi^2  \mathcal{J} (\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu)
	\\
	& \qquad + \alpha_\Xi^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi}^{(4)} \Lambda_\chi \epsilon_\pi^4
	&
	& \qquad + \alpha_{\Xi^*}^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi^*}^{(4)} \Lambda_\chi \epsilon_\pi^4
\end{align*}
where the non-analytic functions correspond to loop diagrams in SU(2) heavy baryon \xpt and are defined as so
\begin{align}
	\mathcal{F}(\epsilon_\pi, \epsilon, \mu) 
	&= -\epsilon \left(\epsilon^2 - \epsilon_\pi^2 \right) R\left( \frac{\epsilon_\pi^2}{\epsilon^2}\right)
	- \frac{3}{2} \epsilon_\pi^2 \epsilon \log \left( \epsilon_\pi^2 \frac{\Lambda_\chi^2}{\mu^2} \right)
	- \epsilon^3 \log \left( 4 \frac{\epsilon^2}{\epsilon_\pi^2} \right) \, ,\\
	\mathcal{J}(\epsilon_\pi, \epsilon, \mu) &=
	\epsilon_\pi^2 \log \left( \epsilon_\pi^2 \frac{\Lambda_\chi^2}{\mu^2} \right)
	+ 2\epsilon^2 \log \left( 4 \frac{\epsilon^2}{\epsilon_\pi^2} \right)
	+ 2 \epsilon^2 R\left( \frac{\epsilon_\pi^2}{\epsilon^2}\right) \, ,\\
	R(x) &= \begin{cases} \label{eq:defn_r}
	\sqrt{1 - x} \log \left( \frac{1-\sqrt{1-x}}{1+\sqrt{1-x}}\right)\, , \qquad 0 < x \leq 1\\
	2 \sqrt{x - 1} \arctan\left( \sqrt{x - 1}\right) \, , \quad x > 1
	\end{cases} 
\end{align}
and we have defined the small parameters
\begin{equation*}
	\epsilon_\pi = \frac{m_\pi}{\Lambda_\chi}
	\qquad
	\epsilon_{\Xi \Xi^*} = \frac{M_{\Xi^*}^{(0)}  - M_{\Xi}^{(0)}}{\Lambda_\chi}
\end{equation*}
with again the chiral scale/renormalization scale set at $\Lambda_\chi = 4 \pi F_\pi$.

From glancing at the chiral expressions, we can immediately glean a few insights. First, in this EFT, baryons of the same strangeness will share many common LECs. Thus we see an immediate advantage of a chiral extrapolation over independent Taylor extrapolations of each: simultaneously fitting both mass formulae will result in more precise determinations of the LECs, which in turn will lead to more precise extrapolations to the physical point. Second, when we later include the axial charges in our analysis, we see that our analysis will benefit twice: once from simultaneously fitting the two and three point functions, thereby improving our determination for the energies on each lattice~\cite{He:2021yvm}, and later when performing the extrapolation to the physical point. Finally, the LECs are manifestly dimensionless as written, other than the constant terms $M_\Xi^{(0)}$ and $M_{\Xi^*}^{(0)}$. Indeed, the only other dimensionful quantity in the expansion is the cutoff $\Lambda_\chi$.

\subsection{Results}

\begin{table}
	\small
	\begin{subtable}{0.48\textwidth}
		\begin{align*}
		\begin{array}{rl}
			\\
			+ 1:& \text{Taylor }\mathcal{O}(m^2_\pi) \\
			+ 1:& \text{$\chi$PT } \mathcal{O}(m^3_\pi)\\
			+ 3:& \text{Taylor } \mathcal{O}(m^4_\pi) + \text{$\chi$PT } \left\{ 0,\, \mathcal{O}(m^3_\pi),\, \mathcal{O}(m^4_\pi) \right\} \\
			\hline
			5:& {\color{RubineRed} \text{chiral choices}}
		\end{array}
		\end{align*}
	\end{subtable}
	\hspace{\fill}
	\begin{subtable}{0.48\textwidth}
		\begin{align*}
		\begin{array}{rl}
			\times 5:& {\color{RubineRed}  \text{chiral choices}}  \\
			\times 2:& \left\{ \mathcal{O}(a^2), \mathcal{O}(a^4)\right\}\\
			\times 2:& \text{incl./excl. strange mistuning} \\
			\times 2:& \text{natural priors or empirical priors} \\
			\hline
			40:& \text{total choices}
		\end{array}
		\end{align*}
	\end{subtable}

	\caption{Models employed in this work.}
	\label{tab:models}
\end{table}

We explore a range of models (summarized in Table \ref{tab:models}) with the models weighted according to their Bayes factors and averaged per the procedure described in Appendix~\ref{app:curve_fitting}.
There are five choices for the chiral expansion. We begin by considering a pure Taylor extrapolation to leading order (LO), i.e.\ $\mathcal O (m_\pi^2)$. Next we consider extensions of the LO fit to next-to-leading-order (NLO), i.e.\ $\chi$PT $\mathcal O (m_\pi^3)$ terms. At N$^2$LO, should we choose to include terms of this order, we consider either a pure Taylor term with or without the inclusion of $\chi$PT terms up to $\mathcal O (m_\pi^4)$. Regardless of the pion mass extrapolation, we assume the observables have common LECs per the chiral expression above. Fig.\ \ref{fig:histograms} explores the impact of these different models.

Next we explore corrections specific to the lattice, starting with lattice discretization corrections up to $\mathcal{O}(a^4)$. We also explore the impact of our simulated strange quark mass being slightly mistuned from the physical value.

The priors for the axial charges are set from either experiment or prior lattice calculations \cite{Jiang:2009sf} but with appreciable (20\%) width. The remaining dimensionless LECs are independently priored per the Gaussians $\mathcal N(0, 2^2)$ as is commensurate with ``naturalness" expectations. The dimensionful constant terms $M_\Xi^{(0)}$ and $M_{\Xi^*}^{(0)}$ are the exception here and are priored at the physical value of $M_\Xi^{(0)}$ with a 20\% width. We have labeled these the \emph{natural priors}. We have also explored an alternative set of priors derived from the empirical Bayes method (see Appendix~\ref{app:curve_fitting}).

After model averaging, we report the masses to be
\begin{align}
	M_{\Xi} &= 1339(17)^\text{s}(02)^\chi(05)^a(00)^\text{phys}(01)^\text{M} \text{ MeV} &=1339(18) \text{ MeV}
	\\
	M_{\Xi^*} &= 1542(20)^\text{s}(03)^\chi(06)^a(00)^\text{phys}(03)^\text{M} \text{ MeV} &=1542(21) \text{ MeV}
\end{align}
Again we have separated the errors as induced by statistics (s), chirality ($\chi$), lattice discretization ($a$), physical point input (phys), and model averaging (M). We have not yet calculated the finite volume corrections.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{./figs/hyperons/xi_xist_histograms.pdf}
	\caption{\label{fig:histograms}
	Truncation of the chiral expansion to different orders in the pion mass. We have adopted a ``data-driven'' analysis, i.e.\ we give no \emph{a priori} weight to any of the different chiral models. Although the LO fit only comprises 1/5 of the total models used in this analysis (see Table \ref{tab:models}), they still contribute more to the model average than the 2/5 of models that truncate at NLO instead. Further, the LO fits contribute almost as much as 2/5 of models that include N$^2$LO terms. The vertical red band is the Particle Data Group average~\cite{Zyla:2020zbs}.
	Figure from~\cite{Miller:2022vcm}.}
  \end{figure}

In this precursory work, we find that a LO Taylor fit describes the data approximately as well as an N$^2$LO \xpt fit. Indeed, including the nonanalytic $\epsilon_\pi^3$ term and chiral logarithms at NLO tanks the weight of the fit, while some cancellation between the NLO and N$^2$LO terms appears to yield an extrapolated value for the masses virtually identical to the LO results. So far the chiral terms introduced by heavy baryon \xpt do not appear to be useful in guiding our extrapolation; however, knowing the relationship between the LECs contained in different observables might still prove helpful yet.