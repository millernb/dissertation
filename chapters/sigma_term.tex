\chapter{The nucleon sigma term}

In this chapter, we present work on a calculation of the nucleon sigma term. For reference, it is defined as 
\begin{equation}
  \label{eq:nucleon_sigma_defn}
\sigma_{\pi N} = \hat m \braket{N | (\overline u u + \overline d d) | N}
\end{equation}
where $\hat m = (m_u + m_d)/2$. The nucleon sigma term $\sigma_{\pi N}$ measures the contribution to the nucleon mass by the explicit breaking of chiral symmetry. It also has implications in certain classes of dark matter searches, as we will explain shortly. 


\section{A callback to the linear-$\sigma$ model}
If the name sounds familiar, it's because we already encountered the sigma term back in Chapter~\ref{chap:xpt} when we discussed the linear-$\sigma$ model (Eq.~\ref{eqn:linear_sigma_lagrangian_axial_vector}). We assumed there was a particle like $\sigma = \overline \psi \psi$ (a Lorentz scalar), which after spontaneous symmetry breaking caused the nucleon to gain a mass term ($gv$). However, there still remained an interaction with $\tilde \sigma  = \sigma - v$.
\begin{equation}
  \mathcal L \supset -\overline N (gv + g\tilde \sigma) N
\end{equation}
If we now include a term $-\epsilon \sigma$ in the potential that explicitly breaks the symmetry (analogous to the explicit symmetry breaking by the quark masses), the potential (Eq.~\ref{eq:linear_sigma_potential}) becomes~\cite{Koch:1997ei}
\begin{align}
  V(\pi, \sigma) = -\frac{\mu^2}{2}\left(\sigma^2 + \pi^2 \right) +\frac{\lambda}{4}\left(\sigma^2 + \pi^2 \right)^2 - \epsilon \sigma
\end{align}
with a new minimum at $\sigma \rightarrow v \approx v_0 + \epsilon/(2\lambda v_0)$ and $\pi \rightarrow 0$ where $v_0 = \mu/\sqrt{\lambda}$ was the original minimum. Notably, the pion mass is now non-zero,
\begin{equation}
  m_\pi^2 \approx \left.\frac{\partial^2 V}{\partial \sigma^2}\right|_{(\sigma, \pi)\rightarrow (v, 0)} = \frac{\epsilon}{v_0} \, .
\end{equation}
This is essentially a statement of the GMOR relation for the linear-$\sigma$ model. Finally, we see that the nucleon mass term also picks up a small correction. 
\begin{equation}
  gv_0 \overline N N \rightarrow g\left(v_0 + \frac{\epsilon}{2\lambda v_0} \right) \overline N N 
\end{equation}

If we reinterpret $\sigma$ as the light quarks $\overline u u + \overline d d$, we see that this correction describes the contribution to the nucleon mass from explicit chiral symmetry breaking, which is exactly what $\sigma_{\pi N}$ is meant to measure. In fact, this expression suggests that at leading order $\sigma_{\pi N} \sim m_\pi^2$, a prediction also supported by chiral perturbation theory. 


\section{Relevance to dark matter searches}
Among the simplest extensions to the Standard Model is the so-called minimal supersymmetric Standard Model (MSSM), which in the process of solving the Higgs hierarchy problem~\cite{Martin:1997ns}, could also explain the abundance of dark matter in the universe by way of weakly interacting massive particles. Specifically, the lightest supersymmetric particle, the neutralino $\chi$, would be the dark matter candidate. The MSSM Lagrangian describes the interaction of this particle with nucleons through the terms~\cite{Falk:1998xj}
\begin{align}
  \mathcal L_\text{MSSM} 
  \supset&
  \alpha_{1f} \left(\overline \chi \gamma_\mu \gamma^5 \chi\right) \left(\overline q_f \gamma^\mu q_f\right)
  +\alpha_{2f} \left(\overline \chi \gamma_\mu \gamma^5 \chi\right) \left(\overline q_f \gamma^\mu \gamma^5 q_f\right)
  +\alpha_{3f} \left(\overline \chi \chi\right) \left(\overline q_f q_f\right) \\
  &
  +\alpha_{4f} \left(\overline \chi \gamma^5  \chi\right) \left(\overline q_f \gamma^5 q_f\right)
  +\alpha_{5f} \left(\overline \chi \chi\right) \left(\overline q_f \gamma^5 q_f\right)
  +\alpha_{6f} \left(\overline \chi \gamma^5 \chi\right) \left(\overline q_f q_f\right)
  \nonumber
\end{align}
which are summed over the quark flavors. The terms can be classified as velocity-independent ($\alpha_{2f}, \alpha_{3f}$ coefficients) and velocity-dependent (the rest). For direct dark matter searches like the LUX-ZEPLIN experiment~\cite{LUX-ZEPLIN:2018poe}, these terms are suppressed by a factor of $(v/c)^2 \sim 10^{-8}$, with $v$ roughly the relative speed between the Earth and the Sun, and are therefore largely irrelevant. (For indirect searches like Super-Kamiokande~\cite{Super-Kamiokande:2020sgt}, however, these velocity-dependent terms are more important).

Of the two velocity-independent terms, one is spin-dependent ($\alpha_{2f}$) and the other spin-independent ($\alpha_{3f}$). Let us focus on the spin-independent term. The cross section is given by~\cite{Ellis:2008hf}
\begin{equation}
  \sigma_\text{SI} = \frac{4 m_r^2}{\pi} \left[ Z f_p + (A -Z) f_n \right]^2 \, .
\end{equation}
with $A$ the mass number, $Z$ the proton number, and $m_r$ the neutralino-nucleon reduced mass. Buried in the definitions of $f_p$ and $f_n$ lies the dependence on the \emph{flavor} sigma terms. 
\begin{equation}
  \frac{f_N}{M_N} = \sum_{f=u,d,s} f^{(N)}_{T_f} \frac{\alpha_{3f}}{m_f} + \frac{2}{27} f^{(N)}_{TG} \sum_{q=c,b,t} \frac{\alpha_{3f}}{m_f}
\end{equation}
where $N = p, n$ and
\begin{align}
  m_N f_{T_f}^{(N)} &= \braket{N | m_f \overline q_f q_f | N} \\
  f^{(N)}_{TG} &= 1 - \sum_{q=u,d,s} f_{T_f}^{(N)} \, .
\end{align}

The importance of the sigma terms here shouldn't be understated---the nucleon sigma term is currently the largest source of uncertainty when calculating the spin-independent neutralino-nucleon cross section; the authors of said uncertainty analysis literally plead for a campaign to better determine the sigma term~\cite{Ellis:2008hf}. 

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/sigma_term/flag_sigma.pdf}

  \caption{\label{fig:flag_sigma}
  Estimates of $\sigma_{\pi N}$ from the lattice and phenomenology. The phenomenological results are denoted by the blue circles; the lattice results are in green and red, with the green values (QCDSF 12~\cite{Bali:2012qs}, $\chi$QCD 15A~\cite{Yang:2015uis}, BMW 15~\cite{Durr:2011mp}, BMW 11A~\cite{Durr:2011mp}, ETM 14A~\cite{Alexandrou:2014sha})  
  passing the FLAG criteria for being ``reasonably consistent''.
  In addition, Gupta et al.~\cite{Gupta:2021ahb} have a recent result near the center of ETM 14A but with tighter error bars. 
  In summary, most lattice calculations are around $\sim 40$ MeV, whereas most phenomenological results are around $\sim 55$ MeV. 
  Figure from FLAG~\cite{Aoki:2021kgd}.}
\end{figure}


\section{Measuring $\sigma_{\pi N}$ via the Feynman-Hellman theorem}
There are two techniques one can employ on the lattice to calculate the sigma term.  First there is the direct method. Notice that Eq.~\ref{eq:nucleon_sigma_defn} is just a matrix element. We can calculate it on the lattice by having a nucleon at the source and sink and then inserting a quark current in the middle. In principle this is straightforward, but it does suffer disadvantages; in particular, this requires fitting a baryonic 3-point function and dealing with all the ensuing messiness. 

Alternatively, one can relate the sigma term to the nucleon mass via the Feynman-Hellmann theorem~\cite{Feynman:1939zza, Cohen:1991nk}, which means one fewer thing to generate on the lattice. The theorem states that given a Hermitian operator $H$ depending on a real parameter $\lambda$ with normalized eigenvectors $\ket{\psi(\lambda)}$ and eigenvalues $E(\lambda)$, 
\begin{equation}
\left\langle\psi(\lambda) \middle| \frac{\partial}{\partial\lambda} H(\lambda) \middle| \psi(\lambda)\right\rangle = \frac{\partial E}{\partial\lambda} \, .
\end{equation}

Let's apply this theorem to the nucleon. In particular, we know that the QCD Hamiltonian has a term like
\begin{align}
  \mathcal H_\text{QCD} &\supset m_u \overline u u + m_d \overline d d \\
  &= \hat m (\overline u u + \overline d d) + \frac 12 \delta_m (\overline u u - \overline d d)
  \nonumber
\end{align}
where we have defined $\hat m = (m_u + m_d)/2$ and $\delta_m = m_d - m_u$. With normalized eigenstates $\ket{N}$ and eigenvalues $M_N$, we find that
\begin{equation}
  \left\langle N \middle| \frac{\partial}{\partial\hat m }\left[\hat m \left(\overline u u + \overline d d\right) \right] \middle| N \right\rangle = \frac{\partial M_N}{\partial\hat m} \, .
\end{equation}
Comparing this result with Eq.~\ref{eq:nucleon_sigma_defn} yields the desired result.
\begin{equation}
  \label{eq:nucleon_sigma_fh}
  \sigma_{\pi N} = \hat m \frac{\partial M_N}{\partial\hat m}
\end{equation}

However, this technique isn't without tradeoffs. Although now the sigma term can be calculated using only the nucleon correlator, the quoted result will be dependent on the \emph{derivative} of the nucleon mass fit. It is not just sufficient that our different fit models converge to the same value at the physical point; they must approach the physical point at the same rate. 


\section{Finagling an expression for the sigma term}

\subsection{Recasting the nucleon mass derivative into something more manageable}
Although we could, in principle, use Eq.~\ref{eq:nucleon_sigma_fh} to determine the nucleon sigma term, it is not a convenient expression for calculations on the lattice. Instead of taking a derivative with respect to $\hat m$, we would prefer to take a derivative with respect $m_\pi$ or (better yet)  $\epsilon_\pi = m_\pi / \Lambda_\chi$, as these are the data we have readily available. 

As a starting point, we can of course write 
\begin{equation}
\hat m \frac{\partial M_N}{\partial \hat m} = \hat m \frac{\partial m_\pi^2}{\partial \hat m}\frac{\partial M_N}{\partial m_\pi^2} \, .
\end{equation}
To NLO,
\footnote{Unfortunately the literature defines ``LO" differently for meson \xpt ($\mathcal O(1)$) versus baryon \xpt ($\mathcal O(\epsilon^2)$). We will stick with the literature here. If an expression mixes baryon and meson \xpt expressions (e.g., $M_N/F_\pi$), we will use the baryon power-counting scheme. We will refer to the $\mathcal O(1)$ contributions in baryon \xpt as LLO (``less-than-leading order").}
the pion mass can be perturbatively related to $\hat m$ by
\begin{equation}
m_\pi^2 \approx 2 B \hat m \left( 1 + \delta_{m^2} \right) 
\quad \implies \quad
\hat m  \approx \frac{m_\pi^2}{2 B}  \left( 1 - \delta_{m^2} \right)
\end{equation}
where $\delta_{m^2}$ is the NLO correction. Therefore to NLO
\begin{align}
\hat m \frac{\partial m_\pi^2}{\partial \hat m}\ &\approx \frac{m_\pi^2}{2 B}  \left( 1 - \delta_{m^2} \right) \frac{\partial}{\partial \hat m} \Big[ 2 B \hat m \left( 1 + \delta_{m^2} \right) \Big] \nonumber \\
&= m_\pi^2 \Big( 1 - \delta_{m^2} \Big) \Big( 1 + \delta_{m^2} + \hat m \frac{\partial \delta_{m^2}}{\partial \hat m} \Big) \nonumber \\
&\approx m_\pi^2 \left( 1 + \hat m \frac{\partial \delta_{m^2}}{\partial \hat m} \right)
\end{align}

At this point we need the expression for $\delta_{m^2}$~\cite{Gasser:1983yg},
\begin{equation}
\delta_{m^2} = \frac{1}{2} \frac{2 B \hat m}{(4 \pi F)^2} \left[ \log \left(\frac{2B\hat m}{\mu^2} \right) + 4 \overline l_3^r (\mu) \right] \, , 
\end{equation}
which has partial derivative 
\begin{align}
\frac{\partial \delta_{m^2}}{\partial \hat m} &= \frac{1}{2} \frac{2 B }{(4 \pi F)^2} \left[ \log \left(\frac{2B\hat m}{\mu^2} \right) + 1 + 4 \overline l_3^r (\mu) \right] 
\nonumber \\ 
&\approx \frac{1}{2} \frac{2 B }{(4 \pi F)^2} \left[ \log \left(\frac{2B\hat m}{\mu^2} \right) + 1  - \left( \overline l_3  + \log \frac{m_\pi^2}{\mu^2} \right) \right]
\nonumber \\
&\approx \frac{1}{2} \frac{2 B }{(4 \pi F)^2} \left( 1 - \overline l_3 \right)
\nonumber \\
&\approx \frac{1}{2} \frac{\epsilon_\pi^2}{\hat m} \left( 1 - \overline l_3 \right) 
\end{align}
where in the second line we have related the renormalized LEC $\overline l_3^r(\mu)$ to the barred LEC $\overline l_3$ by~\cite{Ecker:1994gg}
\begin{equation} \label{eq:l_renom_to_bar}
\overline l_i^r = \frac{\gamma_i}{2} \left[ \overline l_i + \log \left( \frac{m_\pi^2}{\mu^2}\right) \right]
\qquad\text{where }\gamma_3=-\frac 12 \text{ and } \gamma_4 = 2 \, .
\end{equation}
In the third line we have combined the logarithms by rounding to NLO, and in the fourth line we have rounded to NLO again after approximating $2 B \hat m \approx m_\pi^2$ and $F \approx F_\pi$. 

Putting everything together, we get an expression for the nucleon sigma term to NLO that doesn't explicitly depend on $\hat m$,
\begin{equation}
\sigma_{\pi N} 
\approx m_\pi^2 \left[ 1 + \frac 12 \epsilon_\pi^2 \left(1 - \overline l_3 \right) \right] \frac{\partial M_N}{\partial m_\pi^2} \, .
\end{equation}

Next we would like to rewrite the derivative with respect to $\epsilon_\pi$ instead of $m_\pi^2$. Rewriting the derivative as
\begin{equation}
\frac{\partial M_N}{\partial m_\pi^2} = \frac{1}{2 \epsilon_\pi} \frac{\partial \epsilon_\pi^2}{\partial m_\pi^2} \frac{\partial M_N}{\partial \epsilon_\pi}  \, ,
\end{equation}
we find that we must calculate
\begin{equation}
\frac{\partial \epsilon_\pi^2}{\partial m_\pi^2} 
= \frac{1}{(4 \pi F_\pi^2)} \left[ 1 - 2 \frac{m_\pi^2}{F_\pi} \frac{\partial F_\pi}{\partial m_\pi^2} \right] \, .
\end{equation}

At this point it's evident that we also require the partial derivative $\partial F_\pi / \partial m_\pi^2$. The chiral expression for $F_\pi$ to NLO is~\cite{Gasser:1983yg}
\begin{equation}
F_\pi \approx F (1 + \delta_F) \quad \implies  \quad F \approx F_\pi (1 - \delta_F)
\end{equation}
where $\delta_F$ is the NLO piece,
\begin{equation}
\delta_F = \frac{2 B \hat m}{(4 \pi F)^2} \left[ -\log \left( \frac{2 B \hat m}{\mu^2} \right) + \overline l_4^r (\mu) \right] \, . 
\end{equation}
Now we write simplify the derivative
\begin{align}
\frac{\partial F_\pi}{\partial m_\pi^2} &\approx F \frac{\partial \delta_F}{\partial m_\pi^2}
\nonumber \\
&\approx F \frac{\partial}{\partial m_\pi^2} \left\{ \frac{m_\pi^2}{(4 \pi F)^2} \left[ -\log \left( \frac{m_\pi^2}{\mu^2} \right) + \overline l_4^r (\mu) \right] \right\} 
\nonumber \\
&= F \frac{1}{(4\pi F)^2} \left[ -1 -\log \left( \frac{m_\pi^2}{\mu^2} \right)  + \overline l_4^r (\mu)  \right] 
\nonumber \\
&= F \frac{1}{(4\pi F)^2} \left[ -1 -\log \left( \frac{m_\pi^2}{\mu^2} \right)  + \left(\overline l_4 + \log \left( \frac{m_\pi^2}{\mu^2}\right) \right) \right] \nonumber \\
&= F \frac{1}{(4\pi F)^2} \left( -1 + \overline l_4 \right) \nonumber \\
&\approx F_\pi \frac{\epsilon_\pi^2}{m_\pi^2} \left( -1 + \overline l_4 \right) \, .
\end{align}
which means
\begin{align}
\frac{\partial \epsilon_\pi^2}{\partial m_\pi^2} 
&=  \frac{1}{(4 \pi F_\pi^2)} \left[ 1 - 2 \frac{m_\pi^2}{F_\pi} \frac{\partial F_\pi}{\partial m_\pi^2} \right] \nonumber \\
&\approx  \frac{1}{(4 \pi F_\pi^2)} \left[ 1 + 2 \epsilon_\pi^2 \left(1 - \overline l_4 \right) \right]
\end{align}

Combining everything yields the desired result
\begin{align} \label{eq:sigma_term_eps}
\sigma_{\pi N} &\approx
\frac 12 \epsilon_\pi \Big[ 1 + \frac 12 \epsilon_\pi^2 \left(1 - \overline l_3 \right) \Big] 
\Big[ 1 + 2 \epsilon_\pi^2 \left(1 - \overline l_4 \right) \Big] \frac{\partial M_N}{\partial \epsilon_\pi}\nonumber \\
&\approx \frac 12 \epsilon_\pi \left[ 1 + \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline l_3 - 2 \overline l_4 \right) \right] \frac{\partial M_N}{\partial \epsilon_\pi} \, .
\end{align}


\subsection{Accounting for the Gasser-Leutwyler LECs} 

Inspecting Eq.~\ref{eq:sigma_term_eps}, we find that the only dimensionful quantity is $M_N$. This is not technically an issue for us since we have completed our scale setting (Chapter~\ref{chap:scale_setting}). However, we would prefer to avoid introducing a scale as much as possible, since introducing a scale correlates data across ensembles via that scale (ensembles are otherwise uncorrelated). 

Moreover, we find that at NLO we require determinations of $\overline l_3$ and $\overline l_4$, which are the Gasser-Leutwyler LECs defined at a different scale $\mu$ (see Eq.~\eqref{eq:l_renom_to_bar}). These LECs are compiled in FLAG, but they are also accessible with our lattice data; in fact, it would behoove us to calculate these, too, for reasons that will be evident shortly. 

Observe we can expand the derivative in Eq.~\ref{eq:sigma_term_eps} as so
\begin{equation}
\frac{\partial M_N}{\partial \epsilon_\pi} 
= \Lambda_\chi \frac{\partial (M_N/\Lambda_\chi) }{\partial \epsilon_\pi}
+ \frac{M_N}{\Lambda_\chi} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi} \, ,
\end{equation}
resulting in the following expression for the sigma term,
\begin{equation} \label{eq:sigma_lambda_chi}
\sigma_{\pi N} \approx \frac 12 \epsilon_\pi \left[ 1 + \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline l_3 - 2 \overline l_4 \right) \right] \left[ 
\Lambda_\chi \frac{\partial (M_N/\Lambda_\chi) }{\partial \epsilon_\pi}
+ \frac{M_N}{\Lambda_\chi} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}
\right] \, .
\end{equation}
Recall that we set $\Lambda_\chi = 4\pi F_\pi$. Clearly this form for the sigma term requires us to additionally fit $F_\pi$. One might object to this strategy: although we removed the scale setting requirement for $M_N$, now we have a scale setting requirement for $F_\pi$! 

However, given the choice between a difficult fit of $M_N$ and a difficult fit of $F_\pi$, the latter is preferred. Fitting $F_\pi$ requires only SU(2) (meson) $\chi$PT, which has been rather successful; fitting $M_N$, in contrast, requires SU(2) heavy baryon $\chi$PT, which converges more slowly.

Furthermore, as we will see when we discuss the results of this analysis, splitting the terms this way has interesting consequences on where the bulk of the contribution to the sigma term comes from (to preview: not the nucleon part!).

\section{Project goals}
\noindent To summarize, we can determine $\sigma_{\pi N}$ through the following procedure:
\begin{enumerate}
  \item Fit $M_N/\Lambda_\chi$ in lattice units (since we have lattice data for both of these observables).
  \item Fit $F_\pi$ in physical units (using our scale setting results).
  \item Extract the $\overline l_4$ LECs from the $F_\pi$ fit.
  \item Calculate the derivatives.
  \item Combine all results (plus the FLAG result for $\overline l_3$) to determine $\sigma_{\pi N}$ using Eq.~\eqref{eq:sigma_term_eps}.
\end{enumerate}

Note that in this preliminary work we do not determine $\overline l_3$, as this requires us to fit $m_\pi$ also. However, our collaboration plans to include the $m_\pi$ fit in future work. 

\section{Extrapolation functions}

\subsection{Fit function for $F_\pi$}


Again, we separate our fit function as $F_\pi^\text{fit} = F_\pi^\text{chiral} + F_\pi^\text{disc}$. The SU(2) \xpt expression for $F_\pi$ to $\mathcal O(\epsilon^4)$ is given by
\begin{equation}
  F_\pi^\text{chiral} \approx F_0 \left[
    1 + \epsilon_\pi^2 \left( - \log \epsilon_\pi^2 + \overline l^r_4 \right) 
    + \epsilon_\pi^4 \left( 
      \frac 54 \log^2 \epsilon^2 + \alpha_F^\text{(4)} \log \epsilon_\pi^2 + \beta_{F}^{(4)}
    \right)
  \right] 
\end{equation}
Notice that the expression recorded here is much simpler than the one from Chapter~\ref{chap:fk_fpi}---we are employing SU(2) \xpt this time, not SU(3) \xpt. For now we do not include finite volume corrections (as we haven't yet worked these out for the baryons), so $F_\pi^\text{disc}$ only contains terms that are a Taylor expansion in the lattice spacing. 

We will need the reciprocal of this expression soon for $M_N/\Lambda_\chi$, so we note it now.
\begin{align}
  \label{eq:sigma_lambda_inverse}
  \frac{1}{F_\pi} \approx \frac{1}{F_0} \Big[&
    1 - \epsilon_\pi^2 \left( - \log \epsilon_\pi^2 + \overline l^r_4 \right)  \\
    &+ \epsilon_\pi^4 \left( 
      \left(\overline l^r_4\right)^2 -\beta_{F}^{(4)} 
      - \left(\alpha_F^\text{(4)} + 2 \overline l^r_4 \right) \log \epsilon_\pi^2
      -\frac 14 \left(\log \epsilon_\pi^2 \right)^2
    \right) 
  \Big] \, . \nonumber 
\end{align}


\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./figs/sigma_term/fpi_vs_epi.pdf}

	\caption{Pion mass dependence of $F_\pi$. The red, green, blue, and purple bands denote a lattice spacing of roughly $0.15, 0.12, 0.09, 0.06$ fm, respectively. The hashed band is the extrapolation in the continuum ($a=0$) limit. Note the non-trivial lattice spacing dependence, as evident by the crossing of the different pion spacings near $\epsilon_\pi\approx 0.18$. }
	\label{fig:fpi_epi_plot}
\end{figure}

\subsection{Fit function for $M_N$}
We first note the SU(2) heavy baryon \xpt expression for $M_N$~\cite{Tiburzi:2005na},
\begin{align}
  \label{eq:sigma_mn_chiral}
  M_N^\text{chiral} = &\phantom{+}  M_N^{(0)} & \text{(LLO)} \\
  &+ \beta^\text{(2)}_N \Lambda_\chi \epsilon_\pi^2 & \text{(LO)} \nonumber\\
  &- \frac{3\pi}{2} g_{\pi NN}^2 \Lambda_{\chi} \epsilon_\pi^3
  - \frac{4}{3} g_{\pi N\Delta}^2 \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, \epsilon_{N\Delta}, \mu) & \text{(NLO)} \nonumber\\
  &+ \gamma_N^\text{(4)} \Lambda_\chi \epsilon_\pi^2 
  \mathcal{J} (\epsilon_\pi, \epsilon_{N \Delta}, \mu) &\text{(N$^2$LO)} \nonumber\\
  &\qquad + \alpha_N^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{N}^{(4)} \Lambda_\chi \epsilon_\pi^4 \, .\nonumber
\end{align}
We recall that the non-analytic functions are defined as so
\begin{align}
  \mathcal{F}(\epsilon_\pi, \epsilon_\Delta, \mu) 
  &= -\epsilon_\Delta \left(\epsilon_\Delta^2 - \epsilon_\pi^2 \right) R\left( \frac{\epsilon_\pi^2}{\epsilon_\Delta^2}\right)
  - \frac{3}{2} \epsilon_\pi^2 \epsilon_\Delta \log \left( \epsilon_\pi^2 \frac{\Lambda_\chi^2}{\mu^2} \right)
  - \epsilon_\Delta^3 \log \left( 4 \frac{\epsilon_\Delta^2}{\epsilon_\pi^2} \right) \, ,\\
  \mathcal{J}(\epsilon_\pi, \epsilon_\Delta, \mu) &=
  \epsilon_\pi^2 \log \left( \epsilon_\pi^2 \frac{\Lambda_\chi^2}{\mu^2} \right)
  + 2\epsilon_\Delta^2 \log \left( 4 \frac{\epsilon_\Delta^2}{\epsilon_\pi^2} \right)
  + 2 \epsilon_\Delta^2 R\left( \frac{\epsilon_\pi^2}{\epsilon_\Delta^2}\right) \, ,\\
  R(x) &= \begin{cases} 
  \sqrt{1 - x} \log \left( \frac{1-\sqrt{1-x}}{1+\sqrt{1-x}}\right)\, , \qquad 0 < x \leq 1\\
  2 \sqrt{x - 1} \arctan\left( \sqrt{x - 1}\right) \, , \quad x > 1
  \end{cases} \, .
\end{align}
We have left the renormalization scale dependence explicit here. These terms depend on the masses of the nucleon and the $\Delta$ resonance. For $M_N$, we use the lattice value; for $M_\Delta$, we use the PDG value. The estimates for the axial charges are listed in Table~\ref{tab:axial_charges}.

\begin{table}[]
  \begin{tabular}{lr|l}
  \hline\hline
  Charge                    & Value   & Source\\ \hline
  $g_{\pi NN}$    & 1.27    & CalLat \\
  $g_{\pi N \Delta}$  &  -1.48 & Expt. \\
  $g_{\pi\Delta\Delta}$     & -2.20    & SU(3) \\
  \hline\hline
  \end{tabular}
  \caption{Predicted/measured values of the axial charges per \cite{Chang:2018uxx, Jiang:2009sf}. The SU(3) predictions are obtained by three-flavor heavy baryon $\chi$PT as outlined in \cite{Butler:1992pn}.}
  \label{tab:axial_charges}
\end{table}

As written, it is clear that the factors of $\Lambda_\chi$ will cancel in all but the LLO term when multiplying Eq.~\eqref{eq:sigma_mn_chiral} by Eq.~\eqref{eq:sigma_lambda_inverse}.

\begin{align}
  \label{eq:sigma_mp_lam}
  \left(\frac{M_N}{\Lambda_\chi}\right)^\text{chiral} =& \overbrace{\frac{M_N^{(0)}}{\Lambda_\chi}}^\text{starts at LLO}
  & \text{(LLO)}  \\
  &+ \beta^\text{(2)}_N \epsilon_\pi^2 
  & \text{(LO)} \nonumber \\
  &- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
  - \frac{4}{3} g_{\pi N\Delta}^2 \mathcal{F}(\epsilon_\pi, \epsilon_{N\Delta}, \mu) 
  & \text{(NLO)} \nonumber\\
  &+ \gamma_N^\text{(4)} \epsilon_\pi^2 
  \mathcal{J} (\epsilon_\pi, \epsilon_{N \Delta}, \mu) 
  &\text{(N$^2$LO)} \nonumber \\
  &\qquad + \alpha_N^\text{(4)}  \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{p}^{(4)} \epsilon_\pi^4
  \nonumber
\end{align}

For a Taylor-type fit, we can drop the non-analytic functions as well as the $\mathcal O(\epsilon^3)$ term (which is not analytic in $\epsilon^2$). Additionally we treat the LLO term as a constant. Otherwise we should expand the LLO term, whose contributions are in {\color{JungleGreen} green}.
\begin{align}
  \left(\frac{M_N}{{\color{JungleGreen}\Lambda_\chi}}\right)^\text{chiral} = & c_0
  & \text{(LLO)}\\
  &+ \left(\beta^\text{(2)}_N - {\color{JungleGreen} c_0 \overline \ell_4^r} \right) \epsilon_\pi^2 
  + {\color{JungleGreen} c_0 \epsilon_\pi^2 \log \epsilon_\pi^2} 
  & \text{(LO)} \nonumber \\
  &- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
  - \frac{4}{3} g_{\pi N\Delta}^2 \mathcal{F}(\epsilon_\pi, \epsilon_{N\Delta}, \mu) 
  & \text{(NLO)} \nonumber\\
  & + \left( \beta_{N}^{(4)} + {\color{JungleGreen} c_0 \left(\overline \ell_4^r\right)^2} - {\color{JungleGreen} c_0 \beta_{F}^{(4)}}  \right) \epsilon_\pi^4
  + \gamma_N^\text{(4)} \epsilon_\pi^2 \mathcal{J} (\epsilon_\pi, \epsilon_{N \Delta}, \mu) 
  &\text{(N$^2$LO)} \nonumber \\
  &\qquad - {\color{JungleGreen}\frac 14 c_0 \epsilon_\pi^4  \left(\log  \epsilon^2 \right)^2} 
  + \left( \alpha_N^\text{(4)} - {\color{JungleGreen} c_0 \alpha_F^\text{(4)}} - {\color{JungleGreen} 2 c_0 \overline \ell_4^r} \right) \epsilon_\pi^4 \log{\epsilon_\pi^2} 
  \nonumber
  \end{align}

We note that unless we simultaneously fit $M_N/\Lambda_\chi$ with $F_\pi$, we will not be able to disentangle some of these LECs, for example $c_0$, $\overline l^r_4$, and $\beta^\text{(2)}_N$. In this case we can instead fit $\tilde \beta^\text{(2)}_p = (c_0 \overline l^r_4  + \beta^\text{(2)}_p )$. If we combine the other LECs in this manner, then the only functional differences between the two equations (ignoring the LLO term in Eq.~\eqref{eq:sigma_mp_lam}) are the extra $\log \epsilon_\pi^2$ term at LO and $(\log \epsilon_\pi^2)^2$ term at N$^2$LO.

As a final caveat regarding the chiral expression, we comment that the $\mathcal J$ term at N$^2$LO probably cannot be resolved without simultaneously fitting the $\Delta$, which presents its own problems. For now we drop this term from our fits.

Finally, the only discretization corrections we consider for now are counterterms in the lattice spacing starting at $\mathcal O(\epsilon^2)$. However, as is clear from Eq.~\ref{fig:sigma_mn_extrapolations}, we could drop these terms; there is no appreciable lattice spacing dependence here.

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/mn_vs_epspi.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/mn_vs_eps2a.pdf}
	\end{subfigure}
	\caption{\label{fig:sigma_mn_extrapolations}
	Example extrapolation as a function of the pion mass (left) and lattice spacing (right). Notice that, unlike the $F_\pi$ extrapolation, the fit of $M_N/\Lambda_\chi$ has virtually no lattice spacing dependence. 
	}
\end{figure}


\subsection{Derivatives of fit functions}
Once we have fit our extrapolation functions, we must take the derivative with respect to $\epsilon_\pi$ (while keeping track of the correlations for purposes of error propagation). The most straightforward way to calculate this derivative is by taking the derivative of Eq.~\eqref{eq:sigma_mn_chiral}, but we will separate out the $M_N/\Lambda_\chi$ and $\Lambda_\chi$ derivatives so we can estimate their individual contributions to $\sigma_{\pi N}$. 

\begin{align}
\epsilon_\pi \Lambda_\chi \frac{\partial (M_N/\Lambda_\chi)}{\partial \epsilon_\pi}  = 
\epsilon_\pi \Lambda_\chi \Bigg\{&\overbrace{-\frac{M_p^{(0)}}{\Lambda_\chi^2} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}}^\text{starts at LLO}
& \text{(LLO)} \nonumber \\
&+ 2\beta^\text{(2)}_p \epsilon_\pi  
& \text{(LO)} \nonumber \\
&- \frac{9\pi}{2} g_{\pi pp}^2 \epsilon_\pi^2  
- \frac{4}{3} g_{\pi p\Delta}^2 \partial_{\epsilon_\pi}\mathcal{F}(\epsilon_\pi, \epsilon_{p\Delta}) 
& \text{(NLO)} \nonumber \\
&+ \gamma_p^\text{(4)} \Big[
    2 \epsilon_\pi \mathcal{J} (\epsilon_\pi, \epsilon_{p \Delta}) 
    + \epsilon_\pi^2 \partial_{\epsilon_\pi} \mathcal{J} (\epsilon_\pi, \epsilon_{p \Delta}) 
\Big] &\text{(N$^2$LO)}  \nonumber \\
&\qquad + \alpha_p^\text{(4)} \Big[
    4 \epsilon_\pi^3 \log{\epsilon_\pi^2} 
    + 2 \epsilon_\pi^3
\Big]
+ 4 \beta_{p}^{(4)} \epsilon_\pi^3
\Bigg\}
\end{align}
The derivatives of the non-analytic functions are listed below.
\begin{align}
  \frac{\partial \mathcal F}{\partial \epsilon_\pi}
  & = \frac{2 \epsilon_\Delta ^3}{\epsilon_\pi }
  -3 \epsilon_\Delta  \epsilon_\pi  \log \left(\epsilon_\pi ^2\right)
  -3 \epsilon_\Delta  \epsilon_\pi +\left(\frac{2 \epsilon_\pi ^3}{\epsilon_\Delta }
  -2 \epsilon_\Delta  \epsilon_\pi \right) R'\left(\frac{\epsilon_\pi ^2}{\epsilon_\Delta ^2}\right)
  +2 \epsilon_\Delta  \epsilon_\pi  R\left(\frac{\epsilon_\pi ^2}{\epsilon_\Delta ^2}\right)
  \\
  \frac{\partial \mathcal J}{\partial \epsilon_\pi}
  &= -\frac{4 \epsilon_\Delta ^2}{\epsilon_\pi }+4 \epsilon_\pi  R'\left(\frac{\epsilon_\pi ^2}{\epsilon_\Delta ^2}\right)+2 \epsilon_\pi  \log \left(\epsilon_\pi ^2\right)+2 \epsilon_\pi
  \\
  R'(x) &=\begin{array}{cc}
      &  
      \left\{ \begin{array}{cc}
      \frac{1}{x}-\frac{\log \left(\frac{1-\sqrt{1-x}}{\sqrt{1-x}+1}\right)}{2 \sqrt{1-x}} & 0<x<1 \\
      2 & x=1 \\
      \frac{1}{x}+\frac{\tan ^{-1}\left(\sqrt{x-1}\right)}{\sqrt{x-1}} & x>1 \\
     \end{array} \right.
      \\ 
     \end{array}
\end{align}


The $\Lambda_\chi$ derivative at LLO can be combined with the $\Lambda_\chi$ derivative in Eq. \eqref{eq:sigma_lambda_chi}. Expanding to the first non-zero term, we have

\begin{align}
\epsilon_\pi \left(\frac{M_N}{\Lambda_\chi} -\frac{M_p^{(0)}}{\Lambda_\chi} \right)\frac{\partial \Lambda_\chi}{\partial \epsilon_\pi} &\approx \epsilon_\pi
\left(  
  \beta^{(2)}_p \epsilon_\pi^2 
  %- \frac{3\pi}{2} g_{\pi pp}^2 \epsilon_\pi^3  
\right)
\left(
  2 \epsilon_\pi (\overline l_4 - 1) \Lambda_\chi
\right) \\
&= 2 \beta^{(2)}_p (\overline l_4 - 1) \Lambda_\chi \epsilon_\pi^4 \, . 
& \text{(N$^2$LO)} \nonumber
\end{align} 

\section{Results}
In this preliminary exploration for $\sigma_{\pi N}$, we consider a mere 4 models: Taylor terms to $\mathcal O(\epsilon^4)$ with either zero, LO, NLO, or N$^2$LO \xpt corrections. The discretization errors are modeled at $\mathcal O(\epsilon^2)$ for the $F_\pi$ fits, with the posterior for the discretization LECs compatible with zero for the $M_N/\Lambda_\chi$ fits. We omit higher order discretization counterterms as these tank the weights. These models are compared in Fig.~\ref{fig:sigma_histograms}. After averaging, we find
\begin{align}
	M_N &= 951.5(5.4)^\text{s}(1.7)^\chi(0.0)^a(5.8)^\text{phys}(3.8)^\text{M} \text{ MeV} &=951.5(9.0) \text{ MeV}
	\\
	\sigma_{\pi N} &= 47.6(1.8)^\text{s}(1.7)^\chi(0.0)^a(0.6)^\text{phys}(5.2)^\text{M} \text{ MeV} &=47.6(5.9) \text{ MeV}
\end{align}

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/histogram_mn.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/histogram_sigma.pdf}
	\end{subfigure}
	\caption{\label{fig:sigma_histograms}
  Histograms of $M_N=(M_N/\Lambda_\chi)\Lambda_\chi^\text{phys}$ and $\sigma_{N \pi}$. The result for $M_N$ agrees with the PDG value (thin red band), while the histogram for $\sigma_{N \pi}$ must be interpreted more carefully. The red band on the right is the result from BMW 2020, a typical lattice determination of the sigma term which has been included for reference. We find that the distribution is multimodal, with the Taylor expansion contributing the most weight to the model average. However, we caution against giving too much credence to the Taylor fit: although we have priored our models equally here, we have \emph{a priori} reason to believe the Taylor fit is misleading us. Specifically, we do not believe the Taylor model accurately captures the lattice spacing dependence for $\sigma_{\pi N}$, as we expect the contribution from $\Lambda_\chi$ to compete against the LLO contribution from $M_N/ \Lambda_\chi$, which is not possible if the LLO term is just a constant. 
	}
\end{figure}

As previously hinted at, we can make an amusing observation if we split the sigma term in the following manner.

\begin{equation*}
	\sigma_{N\pi} = \frac 12 \epsilon_\pi \left[
		1 
		+ \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right) 
		+ \mathcal{O} \left(\epsilon_\pi^3\right)
	\right]
	\overbrace{
	\left[
		{\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}}
		+ {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}}
	\right]
	}^{\frac{\partial M_N}{\partial \epsilon_\pi}}
\end{equation*}

If we calculate each of these terms individually (including the $\epsilon_\pi/2$ prefactor), we find
\begin{align*}
	\frac 12 \epsilon_\pi {\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}} &=
	\frac 12 \Lambda_\chi^* \left[
		\left({\color{RubineRed} -2 c_0 \left(\overline \ell_4  - 1 \right)}  + 2 \beta^\text{(2)}_N \right) \epsilon_\pi^2 
		+ \mathcal{O}\left(\epsilon_\pi^3\right)
	\right] 
	\sim 10 \text{ MeV} 
	\\
	\frac 12 \epsilon_\pi {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}} &=
	\frac 12 M_N^* \left[ {\color{RubineRed} 2 \left(\overline \ell_4  - 1 \right)\epsilon_\pi^2 } 
	+ \mathcal{O}\left(\epsilon_\pi^3\right) \right]
	\sim 40 \text{ MeV}
\end{align*}

We make two important observations: (1) the bulk of the contribution to the sigma term comes from the fit of $\Lambda_\chi$, not $M_N/\Lambda_\chi$. (2) The determination of the sigma term is highly sensitive to the value of $\overline l_4$, as demonstrated in Fig.~\ref{fig:sigma_l4_dependence}. A precise determination of $F_\pi$ (from which we can extract $\overline l_4$) is therefore essential to better constrain our result for the sigma term.


\begin{figure}
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/flag_l4.pdf}
	\end{subfigure}
  ~
  \begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/sigma_term/sigma_vs_l4_mean.pdf}
	\end{subfigure}
  %\includegraphics[width=0.7\textwidth]{./figs/sigma_term/sigma_vs_l4_mean.pdf}

  \caption{\label{fig:sigma_l4_dependence} Left: FLAG average for $\overline l_4$, including results from 
  RBC/UKQCD~15E~\cite{Boyle:2015exm},
  Gulpers~\cite{Gulpers:2015bba},
  Brandt~\cite{Brandt:2013dua},
  BMW~13~\cite{Budapest-Marseille-Wuppertal:2013vij},
  Borsanyi~\cite{Borsanyi:2012zv},
  NPLQCD~11~\cite{Beane:2011zm},
  MILC~10~\cite{MILC:2010hzw},
  ETM~11~\cite{ETM:2010cqp},
  ETM~09C~\cite{ETM:2009ztk},
  ETM~08~\cite{Frezzotti:2008dr}
  (figure taken from~~\cite{Aoki:2021kgd}).
  Right: LO dependence of $\sigma_{\pi N}$ on $\overline l_4$ (blue band). Like $\sigma_{\pi N}$, there is a great deal of variation in measurements of $\overline l_4$ . If $\overline l_4$ is on the smaller side, we get something close to a typical lattice calculation; if $\overline l_4$ is instead large, we get something closer to the phenomenological result. A precise fit of $F_\pi$ is therefore integral to precisely determining $\sigma_{\pi N}$ using the strategy outlined in this chapter.}
\end{figure}


In future work, we plan to include a fit of $m_\pi$ in our analysis so that we can determine $\overline l_3$. We will additionally fit $M_N$ directly using our scale setting to be compared the technique presented here in which we instead fit $M_N/\Lambda_\chi$ and $\Lambda_\chi$. 
