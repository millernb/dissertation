\chapter{Regarding lattice quantum chromodynamics}.
\label{chap:qcd}
\section{Quantum chromodynamics}

In the same space one could write the range equation for projectile motion, we can write the Lagrangian density for quantum chromodynamics (QCD), which describes the majority of visible matter in the universe. To wit,
\begin{equation}
\mathcal{L} = \sum_{f} \overline{q}_f \left( i \gamma^\mu D_\mu - m_f  \right) q_f - \frac14 G_{\mu\nu}^a G^{\mu\nu}_a \, .
\label{QCD-Lagrangian} 
\end{equation} 

A quantum field theory (QFT) is only as good as its symmetries; QCD imposes color SU(3) ``symmetry" (manifest in Roman indices $a, b, c, \ldots$) and Lorentz symmetry (manifest in the Greeks indices $\mu, \nu, \ldots$). Of course, there is nothing surprising about including the latter symmetry: all quantum field theories have it.

We'll avoid asking \emph{why} the degrees of freedom of the QCD Lagrangian, i.e.\ the gluon and quark fields, have the quantum numbers they have: that question can be (unsatisfactorily) answered with ``because experiment demands so." Instead, we'll see how the particular quantum numbers demanded by the theory (e.g., gluons being spin-1 particles) require the Lagrangian written above.

\subsection{A refresher on Lie groups, their algebras, and their representations}
To check whether a Lagrangian respects a proposed symmetry, we verify that the Lagrangian remains invariant under a transformation of the fields by that symmetry. In practice this means that we codify a symmetry by expressing it in terms of some (matrix) Lie group. Often, rather than work directly with the Lie groups (which in general can be quite messy), we instead work with the Lie algebras (which are linear). These objects are not always carefully between in the physics literature.

Colloquially, we often talk about Lie groups as being exponentiated Lie algebras. More rigorously, if $G$ is a Lie group, then we define the \emph{Lie algebra} of G as~\cite{hall2000elementary}
\begin{equation}
\mathfrak{g} = \left\{ \, X \mid e^{i tX} \in G \quad  \forall t \in \mathbb{R}  \, \right\} \, .
\end{equation}
From this definition, the following theorem follows: if $X, Y \in \mathfrak{g}$, then also  $i (XY -YX) \in \mathfrak{g}$, which perhaps explains the ubiquity of Lie brackets in QFT. Alternatively, one can endow a vector space $V$ with a bilinear operation $[\cdot, \cdot] : V \times V \rightarrow V$ and (assuming a few other requirements are met) generate a Lie algebra that way.

As perhaps the most important example in QFT, consider the Lorentz group, which is the group of rotations and Lorentz boosts (technically we're interested in a subset of the Lorentz group, the restricted Lorentz group $\text{SO}^+(1, 3)$, which forces time and parity operations to be discrete rather than continuous). The corresponding Lie algebra $\mathfrak{so}(1, 3)$ is just the vector space $\mathbb{C}^{4 \times 4}$ with the Lie bracket~\cite{maciejko}
\begin{align} \label{lorentz-algebra-lie-bracket}
\left[ \mathcal J^{\rho \sigma}, \mathcal J^{\tau \nu}  \right] &=
i \left(
\eta^{\sigma \tau} \mathcal J^{\rho \nu}
- \eta^{\rho \tau} \mathcal J^{\sigma \nu}
+ \eta^{\rho \nu} \mathcal J^{\sigma \tau}
- \eta^{\sigma \nu} \mathcal J^{\rho \tau}
\right) \, \\
&= i {f^{\rho \sigma \tau \nu}}_{\alpha \beta} \mathcal{J}^{\alpha \beta}
\end{align}
where $\mathcal J^{\mu \nu }$ is an infinitesimal generator of the Lorentz group. A generic element of the Lorentz Lie group can then be written
\begin{equation}
{\Lambda^\alpha}_\beta = {\left[ \exp \left( - i \omega_{\mu \nu} \mathcal{J}^{\mu \nu} \right) \right]^\alpha}_\beta
\end{equation}
where the antisymmetric $\omega_{\mu \nu}$ is a parameter of the transformation (one can think of $J^{\mu \nu }$ as being the basis elements and $\omega_{\mu \nu}$ as selecting a particular $\Lambda$).

Of particular interest to us are \emph{representations} of the Lorentz group. A (matrix) representation $D$ of the Lorentz groups is a map (technically, homomorphism) taking the Lorentz group to the set of invertible $n \times n$ matrices, i.e.
\begin{equation}
D : \text{SO}^+(3, 1) \rightarrow GL(n; \mathbb C)
\end{equation}
such that $D[\Lambda_1] D[\Lambda_2] = D[\Lambda_1 \Lambda_2]$ (see Chapter 4 of~\cite{tong_qft_notes}). Further, every representation of a Lie group gives rise to a representation of its corresponding Lie algebra (see Theorem 3.18 of~\cite{hall2000elementary}), and often the converse holds too. In the next section, we'll see how these representations are used to characterize particles of different spins.

\subsection{Lorentz symmetry}
\label{sect:lorentz_symmetry}

 To understand how Lorentz symmetry is encoded into QCD, we must dig a little deeper into its Lagrangian. First consider the gluon fields $A^a_\mu$, which are hidden inside $G^a_{\mu \nu}$:
\begin{equation}
G_{\mu\nu}^a = \partial_\mu A_\nu^a - \partial_\nu A_\mu^a + g f_{a b c} A_\mu^b A_\nu^c \, .
\label{QCD-Langrangian-G}
\end{equation}
For now, we are only interested in the Lorentz (Greek) indices.

In general, given a field $\phi^\alpha$, we require the field transform under a Lorentz transformation $\Lambda$ as
\begin{equation}
\phi^\alpha(x) \rightarrow \phi^{\prime \alpha}(x) = {D[\Lambda]^\alpha}_\beta \phi^\beta (\Lambda^{-1} x)
\end{equation}
where the matrices $D[\Lambda]$ form a representation of the Lorentz (Lie) group. When constructing the QCD Lagrangian, we might first consider what the correct representation is for the quark fields with respect to Lorentz symmetry. For instance, given the Klein-Gordon Lagrangian
\begin{equation}
\mathcal{L}_\text{K-G} = \frac12 \left( \partial_\mu \phi \partial^\mu \phi - m^2 \phi^2 \right)
\end{equation}
the scalar (spin-0) field $\phi$ need only transform per the trivial representation in order to preserve Lorentz symmetry: $D[\Lambda] = \mathbf 1$. That is,
\begin{equation}
\phi(x) \rightarrow \phi^\prime(x) = \phi(\Lambda^{-1} x) \, .
\end{equation}
It's straightforward to check that the Klein-Gordon Lagrangian remains unchanged when we take $\phi(x) \rightarrow \phi^\prime(x)$: just apply the chain rule and use the relation $\eta^{\sigma \tau} {\Lambda^\mu}_\sigma {\Lambda^\nu}_\tau = \eta^{\mu \nu}$ along with ${\Lambda_\nu}^\mu = {(\Lambda^{-1})^\mu}_\nu$.

Next, consider a vector (spin-1) field $A_\mu$. The simplest example we can concoct is quantum electrodynamics (QED) without sources, i.e. QED with photons only. The corresponding Lagrangian is
\begin{equation}
\mathcal{L}_\text{QED-light} = - \frac 14 F_{\mu \nu} F^{\mu \nu} \,
\end{equation}
where $F_{\mu \nu} = \partial_\mu A_\nu - \partial_\nu A_\mu$. As a vector field, $A_\mu$ transforms per the fundamental representation: $D[\Lambda] = \Lambda$. Thus
\begin{equation}
A_\mu (x) \rightarrow A_\mu^\prime(x) = {\Lambda_\mu}^\nu A_\nu (\Lambda^{-1} x) \, .
\end{equation}
Again, one can check that under a Lorentz transformation of the fields, in this case $A_\mu (x) \rightarrow A_\mu^\prime(x)$, the Lagrangian remains unchanged.

The similarity between $F_{\mu \nu}$ and $G^a_{\mu \nu}$ is obvious, especially if we suppress the color indices. One might worry about the extra term quadratic in the gluon fields, but this term is Lorentz invariant for the same reason $\partial_\mu \phi \partial^\mu \phi$ is Lorentz invariant in the scalar theory. All of this is to be expected: the gluon, like the photon, is a spin-1 particle.

So far we have considered two different representations of the Lorentz group: the trivial representation, which gives rise to spin-0 particles, and the fundamental representation, which gives rise to spin-1 particles such as the gluon. In some sense, these are the simplest representations of the Lorentz group. However, experiments require that the quarks be neither spin-0 nor spin-1 but spin-$\frac 12$ particles. To that end, we consider the Dirac equation
\begin{equation}
\left(i \gamma^\mu \partial_\mu - m \right) \phi = 0 \, .
\end{equation}

Historically, the Dirac equation stems from Dirac's attempt to write a Lorentz invariant wave equation \emph{linear} in $\partial_\mu$ (contrast to the Klein-Gordon Lagrangian above). If $\gamma^\mu \rightarrow a^\mu$ is just an ordinary number, this task is evidently impossible: $a^\mu \partial_\mu$ is a directional derivative with a clearly preferred direction. But if we act on this wave equation with $(i \gamma^\mu \partial_\mu + m)$, we have that
\begin{equation}
\left(- \gamma^\mu \gamma^\nu \partial_\mu \partial_\nu + m^2 \right) \phi = 0 \, .
\end{equation}
Using the fact that derivatives commute, we can write $\gamma^\mu \gamma^\nu \partial_\mu \partial_\nu = \frac 12 \{ \gamma^\mu, \gamma^\nu \} \partial_\mu \partial_\nu$. From this, we see that $\{ \gamma^\mu, \gamma^\nu \} = 2 \eta^{\mu \nu}$, which means that $\gamma^\mu$ and $\gamma^\nu$ must be an elements of the Clifford algebra $C\ell_{1, 3}(\mathbb R)$.

So far we've spoken of representations of Lie groups, but as mentioned in the previous section, one can also have representations of Lie algebras. One representation of the Lorentz Lie algebra \eqref{lorentz-algebra-lie-bracket} is
\begin{equation}
\tilde D [\mathcal{J}^{\rho \sigma}] = \mathcal{S}^{\rho \sigma} = \frac i4 \left[ \gamma^\rho, \gamma^\sigma \right]
\end{equation}
from which we can define the adjoint representation of the Lorentz group: $D[\Lambda] = \exp \left( - i \omega_{\mu \nu} \mathcal{S}^{\mu \nu} \right)$. Like the fundamental representation, the adjoint representation is also composed of $4\times 4$ matrices; however, it's worth keeping in mind that this representation is unique. The Dirac field therefore transforms as
\begin{equation}
\phi^\alpha(x) \rightarrow \phi^{\prime \alpha}(x) = {\left(e^{- i \omega_{\mu \nu} \mathcal{S}^{\mu \nu}}\right)^\alpha}_\beta \phi^\beta (\Lambda^{-1} x ) \, .
\end{equation}


Now we're equipped to understand the Dirac Lagrangian,
\begin{equation}
\mathcal{L} = \overline \phi (i \gamma^\mu \partial_\mu - m ) \phi \, .
\end{equation}
Here we define $\overline \phi = \phi^\dagger\gamma^0$, where $\gamma^0 = I \otimes \sigma_3$ (the factor of $\gamma^0$ is required for the Lagrangian to remain Lorentz invariant since $D[\Lambda]$ is not unitary for this particular representation). Following some algebra involving $\gamma$-matrices, we see that this Lagrangian is indeed Lorentz invariant under $\phi \rightarrow \phi^\prime$.

Of course, this is all analogous to the case of QCD. With respect to Lorentz symmetry, the quark fields transform in the same way.




\subsection{Color SU(3)}
\label{sect:color_su3}

Even after the success of Gell-Mann's eightfold way and the advent of the quark model in the early 1960s, there was one major hurdle to the quark model remaining: it appeared to violate the Pauli exclusion principle~\cite{griffiths_2014}. Particles like $\Delta^{++} = (u u u)$ required the quarks to all be in the same state if flavor and spin are all there is. In 1964, Oscar Greenberg proposed a solution in which the quarks also carried another quantum number---color. With this addition, the quark model was relatively complete, baring the inclusion of yet unseen heavier quarks. (Nevertheless, it still took another ten years or so before physicists fully accepted the quark model).

Experiments show that quarks must come in three different colors (and three different anti-colors), which we label as red, blue, and green. There are also strong theoretical reasons to believe this, too: due to the triangle anomaly, the Standard Model is only internally consistent if quarks come in three colors (see Chapter~4.2 of~\cite{greiner_schramm_stein_2007}).

\begin{table}[]
	\begin{tabular}{c|c}
		\hline\hline
		$f_{123}$                                                                                                              & 1                    \\ \hline
		\begin{tabular}[c]{@{}c@{}}$f_{147} \qquad -f_{156}$\\ $f_{246} \qquad \phantom{-}\,\, f_{257}$\\ $f_{345} \qquad -f_{367}$\end{tabular} & $\frac 12$           \\ \hline
		$f_{458} \qquad f_{678}$                                                                                                & $\frac{\sqrt{3}}{2}$ \\ \hline\hline
	\end{tabular}
	\caption{Structure constants of $\mathfrak{su}(3)$.}
	\label{su3_structure_constants}
\end{table}

Much of our work in the previous section carries over to explain the color symmetry of QCD.
\footnote{Technically, only \emph{global} SU(3) color is a symmetry, as the gauge bosons play an active (dynamical) role in the theory; thus when we promote this global gauge symmetry to a local one, it is more accurate to describe the resulting local SU(3) gauge ``symmetry'' as a gauge redundancy~\cite{schwichtenberg:2019demystifying}.}
If one is willing to accept that the quarks transform according to the fundamental representation of some color group that forms a color triplet $q_f = ( q^r_f \enspace q^b_f \enspace q^g_f )^T$, then that group must necessarily be SU(3). (To use technical jargon, SU(3) is the only semi-simple Lie group with complex irreducible triplets; again, see~\cite{greiner_schramm_stein_2007}). The Lie algebra $\mathfrak{su}(3)$ of the color group is described by its Lie bracket
\begin{equation}
[\lambda^a, \lambda^b] = 2 i f^{abc} \lambda_c
\end{equation}
where the $\lambda^a$ are the Gell-Mann matrices (i.e., the SU(3) analogs of the SU(2) Pauli matrices). For completeness, the structure constants are given in Table \ref{su3_structure_constants}. As stated before, it is natural to assume (and indeed, it is the case) that the quarks transform per the fundamental representation of color SU(3):
\begin{equation}
q_f^a \rightarrow q_f^{\prime a} = {\left( e^{-i \theta_c \lambda^c/2} \right)^a}_b q_f^b
\end{equation}

The gluons must be in the adjoint representation of SU(3). The easiest way to see this is by considering the dimensions of the irreducible representations of SU(3). If color charge is conserved, then gluons must carry away the excess charge; e.g., if $q_f(r)$ becomes $q_f(b)$, then the outgoing gluon must have quantum numbers $g(r, \overline b)$. From this, one sees there are 9 possible combinations of color/anti-color combinations and might assume that there are 9 gluons. However, this cannot be the case: were it so, the color singlet $(r \overline r + b \overline b + g \overline g)/\sqrt{3}$ would be ubiquitous in nature, revealing itself as a long-range force between hadrons. Evidently such particles do not exist, leaving us with 8 gluons~\cite{griffiths_2014}. And since there is only one irreducible representation of SU(3) with dimension 8---the adjoint representation---the gluons must transform per this representation.

The adjoint representation $\Gamma$ of a Lie algebra can be defined in terms of its structure constants (see Chapter 3.3 of~\cite{shapiro_2017}). That is, ${\Gamma[\lambda^c]^a}_b = i {f^a}_{cb}$, so the the gluon fields transform under color as
\begin{equation}
A^a \rightarrow A^{\prime a} = {\left( e^{-i \theta_c {f^c}_{de}} \right)^a}_b A^b \, .
\end{equation}

The last piece of the QCD Lagrangian to explain is the covariant derivative.
\begin{equation}
D_\mu = \partial_\mu - i g \frac{\lambda_a}{2} A^a_\mu
\end{equation}
The covariant derivative acts to parallel transport a field from $x^\mu \rightarrow x^\mu + \epsilon^\mu$ in a manner analogous to how the covariant derivative transports a vector in general relativity (see Chapter 4.1 of~\cite{greiner_schramm_stein_2007}). In general relativity, the derivative is corrected using the Christoffel symbols, which connects different points on the manifold; in QCD, the extra term accounts for the positional dependence of the gauge. By taking the commutator of the covariant derivatives, we get the piece of the gauge that is physical, 
\begin{equation}
[D_\mu , D_\nu] = \frac{\lambda_a}{2} G^{a}_{\mu \nu}.
\end{equation}

In principle, one could check that the QCD Lagrangian is invariant under the transformations $q_f \rightarrow q^\prime_f$ and $A \rightarrow A^\prime$ as defined above, but that would be quite unusual. I've written the transformations in this manner only to show the similarity to the Lorentz group representations. (For the conventional approach, see e.g. Chapter 2.1 of~\cite{Gattringer:2010zz}.)

\section{Lattice QCD} \label{sect:lattice_qcd}
Central to lattice QCD is the path integral formulation of QFT. Recall that the correlation function of two operators can be written
\begin{equation}
\label{eq:qcd_path_integral}
\langle O_2(t) O_1(0) \rangle = \frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[A] \,  e^{i S_{\text{QCD}}[q, \overline q, A]} O_2[q, \overline q, A] O_1[q, \overline q, A]
\end{equation}
where the integral is over each of the six quark flavors
\begin{equation}
\mathcal{D}[q, \overline q] = \prod_{f\in\{ u, d, s, c, b, t\}} \mathcal{D}[q_f, \overline q_f]\, ,
\end{equation}
the partition function $Z_0$ is just the integral without the operators
\begin{equation}
Z_0 = \int \mathcal{D}[q, \overline q] \mathcal{D}[A] \,  e^{i S_{\text{QCD}}[q, \overline q, A]} \, ,
\end{equation}
and the QCD action is integrated from $0$ to $t$ (and over space)
\begin{equation}
S_{QCD} = \int_0^t d^4 x \, \mathcal{L}_{QCD} \, .
\end{equation}

Unlike the path integral formulation of quantum mechanics, in QFT the degrees of freedom are no longer points in space but the fields (which themselves are positioned at some point in space). In quantum mechanics, we think of the path integral as being some sort of weighted average for a particle to get from $x_A$ to $x_B$, following every conceivable path. In QFT, the fields already permeate ever point in space---rather than have the fields move, they fluctuate like springs on a mattress, with each fluctuation contributing to the likelihood that an excitation---a particle---flows from $x_A$ to $x_B$.

The need for lattice QCD stems from the running of the coupling constant in QCD. At high energies, we can approximate the coupling constant as
\begin{equation}
\alpha_S(Q^2) = \frac{g^2(Q^2)}{4\pi}\approx \frac{4\pi}{\beta_0 \log (Q^2 / \Lambda_\text{QCD}^2)}
\end{equation}
where $\Lambda_\text{QCD}$ is the QCD scale and $Q^2$ is the momentum transfer. However, as $\sqrt{Q^2} \rightarrow \Lambda_\text{QCD} \sim 200 $~MeV (i.e., for temperatures below $\Lambda_\text{QCD}/k_B \sim 10^{12}$ ${}^{\circ}$C), the coupling constant blows up~\cite{Deur:2016tte} (in fact, this is how we can define $\Lambda_\text{QCD}$). Consequently, perturbative methods fail in this regime: if we were to expand our path integral in terms of Feynman diagrams, the more complicated diagrams (those with more vertices) would actually contribute more greatly than the simpler ones; stated another way, at low energies we can no longer think of a nucleon as being composed of naught but three quarks. A nucleon is something much messier, constantly interacting with a sea of virtual quarks and gluons.

Without perturbative methods, we instead evaluate the path integral directly. Analytically, this task is almost certainly futile---with the exception of a few important cases, there is a no general schema for solving the path integral. Instead, we make a couple approximations: (1) we replace the infinite dimensional path integral with a finite dimensional integral by discretizing the location of the fields, forcing them to points on a lattice; and (2) rather than integrate over all of space, we integrate over a finite (and periodic) volume of space.

In fact, theoretically we're on better footing than we might expect: these two assumptions are regulators of the continuum field theory. The former serves as an ultraviolet cutoff (there can be no particles with momenta smaller than the lattice spacing), and the latter serves as an infrared cutoff (there can be no particles with momenta greater than twice the box length).

With these modifications of the path integral in mind, let's rewrite our path integral for the lattice. Splitting up the QCD action, the fermionic part becomes
\begin{align}
S_F[q, \overline q, A] &= \int d^4x \, \overline q(x) \left[i \gamma^\mu \left( \partial_\mu - i g \frac{\lambda_a}{2} A^a_\mu(x) \right) + m \right] q(x)
\\
&\phantom{=} \rightarrow a^4 \sum_{n \in \Lambda} \overline q(n) \left(
	\gamma^\mu \frac{U_\mu(n) q(n + \hat \mu) - U_{-\mu}(n) q(n - \hat \mu)}{2a} + m q(n)
\right)  = S_F[q, \overline q, U]
\, . \label{eq:naive_fermion_action}
\end{align}
Here we have introduced the link variables $U_\mu$, which we will explain shortly. If we temporarily turn off the gluons, the free fermionic action is obtained by taking $U_\mu \rightarrow 1$. In particular, we have that $\partial_\mu q(x) \rightarrow  (q(n+\hat n) - q(n - \hat n))/2a$, i.e. the partial derivative becomes the central difference.
\footnote{The central difference, compared to the forward or backward difference, has the advantage of introducing errors at $\mathcal O(a^2)$ instead of $\mathcal O(a)$.} 
So what about the link variables?

As mentioned previously, the covariant derivative $D_\mu$ (and its lattice equivalent) serves to parallel transport a field from one location to another. In particular, under a gauge transformation $\Omega \in \text{SU(3)}$, we take
\begin{equation}
q(n) \rightarrow q^\prime(n) = \Omega(n) q(n) \, ,
\qquad
\overline q(n) \rightarrow \overline q^\prime(n) =  \overline q(n) \Omega(n)^\dagger \, .
\end{equation}
Clearly the mass term remains invariant under such a transformation. However, the kinetic term does not.
\begin{align}
\overline q^\prime(x) D_\mu q^\prime(x) &\rightarrow  \overline q^\prime(n) \left[\frac{q^\prime(n+\hat\mu) - q^\prime(n - \hat \mu)}{2a} \right]
+\text{parallel transport}
\\
&\phantom{\rightarrow} = \overline q(n) \Omega(n)^\dagger \left[\frac{\Omega(n + \hat \mu) q(n + \hat \mu) - \Omega(n - \hat \mu) q(n - \hat \mu)}{2a} \right]
+\text{parallel transport}\end{align}
(The gauge terms $\Omega(n)^\dagger \Omega(n+\hat{\mu})$ need not cancel since, in general, we can have a different transformation at each point on the lattice.) To ensure that the kinetic term remains invariant under a gauge transformation, we introduce link variables $U_\mu(n)$ such that
\begin{equation}
U_\mu (n) \rightarrow U^\prime_\mu (n) = \Omega(n) U_\mu(n) \Omega(n+\hat\mu)^\dagger \, .
\end{equation}
By including these variables (and noting that $U_{-\mu}(n) = U_\mu (n - \hat \mu)^\dagger$), we see that the fermionic action is invariant under a transformation $\{q, \overline q, U_\mu\} \rightarrow \{ q^\prime, \overline q^\prime, U^\prime_\mu\}$ .

Finally, as a point on terminology, we note that the link between adjacent lattice sites can alternatively be written in matrix notation as
\begin{equation}
S_F[q, \overline q] = a^4 \sum_{n_1, n_2 \in \Lambda} \overline q_{n_1} D_{n_1 n_2} q_{n_2} \, .
\end{equation}
In the lattice literature, such a matrix $D_{n_1 n_2}$ is referred to as a Dirac operator.

Before we consider the gluonic part of the QCD action, it's worth asking: what happened to the gauge fields $A_\mu$? As it so happens, if we write
\begin{equation}
U_\mu (n) = e^{i a A_\mu(n)} = 1 + i a A_\mu(n) + \mathcal O(a^2)
\end{equation}
and substitute this definition of $U_\mu$ into \eqref{eq:naive_fermion_action}, we recover the (discretized) version of the fermionic action with the original gauge fields. But using $U_\mu$ instead of $A_\mu$ isn't merely some book-keeping trick; it amounts to a change in the degrees of freedom in our theory, effecting us to change the measure in our path integral: $\mathcal D[q, \overline q] \mathcal D[A] \rightarrow \mathcal D[q, \overline q] \mathcal D[U]$. 
\footnote{That is, we have changed from using algebra-valued fields to group-valued fields; see Chapter~2.2 of~\cite{Gattringer:2010zz}.}

\begin{figure}
	% https://q.uiver.app/?q=WzAsNSxbMCwwLCJcXHN1YnN0YWNre24rXFxoYXQgXFxudSBcXFxcIFxcYnVsbGV0fSJdLFsyLDAsIlxcc3Vic3RhY2t7bitcXGhhdCBcXG11ICsgXFxoYXQgXFxudSBcXFxcXFxidWxsZXR9Il0sWzIsMiwiXFxzdWJzdGFja3tcXGJ1bGxldFxcXFxuK1xcaGF0IFxcbXV9Il0sWzAsMiwiXFxzdWJzdGFja3tcXGJ1bGxldCBcXFxcbn0iXSxbMSwxLCJVX3tcXG11IFxcbnV9Il0sWzMsMiwiVV97XFxtdX0iLDJdLFsxLDAsIlVfey1cXG11fSIsMl0sWzIsMSwiVV9cXG51IiwyXSxbMCwzLCJVX3stXFxudX0iLDJdXQ==
	\begin{tikzcd}[ampersand replacement=\&]
		%\tikzstyle{every node}=[font=\small]
		{\substack{n+\hat \nu \\ \bullet}} \&\& {\substack{n+\hat \mu + \hat \nu \\\bullet}} \\
		\& {U_{\mu \nu}} \\
		{\substack{\bullet \\n}} \&\& {\substack{\bullet\\n+\hat \mu}}
		\arrow["{U_{\mu}}"', from=3-1, to=3-3]
		\arrow["{U_{-\mu}}"', from=1-3, to=1-1]
		\arrow["{U_\nu}"', from=3-3, to=1-3]
		\arrow["{U_{-\nu}}"', from=1-1, to=3-1]
	\end{tikzcd}
	\caption{\label{fig:plaquette} Plaquette $U_{\mu \nu}$. }
\end{figure}

Continuing with our discussion on how to discretize the QCD action, we now write the gluonic part.
\begin{align}
S_G[U] &= \frac 14 \int d^4x \, G^a_{\mu \nu} G_a^{\mu \nu} \\
&\phantom{=} \rightarrow 2 \sum_{n \in \Lambda} \sum_{\mu <\nu} \text{Re tr} \left[ 1 - U_{\mu \nu}(n) \right] = S_G[U]
\end{align}
Central to understanding this action is the plaquette, defined as
\begin{align}
\label{eqn:plaquette}
U_{\mu \nu} (n) &= U_\mu (n) U_\nu (n +\hat \mu) U_{-\mu} (n +\hat \mu + \hat \nu) U_{-\nu} (n + \hat \nu) \\
&= U_\mu (n) U_\nu (n +\hat \mu) U_{\mu} (n + \hat \nu)^\dagger U_{\nu} (n)^\dagger \, .
\end{align}
(See Fig.~\ref{fig:plaquette}.) Using the gauge transformation for link variables given above, it's easy to verify that the plaquette is gauge invariant.

We should check that the discretized gluonic action written in terms of the link variables $U_\mu$ reduces to the gluonic action written in terms of the gauge fields $A_\mu$. By substituting in $U_\mu(n) = \exp (i a A_\mu (n))$, employing the Baker-Campbell-Hausdorf formula, and Taylor expanding the gauge fields (e.g., $A_\nu(n +\mu) = A_\nu(n) + a \partial_\mu (n) + \mathcal{O} (a^2)$), we eventually find that
\begin{align}
U_{\mu \nu} (n) &= \exp \left[ i a^2 G_{\mu \nu} (n) + \mathcal{O} (a^3) \right] \\
&\approx 1 + i a^2 G_{\mu \nu}(n)
\end{align}
and thus
\begin{align}
S_G [A] &\approx \frac{1}{4} \sum_{n \in \Lambda} \text{Re} \left\{1 - \left( 1 + i a^2 G_{\mu \nu}(n) \right)^a \left( 1 + i a^2 G^{\mu \nu} (n) \right)_a \right\}  \\
&= \frac{a^4}{4} \sum_{n \in \Lambda} G^a_{\mu \nu}(n)  G_a^{\mu \nu}(n)
\end{align}
as expected.

At this point, one might think we're finished. We have managed to discretize the path integral, reducing the number of integrals from infinity to some finite number. However, a typical lattice has something like $32^4 \approx 10^6$ lattice sites---far too many for us to perform directly. Instead we can only estimate the path integral using Monte Carlo techniques.

There is a snag, however; the exponential in \eqref{eq:qcd_path_integral} is imaginary, meaning that the phase will also matter when sampling, leading to the emergence of a \emph{sign problem}. To get around this issue, we perform a \emph{Wick rotation}, taking $t \rightarrow it$. The exponential therefore becomes real, with such a path integral known as a \emph{Euclidean} path integral. Now the expectation value of observables (as well as correlator of observables) can be calculated.
\begin{equation}
\langle O \rangle = \frac 1N \sum_{\{q, \overline q, U\}} O[q, \overline q, U] \qquad \text{where $\{q, \overline q, U\} \sim e^{-S[q, \overline q, U]}$}
\end{equation}
That is, we generate a particular field configuration with probability proportional to $e^{-S[q, \overline q, U]}$. We can ensure that a field configuration is generated per the correct distribution by using the Metropolis algorithm, e.g. Further details are available in Chapter 4 of~\cite{Gattringer:2010zz}.