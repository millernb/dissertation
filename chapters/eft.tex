\chapter{Effective theories applied to quantum fields}

Although one could, given near infinite resources, calculate the motion of a Frisbee from the Standard Model Lagrangian, one does not need to understand quantum field theory to understand Newtonian mechanics. Indeed we often have a clear separation of scales, which allows one to be a veterinarian, for example, without being a nuclear physicist. 

In physics vernacular, we can \emph{integrate out} the \emph{irrelevant} degrees of freedom that are inaccessible at a particular scale. The resulting description of physics is known as an \emph{effective theory}. When this concept is applied to quantum field theory, the resulting theory is known as an \emph{effective field theory}.

Lattice practitioners typically employ effective field theories in their work. By definition, the lattice can never produce data at the physical point, as that would require calculations at zero lattice spacing and infinite volume---clearly an oxymoron. At a minimum we must extrapolate to the infinite volume and continuum limit. Moreover, despite the fact that we could spend all of our computational resources generating data near the physical $u$- and $d$-quark masses, it is significantly cheaper to generate lattice data with the $u$- and $d$-quark masses tuned to larger values. Thus we typically extrapolate in the quark masses, too. Effective field theory, particularly chiral perturbation theory, serves as the tool for guiding these extrapolations.


\section{Effective theories}

Before defining an effective field theory, we first give some examples of effective theories. Although there is no standard definition for effective theories, effective theories share some common features.%
\footnote{See \cite{rivat2020effective} for alternate sets of \emph{desiderata}.}
First, the effective theory is necessarily incomplete---it is only valid in some regime. Consequently the effective theory may obfuscate the details of the complete, underlying theory. Second, there should be a clear separation of scales (e.g., an expansion parameter), so that we may estimate when the effective theory stops being a robust approximation of the full theory. Finally the effective theory should remain valid for its domain even if we do not understand the full theory.

\subsection{The harmonic oscillator}
The harmonic oscillator serves as the most ubiquitous example of an effective theory in physics~\cite{Wells:2012rla}, with applications ranging from quantum field theory to cosmology. In the 17th century, Robert Hooke used this law to describe linear-elastic bodies such as springs. Of course, when formulated in a more modern language, we see that Hooke's law has much broader applicability. Consider a generic potential $V(x)$ and expand around the minimum at $x_0$. 
\begin{equation}
V(x) = V(x_0) 
+ \left.\frac{d\,V}{d\,x} \right|_{x_0} x
+ \frac 12 \left.\frac{d^2V}{d\,x^2} \right|_{x_0} x^2
+ \frac 16 \left.\frac{d^3V}{d\,x^3} \right|_{x_0} x^3  + \cdots
\end{equation}
Since we are only interested in potential differences, we can ignore the constant term; moreover since we have expanded about a minimum, the linear term is necessarily 0. Therefore to next-to-leading (NLO) order, the expanded potential is 
\begin{equation}
V(x_0) = \frac 12  \left.\frac{d^2V}{d\,x^2} \right|_{x_0} x^2
+ \frac 16 \left.\frac{d^3V}{d\,x^3} \right|_{x_0} x^3  + \cdots
\end{equation}
and so long as 
\begin{equation}
x < x_\text{crit} = 3 \frac{\left.\frac{d^2V}{d\,x^2} \right|_{x_0}}{ \left.\frac{d^3V}{d\,x^3} \right|_{x_0} }
\end{equation}
the leading order (LO) term will dominate. For convenience, let us write $\frac 12 \left.\frac{d^2V}{d\,x^2} \right|_{x_0} = k_1$, and so forth, and $w_1^2 = k_1 / m$, and so forth. Then
\begin{equation}
m \ddot x = -k_1  x - k_2 x^2 \, ,
\end{equation}
or perhaps more suggestively, 
\begin{equation}
\ddot x = -\omega_1^2  x \left( 1 + \Lambda x \right) \, .
\end{equation}

In the limit where $\Lambda x \rightarrow 0$, it is straightforward to calculate the period ($T \rightarrow 2 \pi / \omega_1$). Thus given some measurements of the period, we could determine the parameter $w_1$ in our effective theory. But how do we know whether this single parameter sufficiently describes our theory at the scale at which we're working? With a little bit of thought, we realize that although the LO term is restoring, the NLO term has a preferred direction. Therefore if the NLO term cannot be neglected, there will be an asymmetry in the amount of time for the system to pass between the turning points $t_1$ and $t_2$, depending on whether the system is evolving from $t_1 \rightarrow t_2$ or $t_2 \rightarrow t_1$. 

If these half-periods are the same, we have good reason to suspect that this single-parameter effective theory is sufficient. Of course, it could be the case that there exist symmetries in the system (e.g., parity) that preclude the NLO term while leaving higher order terms in tact.

\subsection{Newtonian gravity from the weak field approximation}
Let us now consider Newtonian gravity as an effective theory of general relativity via \emph{linearized gravity}~\cite{Carroll:2004st}. Let us work in the regime where:
\begin{enumerate}
    \item test particles are slow compared to the speed of light ($v \ll c$), 
    \item the gravitational field is weak, and
    \item the bodies are rigid.
\end{enumerate}
Because the gravitational field is assumed to be weak, we perturbatively expand around the Minkowski metric,
\begin{equation}
g_{\mu \nu} = \eta_{\mu \nu} + h_{\mu \nu} \, ,
\end{equation}
with signature $\eta_{\mu \nu} = \text{diag}(-1, 1, 1, 1)$. Let us suggestively write
\begin{equation}
    \begin{bmatrix}
        h_{00} & h_{01} & h_{02} & h_{03}  \\
        h_{10} & h_{11} & h_{12} & h_{13}\\
        h_{20} & h_{21} & h_{22} & h_{23}\\
        h_{30} & h_{31} & h_{32} & h_{33}
    \end{bmatrix} 
    =
    \begin{bmatrix}
        - 2 \phi & A_1 & A_2 & A_3  \\
        A_1 & 0 & 0 & 0 \\
        A_2 & 0 & 0 & 0 \\
        A_3 & 0 & 0 & 0 
    \end{bmatrix}.
\end{equation}
Here we have used the rigid body assumption to set the $h_{ij}$ components equal to zero, as these components would ultimately correspond to shear forces in the body. 

Next we solve the geodesic equation,
\begin{equation}
\frac{d^2 x^\lambda}{d\, \tau^2} 
+ \Gamma^{\lambda}_{\mu \nu} \frac{d\,x^\mu}{d\,\tau} \frac{d\,x^\nu}{d\,\tau}
= 0 \, ,
\end{equation}
for this metric. In the slow-moving limit, the timelike part of the derivative dominates: $\frac{d \, x^0}{d\, \tau} \approx e^i$, so $\frac{d^2 x^0}{d\,\tau^2} \approx 0$. We concentrate on the derivative with respect to the spacial indices, $\frac{d^2 x^i}{d\, \tau^2} \equiv \ddot{x}^i$.

Solving the geodesic equation requires us to evaluate the Christoffel symbols, which parameterize the parallel transport of vectors.
\begin{align}
\Gamma^{\lambda}_{\mu \nu} &=
\frac 12 g^{\lambda \sigma} \left( 
    \partial_\nu g_{\sigma \mu} 
    + \partial_\mu g_{\sigma \nu} 
    - \partial_\sigma g_{\mu \nu}
\right) \\
&\approx
\frac 12 \eta^{\lambda \sigma} \left( 
    \partial_\nu h_{\sigma \mu} 
    + \partial_\mu h_{\sigma \nu} 
    - \partial_\sigma h_{\mu \nu}
\right)
\end{align}
In particular,
\begin{align*}
\Gamma^{i}_{00} \frac{d\,x^0}{d\,\tau} \frac{d\,x^0}{d\,\tau} &= \left( 
    \partial_0 A_i
    + \partial_i \phi
\right) e^i
&\rightarrow \partial_t \boldsymbol{A} + \boldsymbol{\nabla} \phi \, ,
\\
2 \Gamma^{i}_{j0} \frac{d\,x^j}{d\,\tau} \frac{d\,x^0}{d\,\tau} &= \left( 
    \partial_j A_i
    - \partial_i A_j
\right)
\dot{x}^j e^i
&\rightarrow - \dot{\boldsymbol{x}} \times \left( \boldsymbol{\nabla} \times \boldsymbol{A} \right) \, .
\end{align*}
We ignore the terms associated with the $\Gamma^k_{ij}$ Christoffel symbols as these will be suppressed by factors of $(v/c)^2$. Combining everything gives us 
\begin{equation}
\ddot{\boldsymbol{x}} = 
- \left(\partial_t \boldsymbol{A} + \boldsymbol{\nabla} \phi\right)
+ \dot{\boldsymbol{x}} \times \left(\boldsymbol{\nabla} \times \boldsymbol{A} \right) \, .
\end{equation}

The first term is the familiar Newtonian potential for the correct choice of gauge ($\partial_t \boldsymbol A = 0$). When $\nabla^2 \phi \sim \rho$ (the matter density), we recover Newton's law of universal gravitation. This is the \emph{gravitoelectric} field, to make an analogy to electromagnetism.

More curious is the second term, corresponding to the \emph{gravitomagnetic} field. Here we see that the effective theory provides an insight lacking in Newton's formulation of gravity. Like the magnetic field, the gravitomagnetic field is velocity-dependent, but it is suppressed compared to the gravitoelectric field by a factor of $v/c$. More generally there exists a gravitational analog to Maxwell's equations, which was first recognized by Heaviside in the 19th century even before the advent of relativity \cite{heaviside:1893gravitational}.

An estimate for the gravitomagnetic field of the earth can be found by analogy to the magnetic field generated by a rotating ball of uniform volume charge density; the only difference between the two expressions will be the constant prefactors. Taking the electromagnetic solution \cite{Pierrus:2018examples} and replacing the constants by dimensional analysis, we expect the gravitomagnetic field above Earth to be
\begin{equation}
\boldsymbol B_g(r, \theta) \sim \frac{G M R^2 \omega}{c^2 r^3 } \left(
    2\cos\theta \, \hat r
    + \sin \theta \, \hat\theta
\right)
\end{equation}
with $\theta$ the latitude, and so the strength of the gravitomagnetic acceleration on the surface should be roughly $G M \omega^2/c^2 \sim 10^{-12} g$. Gravity Probe B verified the presence of the gravitomagnetic field by measuring the Lense-Thirring precession of the Earth~\cite{Everitt:2011hp}.%
\footnote{In fact, the gravitomagnetic field equals half the Lense-Thirring precession, as described in Chapter~12 of~\cite{Popenco:2017relativity}. (Unfortunately for most of us, the reference is in German. However, the derivation is still surprisingly intelligible, assuming you know the starting point and conclusion.)}

\section{Effective field theories}
In an effective \emph{field} theory, one posits that the ultraviolet physics of the full theory are suppressed below a scale $\Lambda$, where often $\Lambda \sim M$ is the mass of the lightest particle excluded by the theory. By the uncertainty principle, these particles will not be able to propagate far, so their interactions will appear point-like. One then constructs the most general Lagrangian compatible with the symmetries of the full theory and matches (or measures) the coefficients of the effective Lagrangian.

Unlike a quantum field theory, which is renormalizable, an effective field theory need not be. Instead the Lagrangian becomes a tower of infinitely many terms organized by an expansion in the scale. Consider the following contrived extension of $\phi^4$-theory which will demonstrate the principles of effective field theories.
\begin{equation}
\mathcal L_\text{free} =
\frac 12 \left( \partial_\mu \phi\right)^2
- \frac 12 m^2 \phi^2
+ \lambda \phi^4
\end{equation}
Since we require the action be dimensionless, it is straightforward to see that the operators have mass dimension
\begin{equation}
\left[(\partial_\mu \phi)^2 \right] = 4 \qquad\text{and}\qquad \left[\phi\right]  = 1 \, .
\end{equation}
Under the effective field theory philosophy, we include all \emph{irrelevant} combinations of these operators (i.e., combinations with mass dimension greater than 4). Consequently the prefactors must have negative mass dimension (here the $c_{n,m}$ coefficients are dimensionless low energy constants).
\begin{equation}
\label{eq:eff_lagrangian_generic_scalar}
\mathcal L_\text{eff} =
\frac 12 \left( \partial_\mu \phi\right)^2
- \frac 12 m^2 \phi^2
+ \lambda \phi^4
+ \sum_{n=5}^{\infty}\sum_{m=0}^{4m \le n} \frac{c_{n,m}}{\Lambda^{n-4}} \left( \partial_\mu \phi\right)^{2m} \phi^{n-4m}
\end{equation}
If the theory is ``natural'' each coefficient will be $\mathcal O(1)$, with the higher order terms being suppressed by additional powers of $\Lambda$. We can further reduce the number of terms in this expansion by imposing symmetry constraints; for instance, if we require $\phi$ to be parity invariant, then the terms with odd powers of $\phi$ would drop out. 

\subsection{Two scalar fields}
Although the previous example shows the elements of an effective field theory, it is unclear what $\Lambda$ should be. Let us now consider the simplest non-trivial effective field theory: two free scalar fields, $\phi$ and $\rho$, with an interaction term \cite{kaplan:2016eftnotes, Falkowski:2020eftnotes}. Let us further assume that the $\rho$ field is much heavier than the $\phi$ field, $M \gg m$. The full Lagrangian is
\begin{equation}
\mathcal L = \frac 12 \left(\partial_\mu \phi\right)^2 
+ \frac 12 \left(\partial_\mu \rho\right)^2 
- m^2 \phi^2  
- M^2 \rho^2 
+ \kappa \phi^2 \rho \, .
\end{equation}
(Note $[\kappa] = [m] = [M] = 1$.) Recall that under the path integral formulation observables are weighed by a factor of $\exp(iS[\phi, \rho])$. When $\Lambda \ll M$, we expect the heavy scalar field to have little influence in mediating interactions. We would therefore like to integrate-out the heavy field so that we have an effective field theory involving only the light field. That is, 
\begin{equation}
e^{i S_\text{eff}[\phi]} \approx \int \mathcal D[\rho] e^{i S[\phi, \rho]} \, .
\end{equation}
If we Wick rotate ($i S \rightarrow -S$), it is clear that the weight is maximal when $\delta S/\delta \rho = 0$.
\begin{equation}
\frac{\delta S}{\delta \rho} 
= \int \, d^4x \left[
    - \partial^2 \phi \frac{\delta \phi}{\delta \rho} 
    - \partial^2 \rho 
    - 2 m^2 \phi \frac{\delta \phi}{\delta \rho} 
    - 2 M^2 \rho 
    - \kappa \left(\phi^2 + 2 \phi \rho \frac{\delta \phi}{\delta \rho}\right)
\right]
= 0
\end{equation}
Since $\delta \phi/\delta \rho = 0$, we require
\begin{equation}
\left(\frac{\partial^2}{2 M^2} + 1 \right) \rho_0 = - \frac{\kappa \phi^2}{2 M^2} \, .
\end{equation}
We expand the solution perturbatively,
\begin{equation}
\rho_0 = - 
\left[
    1 
    - \frac{\partial^2}{2 M^2}
    + \left(\frac{\partial^2}{2 M^2}\right)^2
    - \cdots
\right]
\frac{\kappa \phi^2}{2 M^2} \, ,
\end{equation}
and reinsert the solution into our original Lagrangian.
\begin{align}
\mathcal L_\text{eff} &= \frac 12 \left(\partial_\mu \phi\right)^2 
+ \frac 12 \left(\partial_\mu 
\left\{
\left[
    1 
    - \frac{\partial^2}{2 M^2}
    + \left(\frac{\partial^2}{2 M^2}\right)^2
    - \cdots
\right]
\frac{\kappa \phi^2}{2 M^2}
\right\}
\right)^2 
\\
&- m^2 \phi^2  
- M^2 \left(\left[
    1 
    - \frac{\partial^2}{2 M^2}
    + \left(\frac{\partial^2}{2 M^2}\right)^2
    - \cdots
\right]
\frac{\kappa \phi^2}{2 M^2}\right)^2  \nonumber
\\
&+ \kappa \phi^2 \left[
    1 
    - \frac{\partial^2}{2 M^2}
    + \left(\frac{\partial^2}{2 M^2}\right)^2
    - \cdots
\right]
\frac{\kappa \phi^2}{2 M^2}  \nonumber
\end{align}

As written there are many redundant terms. For concreteness, let us write out the first few terms to this effective Lagrangian $\mathcal L_\text{eff} = \mathcal L_0 + \mathcal L_1 + \cdots$.
\begin{align}
\label{eq:eff_lagrangian_two_scalars}
\mathcal L_0^\text{free}
&= \frac 12 \left(\partial_\mu \phi \right)^2 
- m^2 \phi^2
%& \left[\ge \mathcal O \left(\Lambda^{0} \right)\right] 
\\
\nonumber \\
\mathcal L_0^\text{int}
&= -\frac{\kappa}{4 M} \phi^4
+ \frac{\kappa}{2 M} \phi^4
%& \left[\mathcal O \left(\Lambda^{0} \right)\right]
\\
&= \frac{\kappa}{4 M} \phi^4
\nonumber \\
\nonumber\\
M \mathcal L_1
&= 0 
%&\left[\mathcal O \left(\Lambda^{-1} \right)\right]
\\
\nonumber\\
M^2 \mathcal L_2
&= \frac{\kappa^2}{2 M^2} \left( \phi^2 \left(\partial_\mu \phi\right)^2 \right)
+ \frac{\kappa^2}{2 M^2} \left( 
    \phi^2 \left( \partial_\mu \phi \right)^2 
    + \phi^3 \partial^2 \phi
\right)
%& \left[\mathcal O \left(\Lambda^{-2} \right)\right]
 \\
& \qquad 
- \frac{\kappa^2}{M^2} \left( 
    \phi^2 \left( \partial_\mu \phi \right)^2 
    + \phi^3 \partial^2 \phi
\right)
\nonumber \\
&= - \frac{\kappa^2}{2 M^2} \phi^3 \partial^2 \phi
\nonumber
\end{align}

The second derivative is a bit awkward, but fortunately we can recast it as something more quotidian. Because $\partial_\mu (\phi^3 \partial_\mu \phi) = \phi^3 \partial^2 \phi + 3 \phi^2 (\partial_\mu \phi)^2$, and because observables are insensitive to adding a gradient to the action, we can replace $\phi^3 \partial^2 \phi$ with  $-3 \phi^2 (\partial_\mu \phi)^2$ in the Lagrangian. Yet even these choices aren't unique. Shifting the irrelevant operators by a term proportional to an equation of motion is equivalent to redefining the fields, which by the equivalence theorem will have no impact on scattering calculations~\cite{Arzt:1993gz}. From the Euler-Lagrange equations, we see that
\begin{equation}
\partial^2 \phi = - 2 m \phi^2 + 2 \kappa \rho \phi \, ,
\end{equation}
with the second term contributing at a higher order than the first, thus allowing us to equivalently write $M^2 \mathcal L_2 \sim \phi^6$.

Regardless, let us reconsider the generic scalar effective Lagrangian from before, \eqref{eq:eff_lagrangian_generic_scalar}. In that case, we took a ``bottom-up'' approach, in which we constructed our EFT without specifying the full theory. In this case we have taken a ``top-down'' approach, in which we started with a theory involving two scalar fields and integrated-out the heavy one. Indeed, we see that the bottom-up Lagrangian \eqref{eq:eff_lagrangian_generic_scalar} could also describe the top-down Lagrangian \eqref{eq:eff_lagrangian_two_scalars} by matching coefficients.

\subsection{The Standard Model?}

Some physicists (e.g., \cite{Weinberg:2021exr, Burgess:2020tbq}) have argued that the Standard Model itself is an effective field theory. Recall that when constructing a quantum field theory, we expect the theory to be renormalizable. As a practical matter renormalizable theories are convenient, as observables can be calculated at any scale using only a finite number of parameters. Let us contrast this approach with the effective field theory approach. When constructing an effective field theory, we write down the most general Lagrangian compatible with the symmetries/gauge redundancies of the system. This Lagrangian will generally include irrelevant terms which will not be renormalizable. 

The Standard Model is a renormalizable theory. Consequently we can calculate observables from this Lagrangian at any scale without encountering unphysical divergences (whether we might have other difficulties in this calculation is a separate matter). But how can we \emph{know} whether the predictions at, for example, $10^{20}$ GeV are correct without an experimental check? Perhaps there are non-renormalizable terms that must be added to the Standard Model to make the correct prediction. Indeed, history teaches that the ``full theory'' of yesteryear becomes the effective theory of today. 

Of course, if the Standard Model is better thought of as an effective field theory, we would like to know the scale $\Lambda_\text{SM}$ at which the renormalizable part of the theory breaks down. One possible candidate for $\Lambda_\text{SM}$ comes from neutrinos. When the Standard Model was originally formulated, experimental data suggested a neutrino mass compatible with zero, an (incorrect) assumption that was then baked into the Standard Model Lagrangian. There are a few ways to cure this deficiency, but let's focus on an EFT approach.

There is only a single dimension-5 operator that can be added to the Standard Model while preserving the SU(3)$\times$SU(2)$\times$U(1) gauge group \cite{Burgess:2020tbq, Weinberg:1980bf, Klinkhamer:2011aa}. To wit, it is the Weinberg operator
\begin{equation}
\mathcal L_5 = \frac{1}{\Lambda} \sum_{f, f^\prime} c_{f, f^\prime}\left(\overline L_f \cdot \widetilde H \right) \left(\widetilde H^\dagger  \cdot L_{f^\prime} \ \right)^c + \text{h.c.}
\end{equation}
Here $L = (\nu_\text{L}, \; l_\text{L})^T$ and $H=(\phi^+, \; \phi^0)^T$ are doublets of SU(2) weak isospin with sums over flavor $f, f^\prime =e, \mu, \tau$. Expanding this term in the unitary gauge $H=(0, h+v)$, we see there is a term proportional to $(v^2/\Lambda) \nu \nu$, where $v \sim 100$ GeV is the vacuum expectation value of the Higgs. Assuming the dimensionless coupling is order $c \sim 1$ and the heaviest neutrino mass is $m_\nu \sim 0.1 $ MeV, we can estimate the EFT scale to be $\Lambda_\text{SM} \sim 10^{14}$ GeV---but only if such an interaction actually occurs in nature. Violation of $B-L$ conservation would be evidence for such a term, as the Weinberg operator does not preserve this number.