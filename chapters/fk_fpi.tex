\chapter{Ratio of Pseudoscalar Decay Constants, $F_K/ F_\pi$}
\label{chap:fk_fpi}
The ratio $F_K/F_\pi$ is known as a \emph{gold-plated} quantity in lattice QCD, being something that's easily calculable on the lattice and thus useful as a benchmark in comparing the fermion actions used by different collaborations.  Moreover, precisely knowing the ratio allows one to determine the ratio of the Cabibbo-Kobayashi-Maskawa matrix elements $|V_{us}|$ and $|V_{ud}|$, as we will explain.

The work described in this chapter culminated in the following publication

\begin{itemize}
	\item[] N. Miller et al., Phys. Rev. D 102, 034507 (2020), arXiv:2005.04795 [hep-lat].
\end{itemize}

\section{Connection to the Cabibbo-Kobayashi-Maskawa matrix}
The Cabibbo-Kobayashi-Maskawa (CKM) matrix characterizes the extent to which the quarks eigenstates of the strong interaction can be thought of as a quark eigenstates of the weak interaction. In a universe where the quark eigenstates of the weak and strong interaction are the same, the CKM matrix is the identity; we are evidently not in that universe (quarks can change flavors through the weak interaction), and indeed the CKM matrix reflects that deviation from unity. Per the PDG~\cite{PhysRevD.98.030001} (and slightly overestimating some uncertainties to simplify the notation),
\begin{equation}
	\begin{bmatrix}
	\vert V_{ud}\vert  & \vert V_{us}\vert  & \vert V_{ub}\vert  \\
	\vert V_{cd}\vert  & \vert V_{cs}\vert  & \vert V_{cb}\vert  \\
	\vert V_{td}\vert  & \vert V_{ts}\vert  & \vert V_{tb}\vert
	\end{bmatrix} = \begin{bmatrix}
	0.97401(11) & 0.22650(48) & 0.00361(11) \\
	0.22636(48) & 0.97320(11) & 0.04053(83) \\
	0.00854(23) & 0.03978(82) & 0.99917(04)
	\end{bmatrix} \, .
\end{equation}

Since the standard model requires the CKM matrix to be unitary, we can derive constraints on the rows and columns of this matrix. We will concentrate in particular on the top-row unitarity condition, 
\begin{equation}
|V_{ud}|^2 + |V_{us}|^2 + |V_{ub}|^2 = 1 \, .
\end{equation}

We make the following observations regarding these matrix elements:

\begin{enumerate}
	\item Of the three matrix elements in this relation, $\vert V_{ud}\vert$ is the most precisely known. Here $\vert V_{ud}\vert$ is extracted using superallowed beta decays, in which one calculates a comparative half-life for some nucleus, which can then be averaged with the comparative half-lives from several different nuclei \cite{Towner:2010zz}; the dominant uncertainty comes from the radiative and nuclear structure corrections as predicted by theory~\cite{Zyla:2020zbs}.
	\item Historically $\vert V_{us} \vert$ was determined by assuming SU(3) flavor symmetry, which allowed one to estimate the form factors by relating the decays of different baryons in the baryon octet \cite{Towner:2010zz}. However, as SU(3) flavor symmetry is broken by $\sim$15$\%$, this leads to a comparably poor estimate. These days one determines $\vert V_{us} \vert$ instead by using either leptonic ($K_{\ell 2}$) or semi-leptonic ($K_{\ell 3}$) kaon decays in conjunction with a lattice estimate of the associated form factor(s) \cite{Aoki:2021kgd}.
	\item Finally, the last matrix element in this relation, $\vert V_{ub} \vert$, is determined from semi-leptonic $B$ decays; however, it is largely irrelevant for top-row unitarity tests, as its central value is small enough to be eclipsed by the uncertainty of the other two. Thus the top-row unitarity condition is primarily a test between $\vert V_{ud} \vert$ and $\vert V_{us} \vert$.
\end{enumerate}

\begin{figure}
	\begin{tikzpicture}
		\begin{feynman}
		  \vertex [label=left:$s$] (i1) at (-3,1);
		  \vertex [label=left:$\overline d$] (i2) at (-3,-1) ;
		  \vertex [label=right:$d$] (f1) at (3,1) ;
		  \vertex [label=right:$\overline s$] (f2) at (3,-1) ;
		  \vertex (v1) at (-1,1);
		  \vertex (v2) at (-1,-1);
		  \vertex (v3) at (1,1);
		  \vertex (v4) at (1,-1);
		  \diagram* {
			(i1) --[fermion] (v1) --[fermion, edge label'=$\overline q$] (v2) --[fermion] (i2),
			(f1) --[anti fermion] (v3) --[anti fermion,  edge label=$q$] (v4) --[anti fermion] (f2),
			(v1) --[photon, edge label=$W^-$] (v3),
			(v2) --[photon, edge label'=$W^+$] (v4),};
		\end{feynman}
	  \end{tikzpicture}
	\caption{A kaon box diagram. Here $q$ is an up-type quark (i.e., $q \in \{u, c, t\}$). Because the quark eigenstates are different in the strong and weak interactions, a $K^0$ can spontaneously change into a $\overline{K}^0$. Consequently, kaon decays do not conserve CP.}
\end{figure}

In summary, $|V_{us}|$ is essential for checking top-row unitarity. Additionally, in the Wolfenstein  $(\lambda, A, \overline{\rho}, \overline{\eta})$ parameterization of the CKM matrix~\cite{Wolfenstein:1983yz}, $|V_{us}| = \lambda$ and thus affects all the other entries of the CKM matrix.

In this work, we determine $|V_{us}|$ through the leptonic decay $K \rightarrow l \overline \nu_l$ by using the lattice and experimental input. We begin with the charge-changing Lagrangian for the weak interaction~\cite{Towner:2010zz}, 
\begin{equation}
	\mathcal L_\text{CC} = -\frac{G_F}{\sqrt{2}} V_{km} \overline u^k \gamma_\mu \left(1 - \gamma^5\right) d^m
	\overline l \gamma^\mu \left(1 - \gamma^5\right) \nu + \text{ h.c.}
\end{equation}
We can now write down the transition matrix element for a pseudoscalar---for example, the pion---decaying into two leptons.

\begin{align}
	\vcenter{\hbox{\begin{tikzpicture}
		\begin{feynman}
			\vertex [label=left:$u$] (i1) at (-2,1);
			\vertex [label=right:$\mu^+$] (f1) at (2,1) ;
			\vertex [label=left:$\overline d$] (i2) at (-2,-1) ;
			\vertex [label=right:$\nu_\mu$] (f2) at (2,-1) ;
			\vertex (v1) at (-1,0);
			\vertex (v2) at (1,0);
			\diagram*{
			(i1) --[fermion] (v1) --[fermion] (i2),
			(f1) --[fermion] (v2) --[fermion] (f2),
			(v1) --[photon] (v2)
			};
		\end{feynman}
	\end{tikzpicture}
	}}
	&= \frac{G_F}{\sqrt{2}} V_{ud} \underbrace{\braket{0 | \overline d \gamma_\mu \gamma^5 u| \pi(p)}}_{i \sqrt{2} p_\mu F_\pi}
	l \gamma^\mu \left(1 - \gamma^5\right) \overline \nu_l
\end{align}

Fermi's golden rule allows us to relate the decay rates to the spin-averaged transition matrix element. To wit, $d\Gamma \sim \braket{|T|^2} d\phi$, with $\phi$ a phase space factor. Consequently, we expect $\Gamma(\pi \rightarrow l \overline \nu_l) \sim |V_{ud}|^2 F_\pi^2$. A similar argument can be made for $|V_{us}|$.


\section{Calculating $F_K/F_\pi$ on the lattice}

Marciano~\cite{Marciano:2004uf, Durr:2010hr} showed how to relate the ratio the decay rates to the ratio of the decay constants exactly, which allows us to extract the ratio $|V_{us}|^2 / |V_{ud}|^2$.
\begin{equation}
	\label{eq:fkfpi_marciano}
	\frac{\Gamma(K \rightarrow l \, \overline{\nu}_l)}{\Gamma(\pi \rightarrow l \, \overline{\nu}_l)} =
	\left(\frac{F_K}{F_\pi} \right)^2 \frac{|V_{us}|^2}{|V_{ud}|^2} \frac{m_K (1 - m_l^2/ m_K^2)^2}{m_\pi (1 - m_l^2/ m_\pi^2)^2} \left[ 1 + \frac{\alpha}{\pi}(C_K - C_\pi) \right]
\end{equation}
The decay rates $\Gamma$ and masses are well-determined from experiment. The last factor (in brackets) accounts for radiative electroweak corrections, but it contributes little to the calculation due to the factor of $\alpha$.

The pseudoscalar decay constants themselves are defined as follows,
\begin{equation}
\langle 0 | \overline{d} \gamma_\mu \gamma_5 u | \pi^+(p) \rangle = i \sqrt{2} p_\mu F_{\pi^+}
\qquad
\langle 0 | \overline{s} \gamma_\mu \gamma_5 u | K^+(p) \rangle = i \sqrt{2} p_\mu F_{K^+} \, .
\end{equation}
That is, they are related to the expectation value for the respective pseudoscalar particle to return to the (QCD) vacuum, which occurs by the action of its antiparticle on the state. This particular combination of gamma matrices ensures the antiparticle is a pseudoscalar also.

On the lattice, we can relate these decay constants to the correlation functions through the following Ward identity~\cite{Berkowitz:2017opd}.
\begin{equation}
F_{q_1 q_2} = Z^{(PS)}_{q_1 q_2}\frac{m_{q_1} + m_{q_2} + m^{(res)}_{q_1} + m^{(res)}_{q_2} }{\sqrt[\leftroot{-3}\uproot{3}3]{E_{q_1 q_2}}} 
\end{equation}
Here $m^{(res)}_q$ is the residual mass of quark $q$ (a quantity particular to M\"obius domain wall fermions, which characterizes the breaking of chiral symmetry), $E_{q_1 q_2}$ is the ground state energy of the meson $(q_1 q_2)$, and $Z^\text{PS}_{q_1q_2}$ is the wavefunction overlap. The latter two quantities are determined from correlator fits, whereas the other quantities are known \emph{a priori}. (Refer to Appendix~\ref{app:correlators} for a nuanced discussion of correlator fits.)


\section{Extrapolation functions}
The goal of this project is to determine the ratio $F_K/F_\pi$, which will allow us to estimate the ratio $|V_{us}|/|V_{ud}|$; doing so will require us to generate the quantity $F_K/F_\pi$ on each lattice and then extrapolate the observable to the physical point. 

As in Chapter~\ref{chap:scale_setting}, we separate our fit function into a chiral piece and discretization piece. 
\begin{equation}
	\left(\frac{F_K}{F_\pi}\right)^\text{fit} 
	= \left(\frac{F_K}{F_\pi}\right)^\text{chiral}
	+ \left(\frac{F_K}{F_\pi}\right)^\text{disc}
\end{equation}

\subsection{Chiral models}

The SU(3) \xpt expression for $F_K/F_\pi$ to N$^2$LO is~\cite{Ananthanarayan:2017qmx}
\begin{align}
	\label{eq:fk_fpi_xpt_expanded}
	\left(\frac{F_K}{F_\pi}\right)^\text{$\chi$PT-expanded} =& 1
	+\frac{5}{8}\epsilon_\pi^2 \log \epsilon_\pi^2 
	-\frac{1}{4}\epsilon_K^2 \log \epsilon_K^2 
	-\frac{3}{8}\epsilon_\eta^2 \log \epsilon_\eta^2
	+4 (4\pi)^2 L_5 \left(\epsilon_K^2 - \epsilon_\pi^2 \right)
	&\text{(NLO)}
	\nonumber\\&
	+\epsilon_K^4 F_F\left(\frac{\epsilon_\pi^2}{\epsilon_K^2}\right)
	+\hat{K}_1^r \left(\log \epsilon_\pi^2\right)^2
	+\hat{K}_2^r \log \epsilon_\pi^2 \log \epsilon_K^2 
	&\text{(N$^2$LO)}
	\nonumber\\&
	+\hat{K}_3^r \log \epsilon_\pi^2  \log \epsilon_\eta^2 
	+\hat{K}_4^r \left(\log \epsilon_K^2\right)^2
	+\hat{K}_5^r \log \epsilon_K^2  \log \epsilon_\eta^2 
	+\hat{K}_6^r \left(\log \epsilon_\eta^2\right)^2
	\nonumber\\&
	+\hat{C}_1^r \log \epsilon_\pi^2 
	+\hat{C}_2^r \log \epsilon_K^2 
	+\hat{C}_3^r \log \epsilon_\eta^2 
	+\hat{C}_4^r\, . 
\end{align}
where $\epsilon_p = m_p / \Lambda_\chi$. Let us refer to this as the \emph{$\chi$PT-expanded} expression for $F_K/F_\pi$. Clearly this chiral expression is significantly more complicated than the chiral expression for $w_0 M_\Omega$ (recall that one reason for choosing $w_0$ as a scale setting quantity was for its weak pion dependence). We will break down these terms by order in the next few sections. 

Compared to our chiral extrapolation of $w_0 M_\Omega$ before, we have an additional constraint on $F_K/F_\pi$: in the SU(3) flavor limit, we expect $F_K/F_\pi = 1$. Indeed, the expression above satisfies this condition, and it is easily verified to NLO using the GMOR relation (Eq.~\ref{eq:gmor}).

However, rather than use the expanded expression, we can instead calculate $F_K$ and $F_\pi$ individually, then calculate the ratio (of course, the decay constants themselves are dimensionful quantities, but this is irrelevant once we take the ratio). In this case, we instead use 
\begin{equation}
	\left(\frac{F_K}{F_\pi}\right)^\text{$\chi$PT-ratio} = \frac{F_K^\text{$\chi$PT}}{F_\pi^\text{$\chi$PT}}
\end{equation}
where to NLO~\cite{Gasser:1984gg}
\begin{align}
	\label{eq:fk_xpt}
	F_K^\text{$\chi$PT} &= F_0 \left[1
	-\frac{3}{8}\epsilon_\pi^2 \log \epsilon_\pi^2
	-\frac{3}{4}\epsilon_K^2 \log \epsilon_K^2
	-\frac{3}{8}\epsilon_\eta^2 \log \epsilon_\eta^2
	+4\epsilon_\pi^2 (4 \pi)^2  L_4 + 4\epsilon_K^2 (4 \pi)^2  (L_5 + 2L_4) \right] \, , \\
	\label{eq:fpi_xpt}
	F_\pi^\text{$\chi$PT}&= F_0 \left[1
	- \epsilon_\pi^2 \log \epsilon_\pi^2
	-\frac{1}{2}\epsilon_K^2 \log \epsilon_K^2
	+4 \epsilon_\pi^2 (4\pi)^2 (L_4 + L_5)
	+8 \epsilon_K^2 (4\pi)^2 L_4\right] \, . 
\end{align}
Let us refer to this as the \emph{$\chi$PT-ratio} expression for $F_K/F_\pi$.

We also attempted to fit $F_K/F_\pi$ using mixed-action effective field theory~\cite{Bar:2003mh}, an extension of \xpt to the sea-quark sector. However, the weights were orders of magnitude worse for this class of fits and were ultimately excluded from the analysis, so we omit a discussion of them here. Refer to the full paper for details~\cite{Miller:2020xhy}.

In this work we also consider the choices $\mu = \Lambda_\chi \in 4 \pi \{F_\pi, F_K, \sqrt{F_\pi F_K}\}$ as different proxies for the chiral cutoff/renormalization scale. Note that we use a sliding renormalization scale (e.g., using the value of $F_\pi$ on an ensemble) and not a fixed renormalization scale (e.g., using the physical point value of $F_\pi$). Different choices (without counterterms) are expected to shift the LECs but not the final extrapolated value. Corrections for these different choices are worked out in Appendix~\ref{app:fk_fpi_extras}.

\subsubsection{Chiral models: NLO}
Appearing in the NLO \xpt expressions Eqs.~\eqref{eq:fk_fpi_xpt_expanded}, \eqref{eq:fk_xpt}, and \eqref{eq:fpi_xpt} are the Gasser-Leutwyler LECs $L_4$ and $L_5$. These LECs are the coefficients in the next leading term in the chiral Lagrangian, e.g.
\begin{equation}
	\mathcal L^{(4)} \supset L_5\Tr\left[\partial_\mu U \partial^\mu U^\dagger \left(MU^\dagger + UM^\dagger\right)\right] \, .
\end{equation}
Even though we could model our expression for $F_K/F_\pi$ as a Taylor expansion plus $\chi$PT-motivated log terms, using the explicit \xpt expression allows us to determine the Gasser-Leutwyler LECs, which can be reused for other \xpt calculations.

Comparing the $\chi$PT-expanded and $\chi$PT-ratio models, we observe that there is an advantage to the expanded form: $L_4$ is eliminated at NLO. 

Finally, we note that although we typically prior our LECs when fitting as $\mathcal O(1)$ (that is, we assume our effective field theory is ``natural''), we note that the Gasser-Leutwyler coefficients have been measured to be much smaller, roughly~$\mathcal O(10^{-3})$.

\subsubsection{Chiral models: N$^2$LO}
\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/FKFpi_vs_epi_xpt_nnnlo_FV_PP.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/FKFpi_vs_ea_xpt_nnnlo_FV_ct_PP.pdf}
	\end{subfigure}
	\caption{\label{fig:fkfpi_extrapolations}
	Example extrapolation as a function of the light quark mass (left) and lattice spacing (right). Figure from~\cite{Miller:2020xhy}.
	}
\end{figure}

The N$^2$LO corrections can be broadly classified into four categories: pure Taylor, single logs, double-logs, and a ``sunset" term, the last of which comes from evaluating a sunset integral.
\begin{equation}
	\delta\left(\frac{F_K}{F_\pi}\right)^\text{$\chi$PT-expanded}_\text{N$^2$LO}
	= \delta \left(\frac{F_K}{F_\pi}\right)^\text{Taylor}_\text{N$^2$LO} 
	+ \delta \left(\frac{F_K}{F_\pi}\right)^\text{log}_\text{N$^2$LO}
	+ \delta \left(\frac{F_K}{F_\pi}\right)^\text{log$^2$}_\text{N$^2$LO}
	+ \delta \left(\frac{F_K}{F_\pi}\right)^\text{sunset}_\text{N$^2$LO}
\end{equation}

The Taylor terms can be modeled in a straightforward fashion: one could simply write $\sum_p \alpha_p \epsilon_p^2 (\epsilon_K^2 - \epsilon_\pi^2)$. However, since the $\chi$PT-expanded expression is known to N$^2$LO, we could instead rewrite the $\alpha_p$ coefficients in terms of the Gasser-Leutwyler coefficients; this is encapsulated in the term
\begin{equation} 
	\delta \left(\frac{F_K}{F_\pi}\right)^\text{Taylor}_\text{N$^2$LO} = \hat{C}_4^r \, .
\end{equation}
In addition to the Gasser-Leutwyler constants, however, this term also includes the coefficients of $\mathcal L^{(6)}$.

The single log dependence is captured by the remaining $\hat{C}_i^r$.
\begin{equation}
	\delta \left(\frac{F_K}{F_\pi}\right)^\text{log}_\text{N$^2$LO}
	=\hat{C}_1^r \log \epsilon_\pi^2 
	+\hat{C}_2^r \log \epsilon_K^2 
	+\hat{C}_3^r \log \epsilon_\eta^2 
\end{equation}
Only the Gasser-Leutwyler LECs appear in these terms.

The double-log terms are entirely determined, with no LECs appearing inside $\hat K_i^r$.
\begin{align}
	\delta \left(\frac{F_K}{F_\pi}\right)^\text{log$^2$}_\text{N$^2$LO}
	=&\hat{K}_1^r \left(\log \epsilon_\pi^2\right)^2
	+\hat{K}_2^r \log \epsilon_\pi^2 \log \epsilon_K^2 
	+\hat{K}_3^r \log \epsilon_\pi^2  \log \epsilon_\eta^2 \\
	&+\hat{K}_4^r \left(\log \epsilon_K^2\right)^2
	+\hat{K}_5^r \log \epsilon_K^2  \log \epsilon_\eta^2 
	+\hat{K}_6^r \left(\log \epsilon_\eta^2\right)^2 \nonumber
\end{align}

The remaining term arises comes from the sunset integral.
\begin{equation}
	\delta \left(\frac{F_K}{F_\pi}\right)^\text{sunset}_\text{N$^2$LO} 
	= \epsilon_K^4 F_F\left(\frac{\epsilon_\pi^2}{\epsilon_K^2}\right)
\end{equation}
The analytic result is quite complicated but was worked out in~\cite{Ananthanarayan:2017qmx}. In our extrapolation routine, we include these terms using a Python wrapper for the software package \texttt{CHIRON}~\cite{Bijnens:2014gsa}.

We can use N$^2$LO chiral corrections for the ratio-type models also so long as we subtract off the N$^2$LO cross terms that come from Taylor expanding 
\begin{equation}
	\frac{F_K}{F_\pi}= \frac{1 + \delta F_K^\text{NLO}}{1 + \delta F_\pi^\text{NLO}} \, .
\end{equation}
Thus 
\begin{equation}
	\delta\left(\frac{F_K}{F_\pi}\right)^\text{$\chi$PT-expanded}_\text{N$^2$LO}
	= \delta\left(\frac{F_K}{F_\pi}\right)^\text{chiral}_\text{N$^2$LO}
	+ \delta F^\text{NLO}_\pi \delta F^\text{NLO}_K - \left(F^\text{NLO}_\pi\right)^2 \, .
\end{equation}

\subsubsection{Chiral models: N$^3$LO}
Corrections at N$^3$LO are pure Taylor counterterms (with the appropriate factor of $\epsilon_K^2 - \epsilon_\pi^2$). Although simple, we find that these terms are useful in obtaining a good quality fit. 

\subsection{Discretization effects}
The discretization corrections for $F_K/F_\pi$ are nearly identical to those we included for $w_0 M_\Omega$ in Chapter~\ref{chap:scale_setting}. That is, for the finite volume corrections we again modify the logs as
\begin{equation}
	\log \epsilon_\pi^2 \rightarrow \log \epsilon_\pi^2 + 4 k_1(m_\pi L) 
\end{equation}
using the coefficients given in Table~\ref{tab:w0_cN_weights}. We omit the N$^2$LO finite volume corrections in which the double-log terms are modified, as we find our fits are insensitive to these corrections when fitting this particular observable. 

The lattice spacing corrections are also modified to ensure that $F_K/F_\pi \rightarrow 1$ in the SU(3) flavor limit. We again require that the discretization terms include a factor of $\epsilon_K^2 - \epsilon_\pi^2$, which consequently means that the lowest-order lattice spacing corrections must enter at~$\mathcal O(\epsilon^4)$. Similarly, if we choose to include the radiative $\alpha_s$ correction, it also contributes at~$\mathcal O(\epsilon^4)$.


\section{Results \& conclusions}

We include the following models in our model average. A representative model is shown in Fig.~\ref{fig:fkfpi_extrapolations}.
\begin{align*}
	\begin{array}{rl}
		\times 3:& \text{Choice of }\mu = \Lambda_\chi \in 4 \pi \{F_\pi, F_K, \sqrt{F_\pi F_K}\}\\
		\times 2:& \text{Choice of $\chi$PT-expanded or $\chi$PT-ratio for chiral model} \\
		\times 2:& \text{Include/exclude $\chi$PT corrections at N$^2$LO}\\
		\times 2:& \text{Include/exclude $\alpha_s$ discretization correction}\\
		\hline
		24:& {\text{Total choices}}
	\end{array}
\end{align*}
The impact of these differences is shown in Figs.~\ref{fig:fkfpi_model_breakdown} and~\ref{fig:fkfpi_model_average}. Notably, we find that simply using Taylor counterterms at N$^2$LO (but keeping the $\chi$PT-motivated terms at NLO) is strongly preferred over a full $\chi$PT fit, suggesting our data is insufficient to discern the chiral logs. Further, as mentioned previously, results from mixed-action \xpt fared no better than regular \xpt, with the most discernible difference being a tanking of the Bayes factor. Fits using the canonical choice $\Lambda_\chi = 4\pi F_\pi$ contribute the most to the model average, whereas those using $\Lambda_\chi = 4\pi F_K$ (the largest deviation from the canonical choice) contribute the least. Nevertheless, the difference on the extrapolation is less than $1\sigma$.

We report a final model-averaged result of 
\begin{align}
	\frac{F_K}{F_\pi} &= 1.1964(32)^s(12)^\chi(20)^a(01)^V(15)^{\rm phys}(12)^M
	\\ &= 1.1964(44)\, . \nonumber
\end{align}
separated into statistical ($s$), chiral ($\chi$), lattice spacing ($a$), finite volume ($V$), physical point input (phys), and model selection (M) uncertainties.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figs/fk_fpi/model_breakdown.pdf}
	\caption{\label{fig:fkfpi_model_breakdown}
	Model comparison. The left panel shows the result of the model average for given restrictions on the models, while the right side shows the relative weight of the model. Only models that include the same set of ensembles can be compared; thus the fits with gray triangles (denoting fits in which the finest lattice spacing ensemble is excluded) should only be compared among themselves. We see that the mixed-action EFT fits produce a similar result as our model average despite having negligibly small weight and being excluded from the model average. The final model average is relatively insensitive to our choice of priors. 
	Figure from~\cite{Miller:2020xhy}.
	}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/hist_ct.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/hist_ratio.pdf}
	\end{subfigure}
	\\
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/hist_FF.pdf}
	\end{subfigure}
	\caption{\label{fig:fkfpi_model_average}
	Histograms for some of the model averaging choices. Besides showing the relative weights, these plots also demonstrate the distribution of values. 
	Figure from~\cite{Miller:2020xhy}.
	}
\end{figure}

Additionally, we also estimate the SU(2) isospin-breaking correction~\cite{Cirigliano:2011tm}. 
\begin{equation}
\delta_{\text{SU(2)}} = \sqrt{3} \epsilon_\text{SU(2)} \left[
-\frac43 (F_K / F_\pi-1)
+\frac{4}{3(4 \pi F)^2} \left( m_K^2 - m_\pi^2 -m_\pi^2 \log \frac{m_k^2}{m_\pi^2} \right)
\right]
\end{equation}
Here $\epsilon_\text{SU(2)} = \sqrt{3}/(4R)$, $R=35.7(2.6)$ per FLAG~\cite{Aoki:2021kgd}, the masses are the physical values, and $F_K/F_\pi$ is the extrapolated result. With $\delta_{\text{SU(2)}}$ in hand, we can compare our value of $F_K/F_\pi$ determined on the lattice to the corrected charged ratio $F^\pm_K/F^\pm_\pi$ through
\begin{equation}
\frac{F^\pm_K}{F^\pm_\pi} = \frac{F_K}{F_\pi} \sqrt{1+ \delta_{\text{SU(2)}}} \, .
\end{equation}
The corrected charge ratio $F^\pm_K/F^\pm_\pi$, as opposed to $F_K/F_\pi$, is the quantity compiled by FLAG. We find that
\begin{align}
	\frac{F^\pm_K}{F^\pm_\pi} &= 1.1942(44)(07)^\text{isospin} \\
	&= 1.1942(45) \nonumber \, .
\end{align}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figs/fk_fpi/vus_vud.pdf}
	\caption{\label{fig:fkfpi_vus_vud}
	Plot of $|V_{us}|/|V_{ud}|$ (red band). The blue band is the global average for $|V_{us}|$ using lattice determinations of $f^+(0)$ plus semi-leptonic kaon decays. The green band is the global average for  $|V_{ub}|$ using superallowed beta decays. 
	The figure from~\cite{Miller:2020xhy} has been updated with more recent averages.
	}
\end{figure}

Next we check the top-row unitarity condition and determine the CKM matrix element $|V_{us}|$. From Eq.~\eqref{eq:fkfpi_marciano}, we have
\begin{equation}
	\frac{|V_{us}|}{|V_{ud}|}\frac{F_K^\pm}{F_\pi^\pm} = 0.2760(4) 
	\implies 
	\frac{|V_{us}|}{|V_{ud}|} = 0.2311(10)
\end{equation}
using global averages from the PDG~\cite{Zyla:2020zbs} and our model-averaged result. 

With our ratio $|V_{us}|/|V_{us}|$, we can either determine $|V_{us}|$ (using the global averages from superallowed beta decay) or $|V_{ud}|$ (using the other lattice determination of $|V_{us}|$ from semi-leptonic kaon decays, $K_{\ell 3}$). 
\begin{align}
	|V_{us}| &= 0.2252(9)  &\text{(w/ $\beta$)} \\
	|V_{ud}| &= 0.9658(49) &\text{(w/ $K_{\ell 3}$)}
\end{align}
Although the $|V_{us}|$ result from $F_K/F_\pi$ is comparable to the result from $f^+(0)$ (see Fig.~\ref{fig:fkfpi_vus_vud}), the result for $|V_{ud}|$ is significantly less precise than superallowed beta decay result, $|V_{ud}| = 0.97373(31)$, as one might expect.

Finally, we check the top-row unitarity condition either assuming the semi-leptonic kaon decay results  for $|V_{us}|$ or the superallowed beta decay value for $|V_{ud}|$.
\begin{align}
	|V_{u}| &= 0.99880(77) &\text{(w/ $\beta$)}\\
	|V_{u}| &= 0.9826(96) &\text{(w/ $K_{\ell 3}$)}
\end{align}
We have incorporated the PDG average $|V_{ub}| = 3.82(24) \times 10^{-3}$, but we note its contribution is minuscule. Either procedure results in some tension with the top-row unitarity condition.

We finish this chapter my recalling a second motivation for calculating $F_K/F_\pi$: being a gold-plated quantity, it is relatively easy to calculate on the lattice and therefore serves as an important benchmark for testing the convergence of different discretizations of the QCD action. Moreover, as we intend to continue to use this action for other calculations, we must verify that our action converges, too. We find that our result agrees with others (Fig.~\ref{fig:fkfpi_flag}).

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figs/fk_fpi/flag_fk_fpi.pdf}
	\caption{\label{fig:fkfpi_flag}
	Results from various collaborations:
	FNAL/MILC~17~\cite{Bazavov:2017lyh},
	QCDSF/UKQCD~16~\cite{QCDSF-UKQCD:2016rau},
	HPQCD~13A~\cite{Dowdall:2013rya},
	ETM~14E~\cite{Carrasco:2014poa},
	RBC/UKQCD~14B~\cite{RBC:2014ntl},
	MILC~10~\cite{MILC:2010hzw},
	BMW~10~\cite{Durr:2010hr},
	ETM 09~\cite{ETM:2009ptp},
	HPQCD/UKQCD~07~\cite{Follana:2007uv}.
	We find that our result (CalLat 20) lies in good agreement. Taken from FLAG~\cite{Aoki:2021kgd}.
	}
\end{figure}

In addition, we find that our action does not require the finest lattice ensemble to obtain our level of precision, which is not true of all actions (see Figs.~\ref{fig:fkfpi_model_breakdown} and~\ref{fig:fkfpi_a06_dependence}).

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/FKFpi_vs_ea_xpt_nnnlo_FV_ct_PP.pdf}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./figs/fk_fpi/FKFpi_vs_ea_xpt_nnnlo_FV_ct_PP_no_a06.pdf}
	\end{subfigure}
	\caption{\label{fig:fkfpi_a06_dependence}
	A representative fit which includes the finest lattice spacing ensemble (left) and a different fit which excludes that ensemble (right). We find that the finest lattice spacing ensemble, although helpful for guiding the extrapolation, is not necessary. Figure from~\cite{Miller:2020xhy}.
	}
\end{figure}